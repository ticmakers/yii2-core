<?php

namespace ticmakers\core;

use Yii;
use yii\base\Application;
use yii\base\BootstrapInterface;
use ticmakers\core\base\Bootstrap as BootstrapBase;
use ticmakers\core\validators\CompareValidator as TicmakersCompareValidator;
use ticmakers\core\validators\UniqueValidator as TicmakersUniqueValidator;
use yii\validators\CompareValidator;
use yii\validators\UniqueValidator;

/**
 * Clase cargadora de características para la aplicación.
 *
 * @package ticmakers
 * @subpackage yii2-base
 * @category Bootstrap
 * 
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @version 0.0.1
 * @since 0.0.0
 */
class Bootstrap extends BootstrapBase
{
    public $moduleId = 'core';
    public $pieces = [
        'modules' => [
            'core' => \ticmakers\core\Module::class
        ],
        'components' => [
            'ui' => \ticmakers\core\helpers\UIHelper::class,
            'html' => \ticmakers\core\helpers\HtmlHelper::class,
            'strings' => \ticmakers\core\helpers\StringsHelper::class,
            'message' => \ticmakers\core\helpers\MessageHelper::class,
            'userHelper' => \ticmakers\core\helpers\UserHelper::class,
            'arrayHelper' => \ticmakers\core\helpers\ArrayHelper::class
        ]
    ];

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        parent::bootstrap($app);

        Yii::$container->set(CompareValidator::class, TicmakersCompareValidator::class);
        Yii::$container->set(UniqueValidator::class, TicmakersUniqueValidator::class);

        if ($app instanceof \yii\console\Application) {
            $app->getModule($this->moduleId)->controllerNamespace =
                'ticmakers\core\commands';
        }

        $bundles = $app->getAssetManager()->bundles;

        if (!isset($bundles['yii\widgets\ActiveFormAsset'])) {
            $app->getAssetManager()->bundles['yii\widgets\ActiveFormAsset'] = [
                'sourcePath' => '@vendor/ticmakers/yii2-core/src/assets',
                'js' => [
                    'yii.activeForm.js',
                ],
            ];
        }

        $this->overrideViews($app, 'core');
    }

    /**
     * Undocumented function
     *
     * @param [type] $app
     * @param [type] $moduleId
     * @return void
     */
    private function overrideViews($app, $moduleId)
    {
        if ($app instanceof \yii\web\Application) {
            $app->getView()->theme->pathMap["@ticmakers/{$moduleId}/views"] = "@app/views/{$moduleId}";
        }
    }
}
