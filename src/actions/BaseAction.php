<?php

namespace ticmakers\core\actions;

use yii\base\Action;
use Yii;
use yii\web\Response;
use ticmakers\core\helpers\ConstantsHelper as C;

class BaseAction extends Action
{
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $debug = false;
    
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $modelClass;

    /**
     * @todo Documentar
     *
     */
    public $viewPath;

    /**
     * @todo Documentar
     *
     */
    public $layout;

    /**
     * Modulo padre de la instancia actual
     */

    public $module;

    /**
     * Permite establecer si el render se debe realizar por medio de ajax
     *
     * @var boolean
     */
    public $isModal = false;

    /**
     * Ruta de la vista a renderizar en caso de no existir en el diretorio de vistas del controlador
     *
     * @var string
     */
    public $commonViewPath = '@ticmakers/core/views/common';

    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'index';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $redirectOnSuccess = true;

    /**
     * Undocumented variable
     *
     * @var array|string|callable
     */
    public $routeRedirect = ['index'];

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnSuccess = 'Successful operation';

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnError = 'Operation failed';

    /**
     * 
     */
    public $_viewFile;

    /**
     * force no render Ajax
     */
    public $renderAjax = true;

    /**
     * This method is called right before `run()` is executed.
     * You may override this method to do preparation work for the action run.
     * If the method returns false, it will cancel the action.
     *
     * @return bool whether to run the action.
     */
    protected function beforeRun()
    {
        if (!empty($this->layout)) {
            $this->controller->layout = $this->layout;
        }

        if (!empty($this->viewPath)) {
            $this->controller->viewPath = $this->viewPath;
        }

        $this->controller->viewPath = str_replace('\\', '/', $this->controller->viewPath);

        //@todo corregir
        $this->module = Yii::$app;

        if ($this->isModal) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }

        $this->loadViewFile();

        return true;
    }

    /**
     * Busca un registro basado en su llave primaria.
     * Si el registro no existe arroja un error HTTP 404.
     *
     * @param integer $id
     * @return Model con el registro.
     * @throws NotFoundHttpException is el registro no es encontrado.
     */
    public function findModel($id)
    {
        return $this->controller->findModel($id);
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private function loadViewFile()
    {
        $this->_viewFile = $this->controller->loadViewFile($this->viewName, $this->commonViewPath);
    }

    /**
     * Renderiza una vista
     *
     * @param [type] $view
     * @param array $params
     * @param [type] $context
     * @return void
     */
    public function render($view, $params = [], $context = null)
    {
        Yii::$app->response->format = Response::FORMAT_HTML;
        if ($this->isModal && $this->renderAjax) {
            $result = $this->controller->renderAjax($view, $params, $context);
        } else {
            $result = $this->controller->render($view, $params, $context);
        }
        return $result;
    }
    /**
     * Permite cargar las instacias de los archivos cargados desde el cliente
     *
     * @param [type] $modelObject Modelo en el cual se procesara el archivo y/o archivos cargadas
     * @return void
     */
    public function loadFileAttributes(&$modelObject)
    {
        return $this->controller->loadFileAttributes($modelObject);
    }

    /**
     * Undocumented function
     *
     * @param array $params
     * @param [type] $routeRedirect
     * @return void
     */
    public function redirect($params = [], $routeRedirect = null)
    {
        $redirect = $routeRedirect ?? $this->routeRedirect;
        if (is_callable($redirect)) {
            $finalRoute = call_user_func_array($redirect, $params);
        } else {
            $finalRoute = $redirect;
        }
        return $this->controller->redirect($finalRoute);
    }

    /**
     * Undocumented function
     *
     * @param Clousure $callback
     * @param array $params
     * @return void
     */
    protected function runCallback($callback, $params = [])
    {
        $result = false;
        if (!empty($callback) && is_callable($callback)) {
            $result = call_user_func_array($callback, $params);
        }
        return $result;
    }
}
