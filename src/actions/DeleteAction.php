<?php

namespace ticmakers\core\actions;

use Yii;
use ticmakers\core\actions\BaseAction;
use yii\base\InvalidConfigException;

/**
 * DeleteAction base para los crud del sistema
 *
 * @package ticmakers
 * @subpackage core
 * @category Actions
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class DeleteAction extends UpdateAttributeAction
{
    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnSuccess = 'It was successfully removed.';

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnError = 'An error occurred while trying to delete.';

    /**
     * Permite cambiar el valor de un atributo de un modelo en el sistema
     *
     * @return void
     */
    public function run($id)
    {
        $modelClass = $this->modelClass;
        if (is_array($this->modelClass)) {
            $primaryClassFound = false;
            foreach ($this->modelClass as $keyModel => $modelConfig) {
                if (isset($modelConfig['isPrimary']) && $modelConfig['isPrimary']) {
                    $modelClass = $modelConfig['class'];
                    $primaryClassFound = true;
                    break;
                }
            }
            if (!$primaryClassFound) {
                throw new InvalidConfigException(Yii::t('app', 'Primary model not found'));
            }
        }

        if (empty($this->value)) {
            $this->value = $modelClass::STATUS_INACTIVE;
        }
        if (empty($this->attribute)) {
            $this->attribute = $modelClass::STATUS_COLUMN;
        }

        return parent::run($id);
    }
}
