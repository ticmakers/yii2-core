<?php

namespace ticmakers\core\actions;

use Yii;
use ticmakers\core\actions\BaseAction;

/**
 * Acción base para los index del los crud del sistema
 *
 * @package ticmakers
 * @subpackage core
 * @category Actions
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class IndexAction extends BaseAction
{

    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'index';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $renderAjax = false;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeRender;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeSearch;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isSortableGrid = false;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $searchMethod = 'search';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $titleInGrid = true;
    /**
     * Lista todos registros según el DataProvider.
     *
     * @return void
     */
    public function run()
    {
        $params = [
            'isSortableGrid' => $this->isSortableGrid,
            'titleInGrid' => $this->titleInGrid
        ];
        if (!empty($this->modelClass)) {
            $modelObject = Yii::createObject($this->modelClass);
            $paramsGet = Yii::$app->request->get();
            if (!empty($paramsGet)) {
                $modelObject->setAttributes($paramsGet);
            }

            $this->runCallback($this->beforeSearch, [&$modelObject]);

            //@todo Remover callback controller
            $this->controller->beforeSearchIndex($modelObject);

            $dataProvider = $this->runCallback([$modelObject, $this->searchMethod], [Yii::$app->request->queryParams]);
            $params = [
                'isSortableGrid' => $this->isSortableGrid,
                'titleInGrid' => $this->titleInGrid,
                'searchModel' => $modelObject,
                'dataProvider' => $dataProvider,
            ];
        }

        $this->runCallback($this->beforeRender, [&$this->_viewFile, &$params]);

        //@todo Remover callback controller
        $this->controller->beforeRenderIndex($this->_viewFile, $params);

        return $this->render($this->_viewFile, $params);
    }
}
