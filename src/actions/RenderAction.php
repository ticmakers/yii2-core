<?php

namespace ticmakers\core\actions;

use Yii;
use ticmakers\core\actions\BaseAction;
/*
 * Acción para renderizar una vista
 *
 * @package ticmakers
 * @subpackage core\actions
 * @category Actions
 * 
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @version 0.0.1
 * @since 1.0.0
 */

class RenderAction extends BaseAction
{
    /**
     * Undocumented variable
     *
     * @var array
     */
    public $paramsRender = [];

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeRender;

    /**
     * Permite renderizar una vista
     *
     * @return mixed
     */
    public function run()
    {
        $paramsGet = Yii::$app->request->get();
        if (!empty($paramsGet)) {
            $this->paramsRender = Yii::$app->arrayHelper::merge($this->paramsRender, $paramsGet);
        }
        if ($this->_viewFile) {
            if (!empty($this->beforeRender) && is_callable($this->beforeRender)) {
                call_user_func_array($this->beforeRender, [&$this->_viewFile, &$this->paramsRender]);
            }
            $output = $this->render(
                $this->_viewFile,
                $this->paramsRender
            );
        } else {
            $output = $this->controller->renderContent('No defined viewFile');
        }
        return $output;
    }
}
