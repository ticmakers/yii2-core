<?php

namespace ticmakers\core\actions;

use ArrayObject;
use Closure;
use Yii;
use ticmakers\core\actions\BaseAction;
use yii\base\Model;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/**
 * SaveAction base para los crud del sistema
 *
 * @package ticmakers
 * @subpackage core
 * @category Actions
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class SaveAction extends BaseAction
{
    public $debug = false;
    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = '_form';

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeSave;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeLoad;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeRender;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isNewRecord = true;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeRun;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $transactionId;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isMultiModel = false;

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isRedis = false;

    public $formInTabs = false;

    public $tabsConfig = [];

    private $moduleInstances = [];

    /**
     * Permite visualizar el formulario para la creación de un registro
     * o realizar la validación de los datos enviados y crear el registro.
     *
     * @return void
     */
    public function run($id = null)
    {
        $this->transactionId = Yii::$app->request->get('t');

        if (!Yii::$app->session->has("saveaction.{$this->transactionId}")) {
            Yii::$app->session->set("saveaction.{$this->transactionId}", [
                'init' => false,
                't' => $this->transactionId
            ]);
        }

        $this->runCallback($this->beforeRun, [&$id]);

        $response = null;
        $loadedData = false;
        $modelObject = null;

        $this->normalizeConfig();

        /**
         * Inicializamos los modelos y cargamos los datos existentes en base datos
         */
        $this->initModels($modelObject, $id);

        /**
         * Cargamos los atributos del modelo principal pasados por metodo GET
         */
        $this->loadGetData($modelObject);

        $this->runCallback($this->beforeLoad, [&$modelObject]);

        /**
         * Inicio carga de datos
         */
        $loadedData = $this->loadData($modelObject);
        /**
         * Fin carga datos POST request
         */
        if ($loadedData) {
            if ($this->isMultiModel) {

                $allSave = true;
                $modelPrimary = null;


                $transaction = Yii::$app->db->beginTransaction();
                try {

                    foreach ($this->modelClass as $keyModel => $modelConfig) {

                        if ($modelConfig['isPrimary']) {
                            $modelPrimary = $modelObject[$keyModel];
                        }

                        if ($modelConfig['isMultiple']) {

                            // if ($modelConfig['isRedis']) {
                            foreach ($modelObject[$keyModel] as $key => $modelObjectMultiple) {
                                if (!empty($modelObjectMultiple->primaryKey)) {
                                    $modelUpdate = $modelObjectMultiple::findOne($modelObjectMultiple->primaryKey);
                                    if ($modelUpdate) {
                                        $modelUpdate->setAttributes($modelObjectMultiple->getAttributes(), false);
                                        $modelObjectMultiple = $modelUpdate;
                                        unset($modelUpdate);
                                    }
                                }
                                $this->loadRelatedData($modelObjectMultiple, $modelObject, $modelConfig);
                                if (!$modelObjectMultiple->save()) {
                                    $allSave = false;
                                    if ($this->debug) {
                                        dd($modelObjectMultiple->getErrors(), $modelObjectMultiple);
                                    }
                                }
                            }
                            // }
                        } else {

                            $this->runCallback($this->beforeSave, [&$modelObject[$keyModel]]);

                            $this->loadRelatedData($modelObject[$keyModel], $modelObject, $modelConfig);

                            if ($keyModel == 'model') {
                                $modelObject[$keyModel]->validate();
                                // dd($modelObject[$keyModel]);
                            }

                            if (!$modelObject[$keyModel]->save()) {
                                $allSave = false;
                                if ($this->debug) {
                                    dd($modelObject[$keyModel]->getErrors(), $modelObject[$keyModel]);
                                }
                            }
                        }
                    }

                    if ($allSave) {
                        $transaction->commit();
                    }
                } catch (\Exception $e) {
                    $transaction->rollBack();
                    $allSave = false;
                    throw $e;
                } catch (\Throwable $e) {
                    $transaction->rollBack();
                    $allSave = false;
                    throw $e;
                }

                if ($allSave) {

                    /**
                     * Borramos cache en redis
                     */
                    $this->clearRedisData();

                    if ($this->isModal) {
                        if ($this->redirectOnSuccess) {

                            $redirect = $this->routeRedirect;
                            if (is_callable($redirect)) {
                                $finalRoute = call_user_func_array($redirect, [$modelPrimary]);
                            } else {
                                $finalRoute = $redirect;
                            }

                            $response = [
                                'state' => Yii::$app->message::TYPE_SUCCESS,
                                'message' => Yii::t('app', $this->messageOnSuccess),
                                'errors' => '',
                                'type' => 'open-load-modal', //open-modal, open-load-modal, redirect, message
                                'url' => $finalRoute,
                                'modal' => 'default-modal'
                            ];
                        } else {
                            $response = [
                                'state' => Yii::$app->message::TYPE_SUCCESS,
                                'message' => Yii::t('app', $this->messageOnSuccess),
                                'errors' => '',
                                'type' => 'message', //open-modal, open-load-modal, redirect, message
                                'url' => '',
                                'modal' => ''
                            ];
                        }
                    } else {

                        Yii::$app->message::setMessage(
                            Yii::$app->message::TYPE_SUCCESS,
                            Yii::t('app', $this->messageOnSuccess)
                        );

                        if ($this->redirectOnSuccess) {
                            $response = $this->redirect([$modelObject]);
                        }
                    }
                } else {
                    if ($this->isModal) {
                        $response = [
                            'state' => Yii::$app->message::TYPE_DANGER,
                            'message' => Yii::$app->html::errorSummary(
                                $modelObject
                            ),
                            'errors' => [],
                            'type' => 'message', //open-modal, open-load-modal, redirect, message
                            'url' => '',
                            'modal' => ''
                        ];

                        foreach ($this->modelClass as $keyModel => $modelConfig) {
                            $response['errors'] = Yii::$app->arrayHelper::merge($response['errors'], ActiveForm::validate($modelObject[$keyModel]));
                        }
                    } else {
                        Yii::$app->message::setMessage(
                            Yii::$app->message::TYPE_DANGER,
                            $this->messageOnError
                            // Yii::$app->html::errorSummary(
                            //     $modelObject
                            // )
                        );
                        $params = [
                            'modelsFormColums' => []
                        ];

                        foreach ($this->modelClass as $keyModel => $modelConfig) {
                            $params['modelsFormColums'][$keyModel] = $modelObject[$keyModel];
                        }
                        $this->runCallback($this->beforeRender, [&$this->_viewFile, &$params]);
                        $response = $this->render($this->_viewFile, $params);
                    }
                }
            } else {
                if (!empty($this->beforeSave) && is_callable($this->beforeSave)) {
                    call_user_func_array($this->beforeSave, [&$modelObject]);
                }

                if ($modelObject->save()) {
                    if ($this->isModal) {
                        $response = [
                            'state' => Yii::$app->message::TYPE_SUCCESS,
                            'message' => Yii::t('app', $this->messageOnSuccess),
                            'errors' => '',
                            'type' => 'message', //open-modal, open-load-modal, redirect, message
                            'url' => '',
                            'modal' => ''
                        ];
                    } else {
                        Yii::$app->message::setMessage(
                            Yii::$app->message::TYPE_SUCCESS,
                            Yii::t('app', $this->messageOnSuccess)
                        );

                        if ($this->redirectOnSuccess) {
                            $response = $this->redirect([$modelObject]);
                        }
                    }
                } else {

                    if ($this->isModal) {
                        $response = [
                            'state' => Yii::$app->message::TYPE_DANGER,
                            'message' => Yii::$app->html::errorSummary(
                                $modelObject
                            ),
                            'errors' => ActiveForm::validate($modelObject),
                            'type' => 'message', //open-modal, open-load-modal, redirect, message
                            'url' => '',
                            'modal' => ''
                        ];
                    } else {
                        Yii::$app->message::setMessage(
                            Yii::$app->message::TYPE_DANGER,
                            Yii::$app->html::errorSummary(
                                $modelObject
                            )
                        );
                        $params = ['model' => $modelObject];
                        $this->runCallback($this->beforeRender, [&$this->_viewFile, &$params]);
                        $response = $this->render($this->_viewFile, $params);
                    }
                }
            }
        } else {
            $params = [];

            if ($this->isMultiModel) {
                $params['modelsFormColums'] = [];
                foreach ($this->modelClass as $keyModel => $modelConfig) {
                    $params['modelsFormColums'][$keyModel] = $modelObject[$keyModel];
                }
            } else {
                $params = [
                    'model' => $modelObject
                    // 'modelsFormColums' => ['model' => $modelObject] //Ajustando error en formularios con un solo modelo
                ];
            }
            $this->runCallback($this->beforeRender, [&$this->_viewFile, &$params]);
            $response = $this->render($this->_viewFile, $params);
        }

        if ($response) {
            return $response;
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private function normalizeConfig()
    {
        $defaultConfig = [
            'isMultiple' => false,
            'isRedis' => false,
            'renderGrid' => false,
            'controller' => false,
            'isPrimary' => false,
            'searchClass' => false,
            'relatedModels' => false,
            'defaultValues' => false,
            'searchModel' => false,
            'dataProvider' => false,
            'redisConfig' => [
                'moduleId' => Yii::$app->id,
                'baseRoute' => ''
            ]
        ];
        if (is_array($this->modelClass)) {

            foreach ($this->modelClass as $keyModel => $modelConfig) {
                $this->modelClass[$keyModel] = Yii::$app->arrayHelper::merge($defaultConfig, $modelConfig);

                if ($this->modelClass[$keyModel]['isMultiple']) {
                    $redisConfig = $this->modelClass[$keyModel]['redisConfig'];

                    $moduleAct = Yii::$app->controller->module->id;
                    $moduleApp = Yii::$app->id;
                    $moduleReq = $redisConfig['moduleId'];

                    if ($moduleAct == $moduleApp && $moduleApp == $moduleReq) {
                        $controller = Yii::$app->createControllerByID($redisConfig['controllerId']);
                        $this->modelClass[$keyModel]['redisConfig']['baseRoute'] = "/{$controller->id}/";
                    } else if (Yii::$app->hasModule($moduleReq) && $moduleAct == $moduleApp) {
                        $moduleInstance = Yii::$app->getModule($moduleReq);
                        $controller = $moduleInstance->createControllerByID($redisConfig['controllerId']);
                        $this->modelClass[$keyModel]['redisConfig']['baseRoute'] = "/{$redisConfig['moduleId']}/{$controller->id}/";
                    } else if (Yii::$app->controller->module->hasModule($moduleReq)) {
                        $moduleInstance = Yii::$app->controller->module->getModule($moduleReq);
                        $controller = $moduleInstance->createControllerByID($redisConfig['controllerId']);
                        $moduleBase = Yii::$app->controller->module->id;
                        $this->modelClass[$keyModel]['redisConfig']['baseRoute'] =
                            "/{$moduleBase}/{$redisConfig['moduleId']}/{$controller->id}/";
                    } else if ($moduleAct == $moduleReq) {
                        $controller = Yii::$app->controller->module->createControllerByID($redisConfig['controllerId']);
                        $this->modelClass[$keyModel]['redisConfig']['baseRoute'] =
                            "/{$redisConfig['moduleId']}/{$controller->id}/";
                    }

                    // if ($redisConfig['moduleId'] === Yii::$app->id) {
                    //     $moduleInstance = Yii::$app;
                    //     $controller = $moduleInstance->createControllerByID($redisConfig['controllerId']);
                    //     $this->modelClass[$keyModel]['redisConfig']['baseRoute'] = "/{$controller->id}/";
                    // } else if ($redisConfig['moduleId'] === Yii::$app->controller->module->id) {
                    //     $moduleInstance = Yii::$app->controller->module;
                    //     $controller = $moduleInstance->createControllerByID($redisConfig['controllerId']);
                    //     $this->modelClass[$keyModel]['redisConfig']['baseRoute'] = "/{$redisConfig['moduleId']}/{$controller->id}/";
                    // } else {
                    //     $moduleInstance = Yii::$app->controller->module->getModule($redisConfig['moduleId']);
                    //     $controller = $moduleInstance->createControllerByID($redisConfig['controllerId']);
                    //     $moduleBase = Yii::$app->controller->module->id;
                    //     $this->modelClass[$keyModel]['redisConfig']['baseRoute'] =
                    //         "/{$moduleBase}/{$redisConfig['moduleId']}/{$controller->id}/";
                    // }

                    $this->modelClass[$keyModel]['redisConfig']['controller'] = $controller;
                    unset($moduleInstance, $controller);


                    if ($this->modelClass[$keyModel]['isRedis']) {
                        $this->isRedis = true;
                    }
                }
            }

            $this->isMultiModel = true;
        } else {
            $this->isMultiModel = false;
        }
    }

    /**
     * Undocumented function
     *
     * @return boolean
     */
    private function loadData(&$modelObject)
    {
        $loadedData = false;

        if ($this->isMultiModel && Yii::$app->request->isPost) {

            $loadedDataMultiple = true;
            foreach ($this->modelClass as $keyModel => $modelConfig) {
                if ($modelConfig['isMultiple'] && $modelConfig['isRedis']) {
                    $loadedDataMultiple &= $this->loadRedisData($modelConfig['class'], $modelObject, $keyModel);
                } else if ($modelConfig['isMultiple'] && !$modelConfig['renderGrid']) {
                    $loadedDataMultiple &= $this->loadPostData($modelConfig['class'], $modelObject, $keyModel);
                } else if (!$modelConfig['isMultiple']) {
                    $this->setDefaultValues($modelObject[$keyModel], $modelConfig['defaultValues'] ?? []);
                    $loadedDataMultiple &= $modelObject[$keyModel]->load(Yii::$app->request->post());
                    //@todo Revisar si es necesario en este punto de la ejecución
                    $this->loadRelatedData($modelObject[$keyModel], $modelObject, $modelConfig);
                }
            }
            $loadedData = $loadedDataMultiple;
        } else if (Yii::$app->request->isPost) {
            $loadedData = $modelObject->load(Yii::$app->request->post());
        }

        return $loadedData;
    }

    /**
     * Undocumented function
     *
     * @param [type] $model
     * @param array $defaultValues
     * @return void
     */
    private function setDefaultValues(&$model, $defaultValues = [])
    {
        if (!empty($defaultValues) && is_array($defaultValues)) {
            foreach ($defaultValues as $fieldModel => $value) {
                $model->{$fieldModel} = $value;
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $modelObject
     * @param [type] $id
     * @return void
     */
    private function loadUpdateInstances(&$modelObject, $id)
    {
        if ($this->isMultiModel) {
            foreach ($this->modelClass as $keyModel => $modelConfig) {

                if ($modelConfig['isPrimary']) {
                    $modelObjectAct = Yii::createObject($modelConfig['class']);
                    if (($modelInstanceAct = $modelObjectAct::findOne($id)) !== null) {
                        $modelObject[$keyModel] = $modelInstanceAct;
                        Yii::configure($modelObject[$keyModel], $modelConfig['configUpdateInstance'] ?? []);
                    } else {
                        throw new NotFoundHttpException(Yii::t(
                            'app',
                            'The requested page does not exist.'
                        ));
                    }
                } else {
                    $modelObjectAct = Yii::createObject($modelConfig['class']);
                    $conditions = Yii::$app->arrayHelper::merge([
                        'active' => 'Y'
                    ], $modelConfig['conditions'] ?? []);

                    foreach ($modelConfig['relatedFieldsCondition'] ?? [] as $fieldModel => $fieldParent) {
                        $conditions[$fieldModel] = $modelObject[$modelConfig['relatedModel']]->{$fieldParent};
                    }

                    $currentModelObject = $modelObjectAct::find()->where($conditions);


                    if ($modelConfig['isMultiple'] && $modelConfig['isRedis']) {

                        $sessionData = Yii::$app->session->get("saveaction.{$this->transactionId}");
                        /* if ($this->debug) {
                            dd($sessionData);
                        } */
                        if ($modelConfig['relatedModels']) {
                            foreach ($modelConfig['relatedModels'] as $keyModelRelated => $relatedConfig) {
                                if (isset($relatedConfig['isModelLoad']) && $relatedConfig['isModelLoad']) {

                                    if (!isset($sessionData['init']) || !$sessionData['init']) {
                                        //@todo Verificar carga de datos anteriores

                                        $allData = $modelObject[$keyModelRelated]->{$relatedConfig['relationName']};
                                        $modelRedis = Yii::createObject($modelConfig['class']);
                                        $modelRedis::deleteAll(['transaction' => $this->transactionId]);

                                        foreach ($allData as $modelRelated) {
                                            $modelRedis = Yii::createObject($modelConfig['class']);
                                            $modelRedis->setAttributes($modelRelated->getAttributes(), false);
                                            $modelRedis->transaction = $this->transactionId;
                                            $modelRedis->save();
                                        }
                                    }

                                    /**
                                     * Creamos dataProvider y searchModel
                                     */

                                    $this->instanceDataProviderGrid($modelConfig['searchClass'], $keyModel, $modelObject, $modelConfig['isRedis']);
                                    break;
                                }
                            }
                        }

                        $modelObject[$keyModel] = [];
                    } else if ($modelConfig['isMultiple'] && $modelConfig['renderGrid']) {

                        $this->instanceDataProviderGrid($modelConfig['searchClass'], $keyModel, $modelObject, false);
                        $modelObject[$keyModel] = [];

                        // if (isset($modelConfig['relationName'])) {
                        //     $modelObject[$keyModel] = $modelObject[$modelConfig['relatedModel']]->{$modelConfig['relationName']};
                        // } else {
                        //     $modelObject[$keyModel] = $currentModelObject->all();
                        // }
                        // foreach ($modelObject[$keyModel] as $keyCurrentModel => $currentModel) {
                        //     Yii::configure($modelObject[$keyModel][$keyCurrentModel], $modelConfig['configUpdateInstance'] ?? []);
                        // }
                    } else if ($modelConfig['isMultiple']) {
                        if (isset($modelConfig['relationName'])) {
                            $modelObject[$keyModel] = $modelObject[$modelConfig['relatedModel']]->{$modelConfig['relationName']};
                        } else {
                            $modelObject[$keyModel] = $currentModelObject->all();
                        }
                        foreach ($modelObject[$keyModel] as $keyCurrentModel => $currentModel) {
                            Yii::configure($modelObject[$keyModel][$keyCurrentModel], $modelConfig['configUpdateInstance'] ?? []);
                        }
                    } else {

                        if (isset($modelConfig['relationName'])) {
                            $modelObject[$keyModel] = $modelObject[$modelConfig['relatedModel']]->{$modelConfig['relationName']};
                        } else if ($modelConfig['relatedModels']) {
                            foreach ($modelConfig['relatedModels'] as $keyModelRelated => $relatedConfig) {
                                if (isset($relatedConfig['isModelLoad']) && $relatedConfig['isModelLoad']) {
                                    $modelObject[$keyModel] = $modelObject[$keyModelRelated]->{$relatedConfig['relationName']};
                                }
                            }
                        } else {
                            $modelObject[$keyModel] = $currentModelObject->one();
                        }

                        if (!isset($modelObject[$keyModel]) || empty($modelObject[$keyModel])) {
                            $modelObject[$keyModel] = $modelObjectAct;
                        }

                        Yii::configure($modelObject[$keyModel], $modelConfig['configUpdateInstance'] ?? []);
                    }
                }

                /**
                 * Inicio carga modelo realcionado simple
                 */
                if (isset($modelConfig['relatedModel']) && isset($this->modelClass[$modelConfig['relatedModel']]['loadOnRelated']) && $this->modelClass[$modelConfig['relatedModel']]['loadOnRelated']) {

                    $modelObjectAct = Yii::createObject($this->modelClass[$modelConfig['relatedModel']]['class']);
                    $conditions = Yii::$app->arrayHelper::merge([
                        'active' => 'Y'
                    ], $this->modelClass[$modelConfig['relatedModel']]['conditions'] ?? []);

                    foreach ($modelConfig['relatedFieldsCondition'] ?? [] as $fieldModel => $fieldParent) {
                        $conditions[$fieldParent] = $modelObject[$keyModel]->{$fieldModel};
                    }

                    $currentModelObject = $modelObjectAct::find()->where($conditions);

                    if (isset($modelConfig['relationName'])) {
                        $modelObject[$modelConfig['relatedModel']] = $modelObject[$keyModel]->{$modelConfig['relationName']};
                    } else {
                        $modelObject[$modelConfig['relatedModel']] = $currentModelObject->one();
                    }

                    if (!$modelObject[$modelConfig['relatedModel']]) {
                        $modelObject[$modelConfig['relatedModel']] = $modelObjectAct;
                    }

                    Yii::configure($modelObject[$modelConfig['relatedModel']], $this->modelClass[$modelConfig['relatedModel']]['configUpdateInstance'] ?? []);
                }
                /**
                 * Fin carga modelo realcionado simple
                 */

                /**
                 * Inicio carga modelo realcionado avanzado
                 */

                if ($modelConfig['relatedModels']) {

                    foreach ($modelConfig['relatedModels'] as $relatedModelkey => $relatedModelconfig) {

                        if (isset($this->modelClass[$relatedModelkey]['loadOnRelated']) && $this->modelClass[$relatedModelkey]['loadOnRelated'] && isset($relatedModelconfig['loadRelated']) && $relatedModelconfig['loadRelated']) {

                            $modelObjectAct = Yii::createObject($this->modelClass[$relatedModelkey]['class']);
                            $conditions = Yii::$app->arrayHelper::merge([
                                'active' => 'Y'
                            ], $this->modelClass[$relatedModelkey]['conditions'] ?? []);

                            foreach ($relatedModelconfig['relatedFieldsCondition'] ?? [] as $fieldModel => $fieldParent) {
                                $conditions[$fieldParent] = $modelObject[$keyModel]->{$fieldModel};
                            }

                            $currentModelObject = $modelObjectAct::find()->where($conditions);

                            if (isset($relatedModelconfig['relationName'])) {
                                $modelObject[$relatedModelkey] = $modelObject[$keyModel]->{$relatedModelconfig['relationName']};
                            } else {
                                $modelObject[$relatedModelkey] = $currentModelObject->one();
                            }

                            if (!$modelObject[$relatedModelkey]) {
                                $modelObject[$relatedModelkey] = $modelObjectAct;
                            }

                            Yii::configure($modelObject[$relatedModelkey], $this->modelClass[$relatedModelkey]['configUpdateInstance'] ?? []);
                        }
                    }
                }

                /**
                 * Fin carga modelo realcionado avanzado
                 */
            }

            Yii::$app->session->set("saveaction.{$this->transactionId}", [
                'init' => true,
                't' => $this->transactionId
            ]);
        } else {
            $modelObject = $this->findModel($id);
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $allModels
     * @param [type] $id
     * @return void
     */
    private function initModels(&$allModels, $id)
    {
        if ($this->isNewRecord) {
            $this->loadNewInstances($allModels);
        } else {
            $this->loadUpdateInstances($allModels, $id);
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $searchClass
     * @param [type] $keyModel
     * @return void
     */
    private function instanceDataProviderGrid($searchClass, $keyModel, $allModels, $isRedis = false)
    {
        $searchModel = Yii::createObject($searchClass);
        if ($isRedis) {
            $searchModel->transaction = $this->transactionId;
        }
        // if (!$isRedis) {
        $this->loadRelatedData($searchModel, $allModels, $this->modelClass[$keyModel]);
        // }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if ($this->isNewRecord && !$isRedis) {
            $dataProvider->query->andWhere('1=0');
        }

        $this->modelClass[$keyModel]['searchModel'] = $searchModel;
        $this->modelClass[$keyModel]['dataProvider'] = $dataProvider;
    }

    /**
     * Undocumented function
     *
     * @param [type] $allModels
     * @return void
     */
    private function loadNewInstances(&$allModels)
    {
        if ($this->isMultiModel) {
            foreach ($this->modelClass as $keyModel => $modelConfig) {
                if ($modelConfig['isMultiple']) {
                    $allModels[$keyModel] = [];
                    if ($modelConfig['isRedis'] || $modelConfig['renderGrid']) {
                        $this->instanceDataProviderGrid($modelConfig['searchClass'], $keyModel, $allModels, $modelConfig['isRedis']);
                    }
                } else {
                    $allModels[$keyModel] = Yii::createObject($modelConfig['class']);
                    Yii::configure($allModels[$keyModel], $modelConfig['configNewInstance'] ?? []);
                }
            }
        } else {
            $allModels = Yii::createObject($this->modelClass);
        }
    }

    /**
     * Cargamos los atributos del modelo principal pasados por metodo GET
     *
     * @param [type] $allModels
     * @return void
     */
    private function loadGetData(&$allModels)
    {
        $paramsGet = Yii::$app->request->get();
        if (!empty($paramsGet)) {
            if ($this->isMultiModel) {
                foreach ($this->modelClass as $keyModel => $modelConfig) {
                    if ($modelConfig['isPrimary']) {
                        $allModels[$keyModel]->setAttributes($paramsGet);
                    }
                    if (!is_array($allModels[$keyModel])) {
                        $allModels[$keyModel]->load($paramsGet);
                    }
                }
            } else {
                $allModels->setAttributes($paramsGet);
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $model
     * @param [type] $allModels
     * @param [type] $relatedFields
     * @param [type] $relatedModel
     * @return void
     */
    private function loadRelatedFields(&$model, $allModels, $relatedFields, $relatedModel)
    {
        $result = true;
        try {
            foreach ($relatedFields ?? [] as $fieldModel => $fieldParent) {
                $model->{$fieldModel} = $allModels[$relatedModel]->{$fieldParent};
            }
        } catch (\Throwable $th) {
            $result = false;
            throw $th;
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param [type] $model
     * @param [type] $allModels
     * @param [type] $config
     * @return void
     */
    private function loadRelatedData(&$model, $allModels, $config)
    {
        $result = true;
        try {
            if (isset($config['relatedFields']) && isset($config['relatedModel'])) {
                $this->loadRelatedFields($model, $allModels, $config['relatedFields'], $config['relatedModel']);
            }
            if (isset($config['relatedModels']) && !empty($config['relatedModels'])) {
                foreach ($config['relatedModels'] as $relatedModelKey => $relatedconfig) {
                    if (isset($relatedconfig['relatedFields']) && !empty($relatedconfig['relatedFields'])) {
                        $this->loadRelatedFields($model, $allModels, $relatedconfig['relatedFields'], $relatedModelKey);
                    }
                }
            }
        } catch (\Throwable $th) {
            $result = false;
            throw $th;
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param [type] $modelRedisClass
     * @param [type] $allModels
     * @param [type] $keyModel
     * @return void
     */
    private function loadRedisData($modelRedisClass, &$allModels, $keyModel)
    {
        try {
            $modelObjectRedis = Yii::createObject($modelRedisClass);
            $dataRedis = $modelObjectRedis::find()->where(['transaction' => $this->transactionId])->all();
            $count = count($dataRedis);
            $models = [];
            for ($i = 0; $i < $count; $i++) {
                $models[$i] = Yii::createObject($modelObjectRedis::$modelClass);
                $models[$i]->setAttributes($dataRedis[$i]->getAttributes(), false);
                $primaryField = $models[$i]->primaryKey()[0];
                if (empty($models[$i]->{$primaryField})) {
                    unset($models[$i]->{$primaryField});
                }
            }
            $allModels[$keyModel] = $models;
            unset($modelObjectRedis, $dataRedis, $count, $models);
            $result = true;
        } catch (\Throwable $th) {
            $result = false;
            throw $th;
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @param [type] $modelClass
     * @param [type] $allModels
     * @param [type] $keyModel
     * @return void
     */
    private function loadPostData($modelClass, &$allModels, $keyModel)
    {
        $result = true;
        try {
            $modelObjectMultipleInstance = Yii::createObject($modelClass);
            $formName = $modelObjectMultipleInstance->formName();
            $count = count(Yii::$app->request->post($formName, []));
            $models = [];
            for ($i = 0; $i < $count; $i++) {
                $models[] = Yii::createObject($modelClass);
            }
            $result &= Model::loadMultiple($models, Yii::$app->request->post());
            $allModels[$keyModel] = $models;
            unset($modelObjectMultipleInstance, $formName, $count, $models);
        } catch (\Throwable $th) {
            $result = false;
            throw $th;
        }
        return $result;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    private function clearRedisData()
    {
        $result = true;
        try {

            foreach ($this->modelClass as $keyModel => $config) {

                if (isset($config['isMultiple']) && $config['isMultiple'] && isset($config['isRedis']) && $config['isRedis']) {
                    $modelObject = Yii::createObject($config['class']);
                    $modelObject->deleteAll(
                        ['transaction' => $this->transactionId]
                    );
                }
            }
        } catch (\Throwable $th) {
            $result = false;
            throw $th;
        }
        return $result;
    }
}
