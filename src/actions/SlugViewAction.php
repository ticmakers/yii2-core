<?php

namespace ticmakers\core\actions;

use Yii;
use ticmakers\core\actions\BaseAction;
use yii\helpers\Inflector;
use yii\web\NotFoundHttpException;

/**
 * ViewAction base para los crud del sistema
 *
 * @package ticmakers
 * @subpackage core
 * @category Actions
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class SlugViewAction extends BaseAction
{
    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'view';

    /**
     * Nombre del atributo a usar para la busqueda del registro
     *
     * @var string
     */
    public $slugAttribute = 'slug';


    /**
     * Permite visualziar los datos de un solo registro.
     *
     * @return void
     */
    public function run($slug)
    {

        $modelClass = $this->modelClass;
        if (is_array($this->modelClass)) {
            $primaryClassFound = false;
            foreach ($this->modelClass as $keyModel => $modelConfig) {
                if (isset($modelConfig['isPrimary']) && $modelConfig['isPrimary']) {
                    $modelClass = $modelConfig['class'];
                    $primaryClassFound = true;
                    break;
                }
            }
            if (!$primaryClassFound) {
                throw new InvalidConfigException(Yii::t('app', 'Primary model not found'));
            }
        }


        $model = $modelClass::find()
            ->where([
                $this->slugAttribute => Inflector::slug($slug),
                $modelClass::STATUS_COLUMN => $modelClass::STATUS_ACTIVE
            ])
            ->one();

        $params = ['model' => $model];

        if ($model !== null) {
            return $this->render($this->_viewFile, $params);
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
