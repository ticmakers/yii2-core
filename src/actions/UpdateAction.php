<?php

namespace ticmakers\core\actions;

use Yii;

/**
 * UpdateAction base para los crud del sistema
 *
 * @package ticmakers
 * @subpackage core
 * @category Actions
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class UpdateAction extends SaveAction
{
    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'update';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isNewRecord = false;

    /**
     * Undocumented variable
     *
     * @var string
     */
    public $messageOnSuccess = 'It was updated successfully.';
}
