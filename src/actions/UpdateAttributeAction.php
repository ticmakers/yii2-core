<?php

namespace ticmakers\core\actions;

use Yii;
use ticmakers\core\actions\BaseAction;
use ticmakers\core\widgets\ActiveForm;

/**
 * UpdateAttributeAction base para los crud del sistema
 *
 * @package ticmakers
 * @subpackage core
 * @category Actions
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class UpdateAttributeAction extends BaseAction
{

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeSave;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $value;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $attribute;

    /**
     * Permite actualizar el valor de un atributo de un modelo en el sistema
     *
     * @return void
     */
    public function run($id)
    {
        $response = null;
        $modelInstance = $this->findModel($id);
        $modelInstance->{$this->attribute} = $this->value;

        if (!empty($this->beforeSave) && is_callable($this->beforeSave)) {
            call_user_func_array($this->beforeSave, [&$modelInstance]);
        }
        if ($modelInstance->save(true, [$this->attribute])) {
            if ($this->isModal) {
                $response = [
                    'state' => Yii::$app->message::TYPE_SUCCESS,
                    'message' => Yii::t('app', $this->messageOnSuccess),
                    'errors' => '',
                    'type' => 'message', //open-modal, open-load-modal, redirect, message
                    'url' => '',
                    'modal' => ''
                ];
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_SUCCESS, Yii::t('app', $this->messageOnSuccess));
            }
        } else {
            if ($this->isModal) {
                $response = [
                    'state' => Yii::$app->message::TYPE_DANGER,
                    'message' => Yii::$app->html::errorSummary(
                        $modelInstance
                    ),
                    'errors' => ActiveForm::validate($modelInstance),
                    'type' => 'message', //open-modal, open-load-modal, redirect, message
                    'url' => '',
                    'modal' => ''
                ];
            } else {
                Yii::$app->message::setMessage(Yii::$app->message::TYPE_DANGER, Yii::t('app', $this->messageOnError));
            }
        }

        if ($this->redirectOnSuccess) {
            $response = $this->redirect([$modelInstance]);
        }

        return $response;
    }
}
