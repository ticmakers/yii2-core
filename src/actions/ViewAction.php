<?php

namespace ticmakers\core\actions;

use Yii;
use ticmakers\core\actions\BaseAction;

/**
 * ViewAction base para los crud del sistema
 *
 * @package ticmakers
 * @subpackage core
 * @category Actions
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ViewAction extends BaseAction
{
    /**
     * Vista a renderizar
     *
     * @var string
     */
    public $viewName = 'view';

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeRender;

    /**
     * Permite visualziar los datos de un solo registro.
     *
     * @return void
     */
    public function run($id)
    {
        $params = [
            'model' => $this->findModel($id),
        ];

        $this->runCallback($this->beforeRender, [&$params]);

        return $this->render($this->_viewFile, $params);
    }
}
