/**
 * Script para el control de las funcionalidades del código generado.
 * 
 * @author Daniel Julian Sanchez <daniel.sanchez@ticmakers.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @author Juan Sebastian Muñoz <juan.munoz@ticmakers.com>
 */
var modalOpen = null
// Identificador del elemento tipo modal que se utilizará en la aplicación.
var modalLoad = 'default-modal';

$(function () {
    yii.allowAction = function ($e) {
        var message = $e.data('confirm');
        return message === undefined || yii.confirm(message, $e);
    };
    yii.confirm = function (message, success) {
        let element = $(this)
        bootbox.confirm(message, function (confirmed) {
            if (confirmed) {
                if (element.hasClass('no-validate')) {
                    window.location.href = element.attr('href')
                } else {
                    success();
                }
            }
        });
        return false;
    }
    $('.breadcrumb li:first-child a').prepend("<i class='fa fa-home'></i> ");
    registerTooltipPopover();
    // Set default action to modals
    $('.modal').each((index, element) => {
        $(element).on('show.bs.modal', () => {
            modalOpen = $(element)
            registerTooltipPopover();
        })
    })

    $('.modal .confirm-close').each((index, element) => {
        $(element).on('click', (ev) => {
            ev.preventDefault()
            ev.stopPropagation()
            let message = ($(element).attr('data-message')) ? $(element).attr('data-message') : '¿Está seguro que desea cancelar?'
            bootbox.confirm(message, (confirm) => {
                if (confirm) {
                    modalOpen.modal('hide')
                    modalOpen.find('.modal-body').html('')
                }
            })
        })
    })

    // Set location
    bootbox.addLocale('yii2-es', {
        OK: 'Aceptar',
        CANCEL: 'Cancelar',
        CONFIRM: 'Aceptar'
    })
    // Set default configuration to bootbox
    bootbox.setDefaults({
        locale: 'yii2-es'
    })

    // Configuración por defecto para los toasr
    toastr.options = {
        "timeOut": "9000"
    }

    $(document).on('click', 'a.reload-gridview', function (e) {
        e.preventDefault();
        e.stopPropagation();
        let $this = $(this),
            $href = $this.attr('href'),
            id = $this.data('gridview'),
            message = $this.data('message');

        if (message) {
            bootbox.confirm(message, (confirm) => {
                if (confirm) {
                    $.ajax({
                        url: $href,
                        type: 'POST',
                        contentType: false, // The content type used when sending data to the server.
                        cache: false, // To unable request pages to be cached
                        processData: false,
                        beforeSend: function () { },
                        success: function (data) {
                            if (data.state == 'success') {
                                showSuccessMessage(data.message);
                                if (typeof callBack == 'function') {
                                    callBack(data);
                                }
                                if (id) {
                                    let pjaxLoad = null
                                    for (let idGrid of id.split(',')) {
                                        if (pjaxLoad != null) {
                                            pjaxLoad.done(() => {
                                                pjaxLoad = $.pjax.reload({
                                                    container: '#dynagrid-' + idGrid + '-pjax'
                                                })
                                            })
                                        } else {
                                            pjaxLoad = $.pjax.reload({
                                                container: '#dynagrid-' + idGrid + '-pjax'
                                            })
                                        }
                                        
                                    }
                                }
                            } else {
                                showErrorMessage(data.message);
                            }
                        },
                        complete: function () { }
                    })
                }
            })
        } else {
            $.ajax({
                url: $href,
                type: 'POST',
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                beforeSend: function () { },
                success: function (data) {
                    if (data.state == 'success') {
                        showSuccessMessage(data.message);
                        if (typeof callBack == 'function') {
                            callBack(data);
                        }
                        if (id) {
                            let pjaxLoad = null
                            for (let idGrid of id.split(',')) {
                                if (pjaxLoad != null) {
                                    pjaxLoad.done(() => {
                                        pjaxLoad = $.pjax.reload({
                                            container: '#dynagrid-' + idGrid + '-pjax'
                                        })
                                    })
                                } else {
                                    pjaxLoad = $.pjax.reload({
                                        container: '#dynagrid-' + idGrid + '-pjax'
                                    })
                                }
                                
                            }
                        }
                    } else {
                        showErrorMessage(data.message);
                    }
                },
                complete: function () { }
            })
        }



    });

    $(document).on('submit', 'form.search-redis', function (event, jqXHR, settings) {

        event.preventDefault(); // stop default submit behavior when it bubbles to <body>
        let form = $(this),
            dynaModalId = form.data('gridview'),
            modalId = '#search-modal-' + dynaModalId;

        $.pjax.submit(event, '#dynagrid-' + dynaModalId + '-pjax');
        $(modalId).modal('hide');

    });

});

$(document).on('pjax:success', function (data, status, xhr, options) {
    if (data.target.attributes.getNamedItem('data-aditional-pjax-reload')) {
        let reload = data.target.attributes.getNamedItem('data-aditional-pjax-reload').value || false;
        if (reload) {
            let gridView = $(data.target.attributes.getNamedItem('id').value)
            $('.loader').hide();
            gridView.removeClass('kv-grid-loading');
            $.pjax.reload({ container: reload, async: false, replaceRedirect: false })
            $('.loader').hide();
            gridView.removeClass('kv-grid-loading');
        }
    }
});


/**
 * Método para mostrar un mensaje de success
 * @param {String} msg
 */
function showSuccessMessage(msg) {
    getMessage('success', msg);
}

/**
 * Función para mostrar un mensaje de error
 * @param {String} msg
 */
function showErrorMessage(msg) {
    getMessage('error', msg);
}


/**
 * Función para mostrar un mensaje de info
 * @param {String} msg
 */
function showInfoMessage(msg) {
    getMessage('info', msg);
}

/**
 * Función para mostrar un mensaje
 * @param {String} type
 * @param {String} message
 */
function getMessage(type, message) {
    toastr.clear();
    toastr[type]('<b>' + message + '</b>');
}

/**
 * Función para usar dentro de los eventos change para combos dependientes
 *
 * @param {Object} padre
 * @param {Object} hijo
 * @param {String} url
 * @param {Object} params
 * @param {Function} callBack
 */
function onChangeCombo(padre, hijo, url, params, callBack) {
    if (!params) {
        params = {
            id: padre.val()
        };
    }

    $.getJSON(url, params,
        function (data) {
            changeCombo(data, hijo, callBack);
        });
}

/**
 * Encargado de rellenar un select con la data indicada
 * @param {Object} data
 * @param {Object} hijo
 * @param {Function} callBack
 */
function changeCombo(data, hijo, callBack) {
    $('select[id$=' + hijo.attr('id') + '] > option').remove();
    hijo.append("<option value=''>" + window.TEXT_EMPTY + "</option>");
    for (var value in data) {
        var name = data[value];
        hijo.append("<option value='" + value + "'>" + name + "</option>");
    }

    if (callBack && typeof (callBack) == 'function') {
        callBack(data, hijo);
    }
}

/**
 * Encargado de rellenar un select agrupado con la data indicada
 * @param {Object} data
 * @param {Object} hijo
 * @param {Function} callBack
 */
function changeComboGroup(data, hijo, callBack) {

    $('select[id$=' + hijo.attr('id') + '] > option').remove();
    hijo.append("<option value=''>" + window.TEXT_EMPTY + "</option>");
    for (var label in data) {
        var dataItems = data[label];
        hijo.append("<optgroup  label='" + label + "'>");
        for (var value in dataItems) {
            var name = dataItems[value];
            hijo.append("<option value='" + value + "'>" + name + "</option>");
        }
        hijo.append("</optgroup>");
    }

    if (callBack && typeof (callBack) == 'function') {
        callBack(data, hijo);
    }
}

/**
 * Función encargada de retornar el valor de un parámetro url
 * @param {String} name
 * @returns {String}
 */
function getParamUrl(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(window.location);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}


/**
 * Función encargada de abrir las modales de CRUD con opción de modal
 * @param {Object} button
 * @param {String} dynaModalId
 * @param {String} template
 */
function openModalGrid(button, dynaModalId, template, callBack) {
    let modalId = '#' + dynaModalId + '-modal';

    if (template == 'create') {
        $(modalId).find('.modal-title').html('Nuevo');
        $('#btn-send-back').show()
    } else if (template == 'update') {
        $(modalId).find('.modal-title').html('Editar');
        $('#btn-send-back').hide()
    }

    if (template == 'view') {
        $(modalId).find('.modal-title').html('Ver');
        $(modalId).find('.modal-footer').hide();
    } else {
        $(modalId).find('.modal-footer').show();
    }

    /**
     * Limpiamos la modal para evitar errores en carga de css y js
     */
    $(modalId).find('.modal-body').html('');

    $(modalId).find('.modal-body').load($(button).attr('href'), function () {
        $(modalId).modal('show');
        registerTooltipPopover();
        $(modalId).find('.modal-body form').on('beforeSubmit', function (event, jqXHR, settings) {
            let form = $(this)
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: new FormData(this),
                contentType: false, // The content type used when sending data to the server.
                cache: false, // To unable request pages to be cached
                processData: false,
                beforeSend: function () {
                    $(modalId).find('button[type="submit"]').prop('disabled', true);
                },
                success: function (data) {
                    if (data.state == 'success') {
                        $(modalId).modal('hide');
                        $(this).off('beforeSubmit');
                        showSuccessMessage(data.message);
                        if (typeof callBack == 'function') {
                            callBack(data);
                        }
                        /**
                        * Limpiamos la modal para evitar errores en carga de css y js
                        */
                        $(modalId).find('.modal-body').html('');
                        $.pjax.reload({
                            container: '#dynagrid-' + dynaModalId + '-pjax'
                        })

                    } else {
                        showErrorMessage(data.message);
                    }
                    if (template == 'create' && form.attr('data-back') == 'true') {
                        setTimeout(function () {
                            openModalGrid(button, dynaModalId, template)
                        }, 1000)
                    }
                },
                complete: function () {
                    $(modalId).find('button[type="submit"]').prop('disabled', false);
                }
            })
            return false
        });

        /**
         * Refresco de evento para los formulario con controles tipo yii2-number
         */
        $(modalId).find('.modal-body form').on('afterValidate', function (e) {
            $('.number-input').each((index, element) => {
                $(element).trigger('change')
            })
        })
    });
}

/**
 * Dispara un mensaje de confirmación justo antes de cerra una ventana modal
 */
function closeModalWithMessage(btn) {
    let message = ($(btn).attr('data-message')) ? $(btn).attr('data-message') : '¿Está seguro que desea cancelar?'
    bootbox.confirm(message, (confirm) => {
        if (confirm) {
            modalOpen.find('.modal-body').html('');
            modalOpen.modal('hide')
        }
    })
}

/**
 * Registro de tooltips y popovers
 */
function registerTooltipPopover() {
    setTimeout(() => {
        /*Tooltip*/
        if ($('[data-toggle="tooltip"]').length > 0) {
            $('[data-toggle="tooltip"]').each(function (index) {
                let $element = $(this);
                $element.tooltip();
            });
        }

        /*Popover*/
        if ($('[data-toggle="popover"]').length > 0) {
            $('[data-toggle="popover"]').each(function (index) {
                let $element = $(this);
                $element.popover();
            });
        }
    }, 500);
}

/**
 * Añadida validación para campos en los cuales existe controles del tipo yii2-number
 */
$(document).ajaxComplete(function () {
    $(this).on('change', '.number-input', function (e) {
        e.stopPropagation();
        let inputOriginal = $(this);
        let id = `#${inputOriginal.prop('id')}-disp`;
        setTimeout(() => {
            let inputDisplaying = $(id);
            inputDisplaying.removeClass('is-valid').removeClass('is-invalid')
            if ((inputOriginal.attr('aria-invalid') && !inputOriginal.hasClass('is-valid')) || (typeof inputOriginal.attr('aria-invalid') == 'undefined')) {
                inputDisplaying.addClass('is-invalid')
            } else {
                inputDisplaying.addClass('is-valid')
            }
        }, 300);
    })
});

/**
 * Define si el valor pasado en el primer parámetro no está vacio y devuelve este valor,
 * de no ser así devuelve el valor por defecto.
 *
 * @param string value
 * @param string defaultValue
 */
function getValue(initialValue, defaultValue) {
    return (initialValue) ? initialValue.trim() : defaultValue;
}

/**
 * Sobreescritura de evento de carga completa de la página.
 */
$(document).ready(function (ev) {

    $(document).ajaxComplete(function () {
        /*Feather Icon*/
        var featherIcon = $('.feather-icon');
        if (featherIcon.length > 0) {
            feather.replace();
        }

        registerTooltipPopover()

        /*Select2*/
        $('select[data-krajee-select2]').each(function (index) {
            let $element = $(this);
            let settings = $element.attr('data-krajee-select2');
            settings = window[settings];
            let id = $element.attr('id');
            $element.select2(settings)
            initS2Loading(id);
        });


    });

    /**
     * Cargar TAB por su hash
     */
    let hash = window.location.hash;
    hash && $('ul.nav a[href="' + hash + '"]').tab('show');


    // let url = window.location.href;
    // axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    // url = new URL(url);
    // if (url.searchParams.get('operation') == 'open-modal') {
    //     openModal(modalLoad, url.searchParams.get('url'));
    // }
    // $(document).ajaxComplete(function () {
    //     organizeModals();
    // });
    // organizeModals();
    // setLinkEvents();

});

/**
 * Permite añadir un campo oculto con su respectivo valor en un formulario
 */
jQuery.fn.addHidden = function (name, value) {
    return this.each(function () {
        var input = $("<input>").attr("type", "hidden").attr("name", name).val(value);
        $(this).append($(input));
    });
};

/**
 * Permite enviar un formulario y regresar al siguiente tab
 * @param {*} btn 
 * @param {*} isBack 
 */
function onclickSend(btn, isBack = true) {
    let form = $('#' + $(btn).attr('form'))
    let tabId = form.data('tab-id')
    if (isBack) {
        form.attr('data-back', 'true')
    }

    if (!tabId) { tabId = '.nav-tabs' }
    let item = $(tabId).find('a.active').parent()
    item = item.next()
    let hrefNextTab = item.find('a').attr('aria-controls');

    form.addHidden('SaveAndReturnTab', hrefNextTab).submit()
}


/**
 * Se sobreescribirán los elementos a para que su contenido cargue en una modal
 * si poseen la clase 'load-modal'
 */
function setLinkEvents() {
    $('a.load-modal').each(function (index, element) {
        $(element).off('click');
        $(element).on('click', function (event) {
            event.preventDefault();
            let url = $(element).attr('href');
            let backUrl = $(element).attr('data-return-page');
            if (backUrl) {
                url += (url.charAt(url.length - 1) == '?') ? '&' : '?';
                url += 'return-page=' + backUrl;
            }
            $(`#${modalLoad}`).modal('hide');
            openModal(modalLoad, url);
        });
    });
}

/**
 * Reorganiza los modales en el DOM
 */
function organizeModals() {
    let container = document.createElement('div');
    $(container).addClass('modals-container');
    $('.modal-dialog').each((index, element) => {
        $(element).parent().appendTo(container);
    });
    if ($('.modals-container')) $('.modals-container').detach();
    $(container).appendTo('body');
}

/**
 * Abre en una modal el contenido cargado por ajax.
 *
 * @param string modalId
 * @param string url
 */
function openModal(modalId, url = null) {
    if (url && url != '#') {
        axios.get(url)
            .then((response) => {
                $(`#${modalId}`).find('.modal-body').html(response.data);
                addInteractions(modalId);
                setLinkEvents();
            })
            .catch((error) => {
                showErrorMessage(error);
            });
    } else {
        addInteractions(modalId);
    }
}

/**
 * Muestra un mensaje
 */
function showMessage(data) {
    let message = (data.errors) ? data.errors : data.message;
    if (message) {
        if (data.state == 'error') {
            showErrorMessage(message)
        } else {
            showSuccessMessage(message)
        }
    }
}

/**
 * Añade las intereacciones para un formulario beforeSubmit, y envío por ajax
 */
function addInteractions(modalId, event = null) {

    console.log('addInteractions');
    let title = getValue($('#modal-title').val(), $('title').text());
    $(`#${modalId}`).find('.modal-title').text(title);
    $(`#${modalId}`).find('.modal-body form').off('beforeSubmit');
    $(`#${modalId}`).find('.modal-body form').on('beforeSubmit', function (ev, jqXHR, settings) {
        let form = $(this);
        $(`#${modalId}`).find('button[type="submit"]').prop('disabled', true);
        axios({
            method: 'post',
            url: form.attr('action'),
            data: new FormData(this),
            config: {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            }
        })
            .then((response) => {
                switch (response.data.type) {
                    case 'open-modal':
                        $(this).off('beforeSubmit');
                        $(`#${modalId}`).modal('hide');
                        $(`#${response.data.modal}`).modal('show');
                        showMessage(response.data);
                        break;
                    case 'open-load-modal':
                        $(this).off('beforeSubmit');
                        $(`#${modalId}`).modal('hide');
                        openModal(response.data.modal, response.data.url);
                        showMessage(response.data);
                        break;
                    case 'redirect':
                        showMessage(response.data);
                        setTimeout(() => {
                            window.location.href = response.data.url;
                        }, 3000);
                        break;
                    case 'message': default:
                        showMessage(response.data);
                        break;
                }
            })
            .catch((error) => {
                showErrorMessage(error);
            })
            .finally(() => {
                $(`#${modalId}`).find('button[type="submit"]').prop('disabled', false);
            });
        return false;
    });

    setTimeout(() => {
        $(`#${modalId}`).modal('show');
    }, 500);
}
