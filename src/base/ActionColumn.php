<?php

namespace ticmakers\core\base;

use Yii;
use kartik\grid\ActionColumn as ActionColumnBase;
use kartik\grid\GridView;

/**
 * ActionColumn personalizado para los CRUDS con modals
 *
 * @package ticmakers
 * @subpackage core
 * @category Components
 *
 * @property string $template Template para los botones
 * @property array $restoreOptions Opciones para el boton de restore
 * @property boolean $isModal Indica si el ActionColumn se usa con ventanas modales
 * @property string $dynaModalId Indica el id para encontrar la modal y el dynagrid para enventos JS en ventanas modales
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ActionColumn extends ActionColumnBase
{

    public $width          = '110px';
    public $template       = '{update} {delete} {restore}';
    public $restoreOptions = [];
    public $isModal        = false;
    public $dynaModalId    = 'default';

    /**
     * Inicializamos los datos del ActionColumn
     *
     * @return void
     */
    public function init()
    {
        $this->hAlign         = GridView::ALIGN_CENTER;
        $this->deleteOptions  = [
            'class' => 'reload-gridview',
            'data-method'  => 'post',
            'data-confirm' => Yii::t(
                'app',
                'Are you sure you want to delete this item?'
            )
        ];
        $this->restoreOptions = [
            'class' => 'reload-gridview',
            'data-method'  => 'post',
            'data-confirm' => Yii::t(
                'app',
                'Are you sure you want to restore this item?'
            )
        ];

        parent::init();
    }

    /**
     * Asignar una configuración por defecto al botón basado en su $name (es diferente al método [[initDefaultButton]])
     *
     * @param string $name Nombre del botón escribo en [[template]]
     * @param string $title Título de el botón
     * @param string $icon Icono por defecto del botón
     */
    protected function setDefaultButton($name, $title, $icon)
    {
        if (isset($this->buttons[$name])) {
            return;
        }
        $this->buttons[$name] = function ($url, $data) use ($name, $title, $icon) {
            $opts      = "{$name}Options";
            $options   = ['title' => $title, 'aria-label' => $title, 'data-pjax' => '0'];
            $className = $data::className();

            if (defined("{$className}::STATUS_COLUMN")) {

                if ($name === 'delete' && $data->{$data::STATUS_COLUMN} == $data::STATUS_INACTIVE) {
                    return;
                } elseif ($name === 'restore' && $data->{$data::STATUS_COLUMN} == $data::STATUS_ACTIVE) {
                    return;
                }
            }


            if ($this->isModal && !empty($this->dynaModalId) && $name !== 'delete' && $name !== 'restore') {
                $options['onclick'] = "openModalGrid(this, '{$this->dynaModalId}', '{$name}'); return false;";
            }

            $options = array_replace_recursive(
                $options,
                $this->buttonOptions,
                $this->$opts
            );

            if ($this->isModal && !empty($this->dynaModalId) && ($name == 'delete' || $name == 'restore')) {
                $options['data-gridview'] = $this->dynaModalId;
                $options['class'] = 'reload-gridview';
                $options['data-message'] = $options['data-confirm'];
                unset($options['data-confirm'], $options['data-method']);
            }

            $label   = $this->renderLabel(
                $options,
                $title,
                ['class' => "fa fa-{$icon}"]
            );
            $link    = Yii::$app->html::a($label, $url, $options);
            if ($this->_isDropdown) {
                $options['tabindex'] = '-1';
                return "<li>{$link}</li>\n";
            } else {
                return $link;
            }
        };
    }

    /**
     * Inicializamos los buttons por defecto
     */
    protected function initDefaultButtons()
    {
        $this->setDefaultButton('view', Yii::t('app', 'View'), Yii::$app->html::ICON_EYE);
        $this->setDefaultButton(
            'update',
            Yii::t('app', 'Edit'),
            Yii::$app->html::ICON_PENCIL
        );
        $this->setDefaultButton(
            'delete',
            Yii::t('app', 'Delete'),
            Yii::$app->html::ICON_TRASH
        );
        $this->setDefaultButton(
            'restore',
            Yii::t('app', 'Restore'),
            Yii::$app->html::ICON_REPEAT
        );
    }
}
