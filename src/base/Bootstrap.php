<?php

namespace ticmakers\core\base;

/**
 * Boostrap base para las aplicaciones.
 * 
 * @package ticmakers
 * @subpackage core\base
 * @category Bootstrap
 * 
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @version 1.0.0
 * @since 0.0.1
 */
class Bootstrap implements \yii\base\BootstrapInterface
{
    public $moduleId;
    public $pieces = [
        'modules' => [],
        'components' => [],
        'models' => []
    ];

    /**
     * Permite cargar las clase asociadas a los componentes, modulos y modelos
     *
     * @param Module $app
     * @return void
     */
    public function loadPieces($app)
    {
        foreach ($this->pieces as $key => $pieces) {
            foreach ($pieces as $keyPiece => $piece) {
                if ($key == 'modules' && !$app->hasModule($keyPiece)) {
                    $app->setModule($keyPiece, $piece);
                }
                if ($key == 'components' && !$app->has($keyPiece)) {
                    $app->set($keyPiece, $piece);
                }
            }
        }
    }

    /**
     * Bootstrap method to be called during application bootstrap stage.
     *
     * @param Application $app the application currently running
     */
    public function bootstrap($app)
    {
        if (is_null($this->moduleId)) {
            throw new \yii\base\InvalidConfigException(Yii::t('app', 'moduleId is not defined'));
        }
        $this->loadPieces($app);
    }
}
