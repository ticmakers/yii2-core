<?php

namespace ticmakers\core\base;

use ticmakers\core\actions\BaseAction;
use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller as ControllerBase;
use ticmakers\core\helpers\ConstantsHelper as C;
use ticmakers\core\actions\DepDropAction;
use ticmakers\core\actions\IndexAction;
use ticmakers\core\actions\CreateAction;
use ticmakers\core\actions\DeleteAction;
use ticmakers\core\actions\ViewAction;
use ticmakers\core\actions\UpdateAction;
use ticmakers\core\actions\RestoreAction;
use ticmakers\core\actions\SaveAction;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;

/**
 * Controlador base para todas las CRUD generales
 *
 * @package ticmakers\core\base\Controller
 *
 * @property string $searchModel Ruta del modelo para la búsqueda.
 * @property string $model Ruta del modelo principal.
 * @property string $layout PATH del layout utilizado por defecto.
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Controller extends ControllerBase
{
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $searchModelClass;
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $modelClass;
    /**
     * Undocumented variable
     *
     * @var string
     */
    public $layout = '@theme/views/layouts/main';
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeSearchIndex;
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $beforeRenderIndex;
    /**
     * Undocumented variable
     *
     * @var array
     */
    public $actionsConfig = [];
    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public $depDropActionClass;
    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $isModal = false;

    /**
     * Permite establecer las migas de pan base para el controlador
     *
     * @var array
     */
    public $breadcrumbsBase = [];

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $formInTabs = false;

    /**
     * Permite establecer la configuración para el renderizado de los formularios en tabs
     *
     * @var array
     */
    public $tabsConfig = [];

    /**
     * Permite definir el título del módulo al cual se encuentra asociado el controlador
     *
     * @var string
     */
    public $moduleTitle;

    /**
     * Undocumented function
     *
     * @return void
     */
    public function actions()
    {

        $defaultActions = [
            'index' => [
                C::CLASS_NAME => IndexAction::class,
                C::IS_MODAL => $this->isModal,
                C::MODEL_CLASS => $this->searchModelClass
            ]
        ];

        if ($this->modelClass) {
            $defaultActions = Yii::$app->arrayHelper::mergeMultiple(
                $defaultActions,
                [
                    'view' => [
                        C::CLASS_NAME => ViewAction::class,
                        C::IS_MODAL => $this->isModal,
                        C::MODEL_CLASS => $this->modelClass
                    ],
                    'delete' => [
                        C::CLASS_NAME => DeleteAction::class,
                        C::IS_MODAL => $this->isModal,
                        C::MODEL_CLASS => $this->modelClass,
                        'redirectOnSuccess' => !$this->isModal
                    ],
                    'restore' => [
                        C::CLASS_NAME => RestoreAction::class,
                        C::IS_MODAL => $this->isModal,
                        C::MODEL_CLASS => $this->modelClass,
                        'redirectOnSuccess' => !$this->isModal
                    ],
                    'update' => [
                        C::CLASS_NAME => UpdateAction::class,
                        C::IS_MODAL => $this->isModal,
                        C::MODEL_CLASS => $this->modelClass,
                        'viewName' => $this->isModal ? '_form' : 'update',
                        'formInTabs' => $this->formInTabs,
                        'tabsConfig' => $this->tabsConfig
                    ],
                    'create' => [
                        C::CLASS_NAME => CreateAction::class,
                        C::IS_MODAL => $this->isModal,
                        C::MODEL_CLASS => $this->modelClass,
                        'viewName' => $this->isModal ? '_form' : 'create',
                        'formInTabs' => $this->formInTabs,
                        'tabsConfig' => $this->tabsConfig
                    ]
                ]
            );
        }

        $this->verifyDefaultActions($defaultActions);


        if (empty($this->depDropActionClass)) {
            $this->depDropActionClass = DepDropAction::class;
        }
        $parentActions = parent::actions();
        $depDropActions = [];

        if ($this->modelClass) {
            $modelObject = \Yii::createObject($this->_getPrimaryModelClass());
            if ($modelObject) {
                foreach ($modelObject->depDropDependencies()
                    as $actionId => $dDropConfig) {
                    if (!isset($dDropConfig[C::CLASS_NAME])) {
                        $dDropConfig[C::CLASS_NAME] = $this->depDropActionClass;
                    }
                    $depDropActions[$actionId] = $dDropConfig;
                }
            }
        }
        return Yii::$app->arrayHelper::mergeMultiple(
            $defaultActions,
            $parentActions,
            $depDropActions,
            $this->actionsConfig
        );
    }

    /**
     * @todo Documentar
     *
     */
    public function beforeSearchIndex(&$searchModel)
    {
        if ($this->beforeSearchIndex) {
            call_user_func_array($this->beforeSearchIndex, [&$searchModel]);
        }
    }
    /**
     * @todo Documentar
     *
     */
    public function beforeRenderIndex(&$viewFile, &$params)
    {
        if ($this->beforeRenderIndex) {
            call_user_func_array($this->beforeRenderIndex, [
                &$viewFile,
                &$params
            ]);
        }
    }

    /**
     * Método para sobreescribir comportamientos antes de las acciones
     *
     * @param \yii\base\Action $action
     * @return boolean
     */
    public function beforeAction($action)
    {
        // Yii::$container->set(
        //     'kartik\grid\GridView',
        //     Yii::$app->ui::getDefaultConfigGridView()
        // );
        // Yii::$container->set(
        //     'kartik\dynagrid\DynaGrid',
        //     Yii::$app->ui::getDefaultConfigDynaGrid()
        // );
        Yii::$container->set(
            'kartik\widgets\TimePicker',
            Yii::$app->ui::getDefaultConfigTimePicker()
        );
        Yii::$container->set(
            'kartik\time\TimePicker',
            Yii::$app->ui::getDefaultConfigTimePicker()
        );
        Yii::$container->set(
            'kartik\widgets\DatePicker',
            Yii::$app->ui::getDefaultConfigDatePicker()
        );
        Yii::$container->set(
            'kartik\date\DatePicker',
            Yii::$app->ui::getDefaultConfigDatePicker()
        );
        Yii::$container->set(
            'kartik\widgets\DateTimePicker',
            Yii::$app->ui::getDefaultConfigDateTimePicker()
        );
        Yii::$container->set(
            'kartik\datetime\DateTimePicker',
            Yii::$app->ui::getDefaultConfigDateTimePicker()
        );
        Yii::$container->set(
            'kartik\widgets\Select2',
            Yii::$app->ui::getDefaultConfigSelect2()
        );
        Yii::$container->set(
            'kartik\select2\Select2',
            Yii::$app->ui::getDefaultConfigSelect2()
        );
        Yii::$container->set(
            'kartik\dialog\Dialog',
            Yii::$app->ui::getDefaultConfigDialog()
        );
        Yii::$app->session->open();

        $this->getView()->params['breadcrumbs'] = [];

        if ($this->moduleTitle) {
            $this->moduleTitle = Yii::t('app', $this->moduleTitle);
            $this->getView()->params['breadcrumbs'][] = $this->moduleTitle;
            $this->getView()->moduleTitle = $this->moduleTitle;
        }

        $this->getView()->params['breadcrumbs'] = Yii::$app->arrayHelper::merge($this->getView()->params['breadcrumbs'], $this->breadcrumbsBase);

        $request = Yii::$app->request;
        $queryParams = $request->getQueryParams();

        if (!isset($queryParams['t']) && !$request->isAjax) {

            $transaction = \Yii::$app->security->generateRandomString(8);
            $request->setQueryParams(Yii::$app->arrayHelper::merge(['t' => $transaction], $request->getQueryParams()));

            if ($action instanceof BaseAction) {
                Yii::$app->getView()->registerJs(
                    '
                const params = new URLSearchParams(location.search);
                params.set("t", "' . $transaction . '");
                window.history.replaceState({}, "", decodeURIComponent(`${location.pathname}?${params}`))',
                    \yii\web\View::POS_READY
                );
            }
        }

        return parent::beforeAction($action);
    }

    /**
     * Configuración de comportamientos
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                    'restore' => ['POST']
                ]
            ]
        ];
    }

    /**
     * Permite verificar y eliminar las acciones si ya estan definidas en la instancia actual
     *
     * @param array $defaultActions
     * @return void
     */
    private function verifyDefaultActions(&$defaultActions)
    {

        if (method_exists($this, 'actionView')) {
            unset($defaultActions['view']);
        }
        if (method_exists($this, 'actionDelete')) {
            unset($defaultActions['delete']);
        }
        if (method_exists($this, 'actionRestore')) {
            unset($defaultActions['restore']);
        }
        if (method_exists($this, 'actionUpdate')) {
            unset($defaultActions['update']);
        }
        if (method_exists($this, 'actionCreate')) {
            unset($defaultActions['create']);
        }
        if (method_exists($this, 'actionIndex')) {
            unset($defaultActions['index']);
        }
    }

    /**
     * Busca un registro basado en su llave primaria.
     * Si el registro no existe arroja un error HTTP 404.
     *
     * @param integer $id
     * @return Model con el registro.
     * @throws NotFoundHttpException is el registro no es encontrado.
     */
    public function findModel($id)
    {
        $modelObject = Yii::createObject($this->_getPrimaryModelClass());
        if (($modelInstance = $modelObject::findOne($id)) !== null) {
            return $modelInstance;
        } else {
            throw new NotFoundHttpException(Yii::t(
                'app',
                'The requested page does not exist.'
            ));
        }
    }

    protected function _getPrimaryModelClass()
    {
        if (is_array($this->modelClass)) {
            foreach ($this->modelClass as $modelConfig) {
                if (isset($modelConfig['isPrimary']) && $modelConfig['isPrimary']) {
                    $modelClass = $modelConfig['class'];
                    break;
                }
            }
        } else {
            $modelClass = $this->modelClass;
        }

        if (!$modelClass) {
            throw new NotFoundHttpException(Yii::t(
                'app',
                'Invalid configuration'
            ));
        }
        return $modelClass;
    }

    /**
     * Permite cargar las instacias de los archivos cargados desde el cliente
     *
     * @param [type] $modelObject Modelo en el cual se procesara el archivo y/o archivos cargadas
     * @return void
     */
    public function loadFileAttributes(&$modelObject)
    {
        if (
            method_exists($modelObject, 'fileAttributes') &&
            count($modelObject->fileAttributes()) > 0
        ) {
            foreach ($modelObject->fileAttributes() as $fileIndex => $config) {
                if (isset($config['multiple']) && $config['multiple']) {
                    $modelObject->{$fileIndex} = UploadedFile::getInstances(
                        $config['model'],
                        $fileIndex
                    );
                } else {
                    $modelObject->{$fileIndex} = UploadedFile::getInstance(
                        $config['model'],
                        $fileIndex
                    );
                }
                $modelObject->upload();
            }
        }
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function loadViewFile($viewName, $commonViewPath = '@ticmakers/core/views/common')
    {
        $_viewFile = $this->getViewPath() . DIRECTORY_SEPARATOR . $viewName . '.' . $this->view->defaultExtension;
        if (is_file($_viewFile)) {
            $_viewFile = $viewName;
        } else {
            $_viewFile = $commonViewPath . DIRECTORY_SEPARATOR . $viewName . '.' . $this->view->defaultExtension;
        }
        return $_viewFile;
    }
}
