<?php

namespace ticmakers\core\base;

use Yii;
use yii\bootstrap4\Modal as Bootstrap4Modal;

/**
 * Modelo base
 *
 * @package ticmakers
 * @subpackage core
 * @category Components
 *
 * @property string $titleReportExport Almacena el nombre del reporte generado con kartik-yii2-export
 * @property-read string STATUS_ACTIVE Estado activo según la columna.
 * @property-read string STATUS_INACTIVE Estado inactivo según la columna.
 * @property-read string STATUS_COLUMN Nombre de columna de estado.
 * @property-read string CREATED_AT_COLUMN Nombre de columna de Creado por.
 * @property-read string CREATED_DATE_COLUMN Nombre de columna de Fecha creación.
 * @property-read string UPDATED_AT_COLUMN Nombre de columna de Modificado por.
 * @property-read string UPDATED_DATE_COLUMN Nombre de columna de Fecha modificación.
 *
 * @property \yii\web\User $creadoPor
 * @property \yii\web\User $modificadoPor
 *
 * @author  Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Modal extends Bootstrap4Modal
{
    public $header;

    /**
     * Get the value of header
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set the value of header
     *
     * @return  static
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Initializes the widget.
     */
    public function init()
    {
        $this->title = $this->header;
        parent::init();

    }
}
