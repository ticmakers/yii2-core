<?php

namespace ticmakers\core\base;

use kartik\grid\GridView;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\Inflector;
use ticmakers\core\helpers\ConstantsHelper as C;
use yii\db\Schema;

/**
 * Modelo base
 *
 * @package ticmakers
 * @subpackage core
 * @category Components
 *
 * @property string $titleReportExport Almacena el nombre del reporte generado con kartik-yii2-export
 * @property-read string STATUS_ACTIVE Estado activo según la columna.
 * @property-read string STATUS_INACTIVE Estado inactivo según la columna.
 * @property-read string STATUS_COLUMN Nombre de columna de estado.
 * @property-read string CREATED_BY_COLUMN Nombre de columna de Creado por.
 * @property-read string CREATED_AT_COLUMN Nombre de columna de Fecha creación.
 * @property-read string UPDATED_BY_COLUMN Nombre de columna de Modificado por.
 * @property-read string UPDATED_AT_COLUMN Nombre de columna de Fecha modificación.
 *
 * @property \yii\web\User $creadoPor
 * @property \yii\web\User $modificadoPor
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Model extends ActiveRecord
{
    const STATUS_ACTIVE = 'Y';
    const STATUS_INACTIVE = 'N';
    const STATUS_COLUMN = 'active';
    const CREATED_BY_COLUMN = 'created_by';
    const CREATED_AT_COLUMN = 'created_at';
    const UPDATED_BY_COLUMN = 'updated_by';
    const UPDATED_AT_COLUMN = 'updated_at';
    const DEFAULT_USER_ID = 1;

    const RENDER_CREATE = 'C';
    const RENDER_UPDATE = 'U';
    const RENDER_SEARCH = 'S';
    const RENDER_GRID = 'G';

    public $classNames = [];

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $includeActionColumns = true;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    public static $titleReportExport = null;

    /**
     * Modulo padre de la instancia actual
     */

    public $module;

    /**
     * @var int a counter used to generate [[id]] for forms.
     * @internal
     */
    public static $counter = 0;

    /**
     * Undocumented variable
     *
     * @var [type]
     */
    private $_dbFields;


    /**
     * Configuración inicial.
     *
     * @return null
     */
    public function init()
    {
        parent::init();
        $this->{$this::STATUS_COLUMN} = static::STATUS_ACTIVE;
    }

    /**
     * Configuración de los comportamientos por defecto de la aplicación
     *
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => static::CREATED_BY_COLUMN,
                'updatedByAttribute' => static::UPDATED_BY_COLUMN,
                'defaultValue' => (isset(Yii::$app->params['defaultUserId'])) ? Yii::$app->params['defaultUserId'] : static::DEFAULT_USER_ID,
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => [static::CREATED_AT_COLUMN, static::UPDATED_AT_COLUMN],
                    ActiveRecord::EVENT_BEFORE_UPDATE => [static::UPDATED_AT_COLUMN],
                    // 'createdAtAttribute' => static::CREATED_AT_COLUMN,
                    // 'updatedAtAttribute' => static::UPDATED_AT_COLUMN,
                ],
                'value' => date('Y-m-d H:i:s'),
            ],
        ];
    }

    /**
     * Método encargado de entregar el nombre de la columna para relaciones
     *
     * @return string
     */
    public static function getNameFromRelations()
    {
        foreach (static::getTableSchema()->columns as $column) {
            if (!$column->isPrimaryKey) {
                return $column->name;
            }
        }
    }

    /**
     * Sobreescribimos para quitar campos de auditoria
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();

        unset($fields[static::STATUS_COLUMN], $fields[static::CREATED_BY_COLUMN],
        $fields[static::CREATED_AT_COLUMN],
        $fields[static::UPDATED_BY_COLUMN],
        $fields[static::UPDATED_AT_COLUMN]);

        return $fields;
    }

    /**
     * Permite establecer la configuración para las acciones de combos dependientes asociadas al modelo
     * ```
     * return [
     *    'cities' => [
     *        // 'class'=>\kartik\depdrop\DepDropAction::className(), // opcional
     *        'outputCallback' => function ($selectedId, $params) {
     *            $result = [];
     *            $allData = static::getData(false, ['country_id' => $selectedId]);
     *            foreach ($allData as $item) {
     *                $result[] = [
     *                    'id' => $item->primaryKey,
     *                    'name' => $item->name
     *                ];
     *            }
     *            return $result;
     *        },
     *    ]
     * ];
     * ```
     * @return void
     */
    public function depDropDependencies()
    {
        return [];
    }

    /**
     * Define la columna que se mostrará en los formularios para identificar el recurso
     * 
     * @return string
     */
    public static function columnInCrud()
    {
        return static::primaryKey()[0];
    }

    /**
     * Define el nombre que se mostrará en el CRUD.
     * 
     * @return string
     */
    public static function crudTitle()
    {
        return ucfirst(str_replace('_', ' ', static::tableName()));
    }

    /**
     * Define las columnas que serán mostradas en el admin por defecto
     * 
     * @return array
     */
    public function gridColumns()
    {
        $gridColumns = [];
        foreach ($this->dbFields as $attribute => $configAttribute) {

            if (in_array(static::RENDER_GRID, $configAttribute['render'])) {
                $gridColumns[$attribute] = [
                    C::ATTRIBUTE => $attribute,
                    C::VISIBLE => true
                ];

                if ($configAttribute['isFileAttribute']) {
                    $gridColumns[$attribute][C::FORMAT] = 'html';
                    $gridColumns[$attribute][C::VALUE] = function ($model) use ($attribute) {
                        return Yii::$app->html::a(Yii::$app->html::img(
                            $model->{$attribute},
                            [
                                'alt' => $model->{$attribute},
                                'width' => '100px',
                            ]
                        ), $model->{$attribute}, [C::CLASS_NAME => 'gallery-popup-image']);
                    };
                }

                if ($configAttribute['useWidget'] && isset($configAttribute['grid'])) {
                    $gridColumns[$attribute] = Yii::$app->arrayHelper::merge($gridColumns[$attribute], $configAttribute['grid']);
                }
            }
        }
        return $gridColumns;
    }

    /**
     * Define las columnas que serán mostradas en los formularios
     * 
     * @return array
     */
    public function formColumns()
    {
        $this->dbFields;
        $columns = [];
        foreach ($this->dbFields as $key => $value) {
            $attribute = $key;
            $inSearch = [
                static::primaryKey()[0], static::STATUS_COLUMN, static::CREATED_BY_COLUMN, static::CREATED_AT_COLUMN, static::UPDATED_BY_COLUMN, static::UPDATED_AT_COLUMN
            ];
            $columns[$key] = [
                'attribute' => $attribute,
                // 'containerOptions' => ['class' => 'col-12 col-md-6'],
                'onlyInSearch' => in_array($attribute, $inSearch),
                'render' => $value['render'],
                'fieldOptions' => [
                    'tabindex' => $value['tabindex']
                ]
            ];

            if ($value['useWidget']) {
                $columns[$key]['widget'] = $value['widget'];
            }
            if (isset($value['type'])) {
                $columns[$key]['type'] = $value['type'];
            }
            if (isset($value['fieldOptions'])) {
                $columns[$key]['fieldOptions'] = $value['fieldOptions'];
            }
        }

        return $columns;
    }

    /**
     * Definición de atributos tipo archivo
     * 
     * ```
     * [
     *     'profile_photo' => [
     *         'type' => UploadFilesBehavior::TYPE_FILE,
     *         'modelClass' => Members::class,
     *         'value' => $this->getMember(),
     *         'attribute' => 'profile_photo',
     *         'isMultiple' => false,
     *         'allowedFileExtensions' => ['jpg', 'jpeg', 'png'],
     *         'relatedKey' => [
     *             'user_id' => 'user_id',
     *         ],
     *         'relatedAttributes' => [
     *             'user_id' => 'user_id',
     *             'name' => 'name',
     *             'mobile_number' => 'mobile_number',
     *             'document_number' => 'document_number',
     *             'document_type_id' => 'document_type_id',
     *             'address' => 'address',
     *             'city_id'  => 'city_id',
     *             'associated' => function($modelOwner){
     *                  return $modelOwner->associated;
     *              },
     *         ]
     *     ]
     * ];
     * ```
     * 
     * @return array
     */
    public function fileAttributes()
    {
        return [];
    }

    /**
     * Permite ordenar el arreglo de columnas para el formulario 
     * @param Array $formColumns: arreglo con las columnas del formulario
     * @param Array $order: arreglo con el nombre de las columnas y el orden que se desea 
     */
    public function orderFormColumns($formColumns, $order)
    {
        $orderedColumns = [];
        foreach ($order as $column) {
            if (isset($formColumns[$column])) {
                $orderedColumns[$column] = $formColumns[$column];
            }
        }
        return $orderedColumns;
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public function getDbFields()
    {
        if (empty($this->_dbFields)) {
            $modelClass = new \ReflectionClass(static::class);
            $db = $this->getDb();
            $dbFields = [];
            foreach ($this->getSchemaNames() as $schemaName) {
                foreach ($db->getSchema()->getTableSchemas($schemaName) as $table) {
                    if ($table->name === $this->tableName()) {
                        $tabIndex = 1;
                        foreach ($table->columns as $column => $columnSchema) {

                            if ($columnSchema->isPrimaryKey && $columnSchema->autoIncrement) {
                                $render = [];
                            } else if ($columnSchema->type == 'text') {
                                $render = [
                                    static::RENDER_CREATE,
                                    static::RENDER_SEARCH,
                                    static::RENDER_UPDATE,
                                ];
                            } else {
                                $render = [
                                    static::RENDER_GRID,
                                    static::RENDER_CREATE,
                                    static::RENDER_SEARCH,
                                    static::RENDER_UPDATE,
                                ];
                            }

                            $onlyInSearch = [
                                static::STATUS_COLUMN,
                                static::CREATED_BY_COLUMN,
                                static::CREATED_AT_COLUMN,
                                static::UPDATED_BY_COLUMN,
                                static::UPDATED_AT_COLUMN
                            ];

                            if (in_array($column, $onlyInSearch)) {
                                foreach ($render as $keyRenderType => $renderType) {
                                    if ($renderType !== static::RENDER_SEARCH) {
                                        unset($render[$keyRenderType]);
                                    }
                                }
                            }

                            $dbFields[$column] = [
                                'isPrimaryKey' => $columnSchema->isPrimaryKey,
                                'isRequired' => !$columnSchema->allowNull,
                                'isFileAttribute' => false,
                                'hasRelations' => false,
                                'columnSchema' => $columnSchema,
                                'relation' => [],
                                'useWidget' => false,
                                'widget' => [],
                                'render' => $render,
                                'tabindex' => $tabIndex
                            ];

                            if ($column == static::STATUS_COLUMN) {
                                $dbFields[$column]['useWidget'] = true;
                                $dbFields[$column]['widget'] = [
                                    'class' => \kartik\widgets\Select2::class,
                                    'data' => Yii::$app->strings::getCondition(),
                                    'options' => [
                                        'placeholder' => Yii::$app->strings::getTextEmpty(),
                                        'tabindex' => $tabIndex,
                                    ]
                                ];
                            }

                            if (in_array($column, [static::UPDATED_BY_COLUMN, static::CREATED_BY_COLUMN])) {
                                $dbFields[$column]['useWidget'] = true;
                                $dbFields[$column]['widget'] = [
                                    'class' => \kartik\widgets\Select2::class,
                                    'data' => Yii::$app->userHelper::getUsers(),
                                    'options' => [
                                        'placeholder' => Yii::$app->strings::getTextEmpty(),
                                        'tabindex' => $tabIndex
                                    ]
                                ];
                            }
                            if (in_array($column, [static::CREATED_AT_COLUMN, static::UPDATED_AT_COLUMN])) {
                                $dbFields[$column]['useWidget'] = true;
                                $dbFields[$column]['widget'] = [
                                    'class' => \kartik\widgets\DatePicker::class,
                                    'options' => [
                                        'tabindex' => $tabIndex
                                    ]
                                ];
                            }


                            switch ($columnSchema->type) {
                                case Schema::TYPE_DATE:
                                case Schema::TYPE_DATETIME:
                                case Schema::TYPE_TIMESTAMP:
                                    $dbFields[$column]['useWidget'] = true;
                                    $dbFields[$column]['widget'] = [
                                        'class' => \kartik\widgets\DatePicker::class,
                                        'options' => [
                                            'tabindex' => $tabIndex
                                        ]
                                    ];

                                    $dbFields[$column]['grid'] = [
                                        'filterType' => GridView::FILTER_DATE,
                                        'filterInputOptions' => [
                                            'id' => Yii::$app->html::activeInputId($this, $column, null, '-grid'),
                                        ]
                                    ];

                                    break;

                                default:
                                    # code...
                                    break;
                            }




                            if ($columnSchema->type == 'text') {
                                $dbFields[$column]['type'] = 'textArea';
                                $dbFields[$column]['fieldOptions'] = [
                                    'class' => 'form-control',
                                    'tabindex' => $tabIndex
                                ];
                            }

                            $tabIndex++;
                        }
                        foreach ($table->foreignKeys as $refs) {
                            $refTable       = $refs[0];
                            $refTableSchema = $db->getTableSchema($refTable);
                            if ($refTableSchema === null) {
                                // Foreign key could point to non-existing table: https://github.com/yiisoft/yii2-gii/issues/34
                                continue;
                            }
                            unset($refs[0]);

                            $fks          = array_keys($refs);
                            $refClassName = $this->generateClassName($refTable);
                            $relationName                               = Inflector::variablize($this->generateRelationName(
                                [],
                                $table,
                                $fks[0],
                                false
                            ));
                            $modelRef = Yii::$container->get($modelClass->getNamespaceName() . '\\' . $refClassName);
                            $dbFields[$fks[0]]['hasRelations'] = true;
                            $dbFields[$fks[0]]['useWidget'] = true;
                            $dbFields[$fks[0]]['widget'] = [
                                'class' => \kartik\widgets\Select2::class,
                                'data' => $modelRef::getData(),
                                'options' => [
                                    'placeholder' => Yii::$app->strings::getTextEmpty(),
                                    'tabindex' => $dbFields[$fks[0]]['tabindex'],
                                ]
                            ];
                            $dbFields[$fks[0]]['grid'] = [
                                'value' => function ($model, $key, $index, $column) {
                                    $relatedName = $column->options['data-related-name'] ?? false;
                                    if (!empty($model->{$column->attribute}) && $relatedName) {
                                        $result = $model[$column->attribute];
                                        try {
                                            $name = $model->{$relatedName}->getNameFromRelations();
                                            $result = $model->{$relatedName}->{$name};
                                        } catch (\Throwable $th) {
                                            //throw $th;
                                        }
                                        return $result;
                                    } else {
                                        return $model[$column->attribute];
                                    }
                                },
                                'options' => [
                                    'style' => 'min-width:180px;',
                                    'data-related-name' => $relationName
                                ],
                                'filterType' => GridView::FILTER_SELECT2,
                                'filter' => $modelRef::getData(),
                                'filterInputOptions' => [
                                    'id' => Yii::$app->html::activeInputId($this, $fks[0], null, '-grid'),
                                    'placeholder' => Yii::$app->strings::getTextAll()
                                ]
                            ];
                        }

                        if (($this->checkJunctionTable($table)) === false) {
                            continue;
                        }

                        break;
                    }
                }
            }

            if ($uploadFilesBehavior = $this->getBehavior('uploadFiles')) {
                foreach ($uploadFilesBehavior->attributes as $attribute => $value) {
                    if (isset($dbFields[$attribute])) {

                        unset($dbFields[$attribute]['type'], $dbFields[$attribute]['fieldOptions']);
                        $dbFields[$attribute]['useWidget'] = true;
                        $dbFields[$attribute]['isFileAttribute'] = true;
                        $dbFields[$attribute]['widget'] = [
                            'class' => \ticmakers\core\widgets\fileinput\FileInput::class,
                            'attribute' => $attribute,
                            'formName' => $this->formName(),
                            'dataPreview' => $this->{$attribute},
                            'allowedFileExtensions' => $value['allowedFileExtensions'],
                        ];
                    }
                }
            }
            $this->_dbFields = $dbFields;
            unset($dbFields);
        }

        return $this->_dbFields;
    }

    /**
     * @return string[] all db schema names or an array with a single empty string
     * @throws NotSupportedException
     * @since 2.0.5
     */
    protected function getSchemaNames()
    {
        $db     = $this->getDb();
        $schema = $db->getSchema();
        if ($schema->hasMethod('getSchemaNames')) { // keep BC to Yii versions < 2.0.4
            try {
                $schemaNames = $schema->getSchemaNames();
            } catch (NotSupportedException $e) {
                // schema names are not supported by schema
            }
        }
        if (!isset($schemaNames)) {
            if (($pos = strpos($this->tableName, '.')) !== false) {
                $schemaNames = [substr($this->tableName, 0, $pos)];
            } else {
                $schemaNames = [''];
            }
        }
        return $schemaNames;
    }

    /**
     * Checks if the given table is a junction table, that is it has at least one pair of unique foreign keys.
     * @param \yii\db\TableSchema the table being checked
     * @return array|boolean all unique foreign key pairs if the table is a junction table,
     * or false if the table is not a junction table.
     */
    protected function checkJunctionTable($table)
    {
        if (count($table->foreignKeys) < 2) {
            return false;
        }
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge(
                $uniqueKeys,
                $this->getDb()->getSchema()->findUniqueIndexes($table)
            );
        } catch (NotSupportedException $e) {
            // ignore
        }
        $result      = [];
        // find all foreign key pairs that have all columns in an unique constraint
        $foreignKeys = array_values($table->foreignKeys);
        for ($i = 0; $i < count($foreignKeys); $i++) {
            $firstColumns = $foreignKeys[$i];
            unset($firstColumns[0]);

            for ($j = $i + 1; $j < count($foreignKeys); $j++) {
                $secondColumns = $foreignKeys[$j];
                unset($secondColumns[0]);

                $fks = array_merge(
                    array_keys($firstColumns),
                    array_keys($secondColumns)
                );
                foreach ($uniqueKeys as $uniqueKey) {
                    if (count(array_diff(
                        array_merge($uniqueKey, $fks),
                        array_intersect(
                            $uniqueKey,
                            $fks
                        )
                    )) === 0) {
                        // save the foreign key pair
                        $result[] = [$foreignKeys[$i], $foreignKeys[$j]];
                        break;
                    }
                }
            }
        }
        return empty($result) ? false : $result;
    }

    /**
     * Generate a relation name for the specified table and a base name.
     * @param array $relations the relations being generated currently.
     * @param \yii\db\TableSchema $table the table schema
     * @param string $key a base name that the relation name may be generated from
     * @param boolean $multiple whether this is a has-many relation
     * @return string the relation name
     */
    protected function generateRelationName($relations, $table, $key, $multiple)
    {
        if (!empty($key) && substr_compare($key, 'id', -2, 2, true) === 0 && strcasecmp(
            $key,
            'id'
        )) {
            $key = rtrim(substr($key, 0, -2), '_');
        }
        if ($multiple) {
            $key = Inflector::pluralize($key);
        }
        $name    = $rawName = Inflector::id2camel($key, '_');
        $i       = 0;
        while (isset($table->columns[lcfirst($name)])) {
            $name = $rawName . ($i++);
        }
        while (isset($relations[$table->fullName][$name])) {
            $name = $rawName . ($i++);
        }

        return $name;
    }

    /**
     * Método encargado de entregar el comentario de una tabla de la base de datos
     *
     * @param string $tableName Nombre de la tabla (con schema si es necesario)
     * @return string
     */
    public function getComment($tableName)
    {
        $db      = $this->getDb();
        $comment = '';

        if ($db->driverName == 'pgsql') {
            $command = $db->createCommand("SELECT obj_description('{$tableName}'::regclass, 'pg_class') AS comment");
            $comment = $command->queryOne()['comment'];
        } elseif ($this->getDb()->driverName == 'mysql') {
            $query = new Query;
            $query->select('table_comment as comment');
            $query->from('INFORMATION_SCHEMA.TABLES');

            if (strpos($tableName, '.') !== false) {
                $tableNaname = explode('.', $tableName);
                $schema      = $tableNaname[0];
                $table       = $tableNaname[1];
                $query->andWhere(['table_schema' => $schema]);
                $query->andWhere(['table_name' => $table]);
            } else {
                $query->andWhere(['table_name' => $tableName]);
            }
            $comment = $query->one()['comment'];
        }


        return $comment;
    }

    /**
     * Generates the link parameter to be used in generating the relation declaration.
     * @param array $refs reference constraint
     * @return string the generated link parameter.
     */
    protected function generateRelationLink($refs)
    {
        $pairs = [];
        foreach ($refs as $a => $b) {
            $pairs[] = "'$a' => '$b'";
        }

        return '[' . implode(', ', $pairs) . ']';
    }

    /**
     * Generates a class name from the specified table name.
     * @param string $tableName the table name (which may contain schema prefix)
     * @param boolean $useSchemaName should schema name be included in the class name, if present
     * @return string the generated class name
     */
    protected function generateClassName($tableName, $useSchemaName = null)
    {

        if (isset($this->classNames[$tableName])) {
            return $this->classNames[$tableName];
        }

        $schemaName    = '';
        $fullTableName = $tableName;
        if (($pos           = strrpos($tableName, '.')) !== false) {
            if (($useSchemaName === null && $this->useSchemaName) || $useSchemaName) {
                $schemaName = substr($tableName, 0, $pos) . '_';
            }
            $tableName = substr($tableName, $pos + 1);
        }

        $db         = $this->getDb();
        $patterns   = [];
        $patterns[] = "/^{$db->tablePrefix}(.*?)$/";
        $patterns[] = "/^(.*?){$db->tablePrefix}$/";
        $className = $tableName;
        foreach ($patterns as $pattern) {
            if (preg_match($pattern, $tableName, $matches)) {
                $className = $matches[1];
                break;
            }
        }

        return $this->classNames[$fullTableName] = Inflector::id2camel(
            $schemaName . $className,
            '_'
        );
    }

    /**
     * Determines if relation is of has many type
     *
     * @param TableSchema $table
     * @param array $fks
     * @return boolean
     * @since 2.0.5
     */
    protected function isHasManyRelation($table, $fks)
    {
        $uniqueKeys = [$table->primaryKey];
        try {
            $uniqueKeys = array_merge(
                $uniqueKeys,
                $this->getDb()->getSchema()->findUniqueIndexes($table)
            );
        } catch (NotSupportedException $e) {
            // ignore
        }
        foreach ($uniqueKeys as $uniqueKey) {
            if (count(array_diff(
                array_merge($uniqueKey, $fks),
                array_intersect($uniqueKey, $fks)
            )) === 0) {
                return false;
            }
        }
        return true;
    }
}
