<?php

namespace ticmakers\core\base;

use Yii;
use yii\base\Module as YiiModule;
use yii\i18n\PhpMessageSource;

/**
 * Clase base para los modulos del sistema
 * @package ticmakers/core
 * @subpackage base
 * @category Core
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Module extends YiiModule
{
    /**
     * Permite establecer los modelos a sobreescribir en el modulo
     * ```
     * $modelMap = [
     *     'base' => [
     *         'ScheduledTask' => ScheduledTask::class,
     *         'ScheduledTaskLogs' => ScheduledTaskLogs::class,
     *         'TaskPeriodicities' => TaskPeriodicities::class,
     *     ],
     *     'searchs' => [
     *         'ScheduledTask' => ScheduledTaskSearch::class,
     *         'ScheduledTaskLogs' => ScheduledTaskLogsSearch::class,
     *         'TaskPeriodicities' => TaskPeriodicitiesSearch::class,
     *     ],
     *     'task' => [
     *         'TaskBase' => TaskBase::class,
     *     ],
     * ];
     * ```
     *
     * @var array Model map
     */
    public $modelMap = [];

    /**
     * Permite establecer las migas de pan base para el módulo
     *
     * @var array
     */
    public $breadcrumbsBase;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        if (empty($this->breadcrumbsBase)) {
            $this->breadcrumbsBase = [];
        }
        // inicializa el módulo con la configuración cargada desde config.php
        \Yii::configure(
            $this,
            require $this->getBasePath() . '/config/main.php'
        );
        $this->registerTranslations();
    }

    /**
     * Registra las traducciones para los mensajes de todo el módulo
     *
     * @return void
     */
    public function registerTranslations()
    {
        if (!isset(Yii::$app->i18n->translations["{$this->id}*"])) {
            Yii::$app->i18n->translations["{$this->id}*"] = [
                'class' => PhpMessageSource::class,
                'sourceLanguage' => 'en-US',
                'basePath' => $this->getBasePath() . '/messages',
                'fileMap' => [
                    $this->id => "all.php"
                ]
            ];
        }
    }
}
