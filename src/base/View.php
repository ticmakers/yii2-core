<?php

namespace ticmakers\core\base;

use yii\web\View as YiiView;

class View extends YiiView
{

    /**
     * Permite definir el título del módulo al cual se encuentra asociada la vista
     *
     * @var string
     */
    public $moduleTitle;

    /**
     * Undocumented function
     *
     * @param [type] $viewFile
     * @param [type] $params
     * @param [type] $output
     * @return void
     */
    public function afterRender($viewFile, $params, &$output)
    {
        $output = str_replace('{title-grid}', $this->title, $output);
    }
}
