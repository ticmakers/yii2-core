<?php

namespace ticmakers\core\components;

/**
 * Clase base para los modelos dinámicos.
 *
 * @package ticmakers
 * @subpackage core/components
 * @category Components
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class DynamicModel extends \yii\base\DynamicModel
{
    protected $_labels;

    public function setAttributeLabels($labels)
    {
        $this->_labels = $labels;
    }

    public function getAttributeLabel($name)
    {
        return $this->_labels[$name] ?? $name;
    }

    /**
     * Crear una intancia de la clase con los attributos y las reglas ya pasadas y listo para utilizar.
     *
     * @param array $attributes Atributos a ser validados.
     * @param array $rules Reglas de validación en forma de arreglo.
     * @return DynamicModel
     */
    public static function withRules($attributes, $rules = [])
    {
        $instance = new self($attributes);
        foreach ($rules as $rule) {
            $options = [];
            if (count($rule) > 2) {
                $options = $rule;
                unset($options[0], $options[1]);
            }
            $instance->addRule($rule[0], $rule[1], $options);
        }
        return $instance;
    }
}
