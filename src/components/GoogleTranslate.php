<?php

namespace ticmakers\core\components;

use Stichoza\GoogleTranslate\GoogleTranslate as StichozaGoogleTranslate;
use yii\base\Component;

/**
 * 
 */
class GoogleTranslate extends Component
{
    public $key;
    /**
     * You can translate text from one language
     * to another language
     * @param string $source Source language
     * @param string $target Target language
     * @param string $text   Source text string
     * @return array
     */
    public function text($text, $source = 'en', $target = 'es')
    {

        if (!empty($text)) {
            $tr = new StichozaGoogleTranslate($source, $target, [
                'timeout' => 10,
                'proxy' => [
                    'socks4'  => 'socks5://86.111.88.10:58242'
                ],
                'headers' => [
                    'User-Agent' => 'Foo/5.0 Lorem Ipsum Browser'
                ]
            ]); // Translates to 'en' from auto-detected language by default
            // $tr->setSource($source); // Translate from English
            $tr->setSource(); // Detect language automatically
            $tr->setTarget($target); // Translate to Georgian

            try {
                $translatedText = $tr->translate($text);
            } catch (\Throwable $th) {
                $translatedText = $text;
                // throw $th;
            }
        } else {
            $translatedText = $text;
        }
        // return $translatedText;
    }
}
