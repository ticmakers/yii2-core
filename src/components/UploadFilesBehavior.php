<?php

namespace ticmakers\core\components;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\helpers\Url;
use yii\helpers\Inflector;
use yii\helpers\StringHelper;
use yii\web\UploadedFile;

/**
 * Undocumented class
 * 
 * ```
 * $behaviors = parent::behaviors();
 * $newBehaviors = [
 *     'uploadFiles' => [
 *         'class' => UploadFilesBehavior::class,
 *         'attributes' => [
 *             'profile_photo' => [
 *                 'type' => UploadFilesBehavior::TYPE_FILE,
 *                 'modelClass' => Members::class,
 *                 'instance' => $this->getMember(),
 *                 'attribute' => 'profile_photo',
 *                 'isMultiple' => false,
 *                 'relatedKey' => [
 *                     'user_id' => 'user_id',
 *                 ],
 *                 'relatedAttributes' => [
 *                     'user_id' => 'user_id',
 *                     'name' => 'name',
 *                     'mobile_number' => 'mobile_number',
 *                     'document_number' => 'document_number',
 *                     'document_type_id' => 'document_type_id',
 *                     'address' => 'address',
 *                     'city_id'  => 'city_id',
 *                     'associated' => function($modelOwner){
 *                          return $modelOwner->associated;
 *                      },
 *                 ]
 *             ]
 *         ]
 *     ]
 * ];
 *
 * return Yii::$app->arrayHelper::merge($behaviors, $newBehaviors);
 * ```
 * 
 * @package ticmakers\core\components
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 */
class UploadFilesBehavior extends Behavior
{
    /**
     * 
     */
    const TYPE_FILE = 'UPLOAD';

    /**
     * 
     */
    const TYPE_BASE64 = 'BASE64';

    /**
     * @var string Undocumented variable
     */
    public $basePath = '@webroot/uploads';

    /**
     * @var string Undocumented variable
     */
    public $baseUrl = '@web/uploads/';

    /**
     * @var string Undocumented variable
     */
    public $defaultImage = '@web/images/no-image.png';

    /**
     * @var string Undocumented variable
     */
    public $suffixOld  = '-old';

    /**
     * @var array Undocumented variable
     */
    public $attributes = [];

    /**
     * Undocumented function
     *
     * @return void
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_INIT => 'onInit',
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
            ActiveRecord::EVENT_AFTER_FIND => 'afterFind',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterSave',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterSave',
            ActiveRecord::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    /**
     * Undocumented function
     *
     * @param [type] $event
     * @return void
     */
    public function onInit($event)
    {
        foreach ($this->attributes as $key => $attribute) {
            if (!array_key_exists('instance', $attribute) && array_key_exists('value', $attribute)) {
                $this->attributes[$key]['instance'] = $attribute['value'];
                unset($this->attributes[$key]['value']);
            }

            /**
             * Eliminamos carpetas vacias
             */
            try {
                if (PHP_OS === 'Windows') { } else {
                    $directoryName = $this->getDirectoryName($attribute['modelClass']);
                    exec("find " . Url::to("{$this->basePath}/{$directoryName}/") . " -type d -empty -exec rmdir {} \;");
                }
            } catch (\Throwable $th) {
                //throw $th;
            }
        }
    }


    /**
     * Undocumented function
     *
     * @param [type] $event
     * @return void
     */
    public function beforeValidate($event)
    {

        foreach ($this->attributes as $key => $attribute) {

            if (isset($attribute['relatedKey']) && !empty($attribute['relatedKey'])) {
                if ($attribute['instance'] instanceof \yii\db\ActiveRecord) {
                    $modelInstance = $attribute['instance'];
                } else {
                    $modelInstance = Yii::createObject($attribute['modelClass']);
                }
            } else {
                $modelInstance = $event->sender;
            }




            $fileIndex = $attribute['attribute'];

            if (!isset($this->attributes[$key]['basePath']) && !(isset($attribute['isMultiple']) && $attribute['isMultiple'])) {
                $this->getBasePathAndUrl($attribute, $modelInstance);
                $this->attributes[$key] = $attribute;
            }

            if (isset($attribute['type']) && $attribute['type'] === static::TYPE_BASE64) {
                $dataFileBase64 = $modelInstance->{$fileIndex};
                $parts = explode(';', $dataFileBase64);
                if (count($parts) == 2) {
                    $content = str_replace('base64,', '', $parts[1]);
                    $verifyBase64 = "$parts[0];base64," . base64_encode(base64_decode($content));
                } else {
                    $verifyBase64 = 'no-valid';
                }
                if ($verifyBase64 === $dataFileBase64) {
                    if (isset($attribute['isMultiple']) && $attribute['isMultiple']) {
                        $uploadedFiles = $dataFileBase64;
                    } else {
                        $uploadedFiles = [$dataFileBase64];
                    }
                }
            } else {

                $uploadedFiles = UploadedFile::getInstances(
                    $modelInstance,
                    $fileIndex
                );
            }
            if (!empty($uploadedFiles)) {
                $event->sender->$key = 'validate';
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $event
     * @return void
     */
    public function beforeSave($event)
    {
        foreach ($this->attributes as $key => $attribute) {

            if (isset($attribute['relatedKey']) && !empty($attribute['relatedKey'])) {
                if ($attribute['instance'] instanceof \yii\db\ActiveRecord) {
                    $modelInstance = $attribute['instance'];
                } else {
                    $modelInstance = Yii::createObject($attribute['modelClass']);
                }
            } else {
                $modelInstance = $event->sender;
            }

            $fileIndex = $attribute['attribute'];

            if (!isset($this->attributes[$key]['basePath']) && !(isset($attribute['isMultiple']) && $attribute['isMultiple'])) {
                $this->getBasePathAndUrl($attribute, $modelInstance);
                $this->attributes[$key] = $attribute;
            }

            if (isset($attribute['type']) && $attribute['type'] === static::TYPE_BASE64) {
                $dataFileBase64 = $modelInstance->{$fileIndex};
                $parts = explode(';', $dataFileBase64);
                if (count($parts) == 2) {
                    $content = str_replace('base64,', '', $parts[1]);
                    $verifyBase64 = "$parts[0];base64," . base64_encode(base64_decode($content));
                } else {
                    $verifyBase64 = 'no-valid';
                }
                if ($verifyBase64 === $dataFileBase64) {
                    if (isset($attribute['isMultiple']) && $attribute['isMultiple']) {
                        $uploadedFiles = $dataFileBase64;
                    } else {
                        $uploadedFiles = [$dataFileBase64];
                    }
                }
            } else {

                $uploadedFiles = UploadedFile::getInstances(
                    $modelInstance,
                    $fileIndex
                );
                
                $postOldFiles = Yii::$app->request->post($modelInstance->formName())[$fileIndex . $this->suffixOld] ?? '';
            
                if (!empty($postOldFiles)) {
                    $oldFiles = explode(',', $postOldFiles);
                } else {
                    $oldFiles = [];
                }
                $this->attributes[$key]['oldFiles'] = $oldFiles;
            }
            
            if (!empty($uploadedFiles)) {
                $this->upload($uploadedFiles, $key);
            }
          
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $event
     * @return void
     */
    public function afterFind($event)
    {
        foreach ($this->attributes as $key => $attribute) {
            $event->sender->{$key} = $this->getFiles($key);
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $event
     * @return void
     */
    public function afterSave($event)
    {

        $modelSender =  $event->sender;
        foreach ($this->attributes as $key => $attribute) {

            $fileIndex = $attribute['attribute'];
            if (isset($attribute['isMultiple']) && $attribute['isMultiple']) {
                $condition = [];
                if (isset($attribute['relatedKey'])) {

                    foreach ($attribute['relatedKey'] as $modelUploadedFileAttribute => $modelSenderAttribute) {

                        if (is_callable($modelSenderAttribute)) {
                            $condition[$modelUploadedFileAttribute] = call_user_func_array($modelSenderAttribute, ['model' => $modelSender]);
                        } else {
                            $condition[$modelUploadedFileAttribute] = $modelSender->{$modelSenderAttribute};
                        }
                    }
                } 
                
                $attribute['modelClass']::updateAll([
                    $attribute['modelClass']::STATUS_COLUMN => $attribute['modelClass']::STATUS_INACTIVE
                ], $condition);
                
                if (isset($attribute['oldFiles']) && !empty($attribute['oldFiles'])) {
                    foreach ($attribute['oldFiles'] as $oldFile) {
                        $oldFileExplode = \explode('/', $oldFile);
                        $nameOldFile = end($oldFileExplode);
                        $condition[$attribute['attribute']] = $nameOldFile;  
                        $attribute['modelClass']::updateAll([$attribute['modelClass']::STATUS_COLUMN => $attribute['modelClass']::STATUS_ACTIVE], $condition);
                    }
                }

                if (isset($attribute['uploaded']) && !empty($attribute['uploaded'])) {
                    
                    foreach ($attribute['uploaded'] as $keyPath => $uploadedFile) {
                        $attribute['keyPath'] = $keyPath;
                        $modelInstance = Yii::createObject($attribute['modelClass']);
                        $modelInstance->{$fileIndex} = $uploadedFile;
                        if (isset($attribute['relatedAttributes'])) {
                            foreach ($attribute['relatedAttributes'] as $modelUploadedFileAttribute => $modelSenderAttribute) {
                                if (is_callable($modelSenderAttribute)) {
                                    $modelInstance->{$modelUploadedFileAttribute} = call_user_func_array($modelSenderAttribute, ['model' => $modelSender]);
                                } else {
                                    $modelInstance->{$modelUploadedFileAttribute} = $modelSender->{$modelSenderAttribute};
                                }
                            }
                        }

                        $oldBehavior = $modelInstance->getBehavior('uploadFiles');
                        $modelInstance->detachBehavior('uploadFiles');
                        $modelInstance->{$modelInstance::STATUS_COLUMN} = $modelInstance::STATUS_ACTIVE; 
                        $modelInstance->save();
                        $modelInstance->attachBehavior('uploadFiles', $oldBehavior);
                        unset($oldBehavior);
                        $this->renameDirectory($attribute, $modelInstance);
                    }
                }
            } else {

                if (isset($attribute['relatedKey']) && !empty($attribute['relatedKey'])) {

                    if (!empty($attribute['instance'])) {
                        /* if ($attribute['instance'] instanceof \yii\db\ActiveRecord) {
                            $modelInstance = $attribute['instance'];
                        } else {
                            $modelInstance = $attribute['instance'];
                        } */
                        $modelInstance = $attribute['instance'];
                    } else {
                        $modelInstance = Yii::createObject($attribute['modelClass']);
                    }
                } else {
                    $modelInstance = $modelSender;
                    $modelInstance->{$key} = \str_replace($attribute['baseUrl'], '', $modelInstance->{$key});
                }

                if (isset($attribute['uploaded']) && !empty($attribute['uploaded'])) {
                    foreach ($attribute['uploaded'] as $uploadedFile) {
                        $modelInstance->{$fileIndex} = $uploadedFile;
                        break;
                    }
                }

                if (isset($attribute['relatedAttributes'])) {
                    foreach ($attribute['relatedAttributes'] as $modelUploadedFileAttribute => $modelSenderAttribute) {

                        if (is_callable($modelSenderAttribute)) {
                            $modelInstance->{$modelUploadedFileAttribute} = call_user_func_array($modelSenderAttribute, ['model' => $modelSender]);
                        } else {
                            $modelInstance->{$modelUploadedFileAttribute} = $modelSender->{$modelSenderAttribute};
                        }
                    }
                }

                if ($modelInstance->{$attribute['attribute']} == Url::to($this->defaultImage, true)) {
                    $modelInstance->{$attribute['attribute']} = null;
                }

                $modelInstance->detachBehavior('uploadFiles');
                $modelInstance->save(true, [$attribute['attribute']]);
                $modelInstance->attachBehavior('uploadFiles', $this);
                $modelInstance->trigger(ActiveRecord::EVENT_AFTER_FIND);
                $this->renameDirectory($attribute, $modelInstance);
            }
        }
    }
    /**
     * Validar si el directorio esta vacio
     * @param string
     * @return boolean
     */
    /*  public function validateEmptyDir($dir)
    {
        if (!is_readable($dir)) return NULL; 
        return (count(scandir($dir)) == 2);
    }
 */

    /**
     * Undocumented function
     *
     * @param [type] $attribute
     * @param [type] $modelInstance
     * @return void
     */
    public function renameDirectory($attribute, $modelInstance)
    {
        $directoryName = $this->getDirectoryName($attribute['modelClass']);
        $routeDirectory = Url::to("{$this->basePath}/{$directoryName}/{$modelInstance->getPrimaryKey()}");
        if ($attribute['keyPath'] != $modelInstance->getPrimaryKey()) {
            if (file_exists($routeDirectory) && (count(glob("$routeDirectory/*")) === 0)) {
                rmdir($routeDirectory);
            }
            rename(
                Url::to("{$this->basePath}/{$directoryName}/{$attribute['keyPath']}"),
                $routeDirectory
            );
            if (PHP_OS === 'Windows') {
                exec("rd /s /q " . Url::to("{$this->basePath}/{$directoryName}/{$attribute['keyPath']}"));
            } else {
                exec("rm -rf " . Url::to("{$this->basePath}/{$directoryName}/{$attribute['keyPath']}"));
            }
        }
    }

    /**
     * Undocumented function
     *
     * @param [type] $attribute
     * @param [type] $modelInstance
     * @return void
     */
    private function getBasePathAndUrl(&$attribute, $modelInstance)
    {
        $directoryName = $this->getDirectoryName($attribute['modelClass']);

        if ($modelInstance->isNewRecord) {
            $keyDirectory = sha1(time() . rand(1000, 9999));
        } else {
            $keyDirectory = $modelInstance->getPrimaryKey();
        }

        $verifyDirectory = Yii::getAlias("{$this->basePath}/{$directoryName}/{$keyDirectory}");
        $directory = "{$this->basePath}/{$directoryName}/{$keyDirectory}";
        $urlUpload = "{$this->baseUrl}{$directoryName}/{$keyDirectory}/";


        if (!file_exists($verifyDirectory)) {
            mkdir($verifyDirectory, 0775, true);
        }

        $attribute['baseUrl'] = Url::to($urlUpload, true);
        $attribute['basePath'] = $directory;
        $attribute['keyPath'] = $keyDirectory;
    }

    /**
     * Undocumented function
     *
     * @param [type] $model
     * @return void
     */
    private function getDirectoryName($model)
    {
        return Inflector::underscore(StringHelper::basename($model));
    }

    /**
     * Undocumented function
     *
     * @param [type] $uploadedFiles
     * @param [type] $attribute
     * @return void
     */
    public function upload($uploadedFiles, $attribute)
    {
        $attributeData = $this->attributes[$attribute];
        $attributeData['uploaded'] = [];
        foreach ($uploadedFiles as $file) {

            if (isset($attributeData['isMultiple']) && $attributeData['isMultiple']) {
                $modelInstance = Yii::createObject($attributeData['modelClass']);
                $this->getBasePathAndUrl($attributeData, $modelInstance);
            }
            $saveFile = $this->saveFile($file, $attributeData);
            $attributeData['uploaded'][$saveFile['keyPath']] = $saveFile['file'];
            
        }

        $this->attributes[$attribute] = $attributeData;
    }

    /**
     * Undocumented function
     *
     * @param [type] $file
     * @param [type] $config
     * @return void
     */
    public function saveFile($file, $config)
    {
        if (isset($config['type']) && $config['type'] === static::TYPE_BASE64) {
            $parts = explode(';', $file);
            if (count($parts) == 2) {
                $extention = explode('/', $parts[0])[1];
                $content = str_replace('base64,', '', $parts[1]);

                $name = $config['attribute'] . '-' . date('Ymd-His') . rand(100, 999) . '.' . $extention;

                $savePath = Url::to($config['basePath']) . DIRECTORY_SEPARATOR . $name;

                file_put_contents($savePath, base64_decode($content));
            }
        } else {
            $name = $config['attribute'] . '-' . date('Ymd-His') . rand(100, 999) . '.' . $file->extension;
            $savePath = Url::to($config['basePath']) . DIRECTORY_SEPARATOR . $name;
            $file->saveAs($savePath, true);
        }

        return [
            'keyPath' => $config['keyPath'],
            'file' => $name
        ];
    }

    /**
     * Undocumented function
     *
     * @param [type] $keyAttribute
     * @return void
     */
    public function getFiles($keyAttribute)
    {
        $result = [];
        $attribute = $this->attributes[$keyAttribute];
        if ($attribute['instance'] instanceof \yii\db\ActiveQuery) {
            $models = $attribute['instance']->all();
            foreach ($models as $model) {
                $this->getBasePathAndUrl($attribute, $model);
                if (empty($model->{$attribute['attribute']})) {
                    $result[] = Url::to($this->defaultImage, true);
                } else {
                    $result[] = $attribute['baseUrl'] . $model->{$attribute['attribute']};
                }
            }
        } else if ($attribute['instance'] instanceof \yii\db\ActiveRecord) {
            $this->getBasePathAndUrl($attribute, $attribute['instance']);
            if (empty($attribute['instance']->{$attribute['attribute']})) {
                $result = Url::to($this->defaultImage, true);
            } else {
                $result = $attribute['baseUrl'] . $attribute['instance']->{$attribute['attribute']};
            }
        }

        return $result;
    }
}
