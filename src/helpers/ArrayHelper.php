<?php

namespace ticmakers\core\helpers;

use yii\helpers\ArrayHelper as ArrayHelperBase;

/**
 * 
 * @package ticmakers
 * @subpackage helpers
 * @category Helpers
 *
 * @author  Daniel Julian Sanchez Alvarez <daniel.sanchez@timakers.com>
 * @copyright Copyright (c) 2019 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ArrayHelper extends ArrayHelperBase
{
    /**
     * Permite unir varios arreglos de datos en uno solo
     *
     * @param array ...$arrays
     * @return void
     */
    public static function mergeMultiple(...$arrays)
    {
        $value = [];
        foreach ($arrays as $array) {
            $value = parent::merge($value, $array);
        }
        return $value;
    }
}
