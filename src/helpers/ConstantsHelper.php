<?php

namespace ticmakers\core\helpers;

class ConstantsHelper
{
    const CLASS_NAME = 'class';
    const ORDER = 'order';
    const ATTRIBUTE = 'attribute';
    const VISIBLE = 'visible';
    const FORMAT = 'format';
    const VALUE = 'value';
    const FILTER = 'filter';
    const FILTER_TYPE = 'filterType';
    const FILTER_INPUT_OPTIONS = 'filterInputOptions';
    const IS_MODAL = 'isModal';
    const DYNA_MODAL_ID = 'dynaModalId';
    const TYPE = 'type';
    const TABINDEX = 'tabindex';
    const ID = 'id';
    const ACTION = 'action';
    const METHOD = 'method';
    const DATA = 'data';
    const OPTIONS = 'options';
    const PLACEHOLDER = 'placeholder';
    const MODEL = 'model';
    const SEARCH_MODEL = 'searchModel';
    const VIEW_PATH = 'viewPath';
    const MODEL_NAME = 'modelName';
    const PREFIX_SEARCH = 'search-';
    const PREFIX_GRIDVIEW = 'gridview-';
    const PREFIX_VIEW = 'view-';
    const PREFIX_DEFAULT = '';
    const SUFIX_DEFAULT = '-default';
    const INDEX = 'index';
    const MODEL_CLASS = 'modelClass';
    const SEARCH_MODEL_CLASS = 'searchModelClass';
    const RENDER_UPDATE = 'U';
    const RENDER_CREATE = 'C';
    const RENDER_SEARCH = 'S';
    const RENDER_SEARCH_TOP = 'T';
    const RENDER_INDEX = 'I';
}
