<?php

namespace ticmakers\core\helpers;

use Yii;
use ticmakers\core\helpers\ConstantsHelper as C;
use ticmakers\core\helpers\ArrayHelper;
use yii\helpers\Inflector;

class ControllerHelper
{

    /**
     * Permite la normalización de los arreglos de controlladores a ser mapeados en la aplicación
     *
     * @param array $controllersMap
     * @param string $controllerBase
     * @param string $basePathModels
     * @param string $basePathViews
     * @return mixed
     */
    public static function normalizeControllerMap(
        $controllersMap,
        $controllerBase,
        $commonConfig = [],
        $basePathModels = 'app\models',
        $basePathViews = '@app/views'
    ) {
        foreach ($controllersMap as $key => $value) {

            if (is_array($value)) {
                ArrayHelper::setValue($controllersMap[$key], C::CLASS_NAME, $controllerBase);

                if (isset($value[C::MODEL_NAME]) && !isset($controllersMap[$key][C::MODEL_CLASS])) {
                    $controllersMap[$key][C::MODEL_CLASS] = "{$basePathModels}\\base\\{$value[C::MODEL_NAME]}";
                }
                if (isset($value[C::MODEL_NAME]) && !isset($controllersMap[$key][C::SEARCH_MODEL_CLASS])) {
                    $controllersMap[$key][C::SEARCH_MODEL_CLASS] = "{$basePathModels}\\searchs\\{$value[C::MODEL_NAME]}";
                }
                if (!isset($controllersMap[$key][C::VIEW_PATH])) {
                    $controllersMap[$key][C::VIEW_PATH] = "{$basePathViews}\\{$key}";
                }
                static::setCommonConfig($controllersMap[$key], $commonConfig);
                unset($controllersMap[$key][C::MODEL_NAME]);
            } else if (!is_string($key)) {
                unset($controllersMap[$key]);
                $idController = Inflector::camel2id($value);
                $controllersMap[$idController] = [
                    C::CLASS_NAME => $controllerBase,
                    C::MODEL_CLASS => "{$basePathModels}\\base\\{$value}",
                    C::SEARCH_MODEL_CLASS => "{$basePathModels}\\searchs\\{$value}",
                    C::VIEW_PATH => "{$basePathViews}\\{$idController}",
                ];
                static::setCommonConfig($controllersMap[$idController], $commonConfig);
            } else {
                $controllersMap[$key] = [
                    C::CLASS_NAME => $controllerBase,
                    C::MODEL_CLASS => "{$basePathModels}\\base\\{$value}",
                    C::SEARCH_MODEL_CLASS => "{$basePathModels}\\searchs\\{$value}",
                    C::VIEW_PATH => "{$basePathViews}\\{$key}",
                ];
                static::setCommonConfig($controllersMap[$key], $commonConfig);
            }
        }
        return $controllersMap;
    }

    /**
     * Undocumented function
     *
     * @param [type] $arrayConfig
     * @param [type] $commonConfig
     * @return void
     */
    protected static function setCommonConfig(&$arrayConfig, $commonConfig)
    {
        $arrayConfig = ArrayHelper::mergeMultiple($commonConfig, $arrayConfig);
    }
}
