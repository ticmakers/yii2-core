<?php

namespace ticmakers\core\helpers;

use Yii;

/**
 * Permite la facil administración de los mensajes de operaciones
 *
 * @package ticmakers
 * @subpackage helpers
 * @category Helpers
 *
 * @property-read string $TYPE_SUCCESS Indica el texto que identifica los mensajes "success"
 * @property-read string $TYPE_INFO Indica el texto que identifica los mensajes "info"
 * @property-read string $TYPE_WARNING Indica el texto que identifica los mensajes "warning"
 * @property-read string $TYPE_DANGER Indica el texto que identifica los mensajes "danger"
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class MessageHelper
{
  const TYPE_SUCCESS = 'success';
  const TYPE_INFO = 'info';
  const TYPE_WARNING = 'warning';
  const TYPE_DANGER = 'error';

  /**
   * Este método añade un mensaje de operación a los flashes de sesión según su tipo
   *
   * @param string $message Mensaje a mostrar
   * @param string $type Tipo de mensaje a mostrar
   */
  public static function setMessage($type, $message)
  {
    Yii::$app->session->setFlash($type, "<b>{$message}</b>");
  }

  /**
   * Este método sera lamado para mostrar el mensaje en alert al momento de borrar un registro
   *
   * @return string Funcion js para validar mensaje de error
   */
  public static function messageDelete()
  {
    $mensaje_success = "<b>" . static::getIcon(Growl::TYPE_SUCCESS) . ' ' . Yii::t(
      'app',
      'It was successfully removed.'
    ) . "</b>";
    $mensaje_error = "<b>" . static::getIcon(Growl::TYPE_DANGER) . ' ' . Yii::t(
      'app',
      'An error occurred while trying to delete the record.'
    ) . "</b>";

    return "
            function(link,success,data){
                if(success)
                {
                    showSuccessMessage('{$mensaje_success}');
                }else
                {
                    showErrorMessage('{$mensaje_error}');
                }

            }
        ";
  }

  /**
   * Este método sera lamado para mostrar el mensaje en alert al momento de restaurar un registro
   *
   * @return string Funcion js para validar mensaje de error
   */
  public static function messageRestore()
  {
    $mensaje_success = "<b>" . static::getIcon(Growl::TYPE_SUCCESS) . ' ' . Yii::t(
      'app',
      'It was successfully restored.'
    ) . "</b>";
    $mensaje_error = "<b>" . static::getIcon(Growl::TYPE_DANGER) . ' ' . Yii::t(
      'app',
      'An error occurred while trying to restore the registry.'
    ) . "</b>";

    return "
            function(link,success,data){
                if(success)
                {
                    showSuccessMessage('{$mensaje_success}');
                }else
                {
                    showErrorMessage('{$mensaje_error}');
                }

            }
        ";
  }

  /**
   * Encargado de entregar el icono correspondiente segun el TYPE
   * @param string $type
   * @return string Html con el icono
   */
  private static function getIcon($type)
  {
    $icon = '';
    switch ($type) {
      case static::TYPE_SUCCESS:
        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_OK);
        break;
      case static::TYPE_DANGER:
        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_REMOVE);
        break;
      case static::TYPE_WARNING:
        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_INFO_SIGN);
        break;
    }

    return $icon;
  }


  /**
   * Método encargado de entrega los mensajes enviados desde static::setMessage() en forma de JS
   *
   * @return string
   */
  public static function getMessagesJS()
  {
    $js = '';
    foreach (Yii::$app->session->getAllFlashes() as $type => $message) {

      $js .= "getMessage('{$type}',`{$message}`)\n";
    }

    return $js;
  }
}
