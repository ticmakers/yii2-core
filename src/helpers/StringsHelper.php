<?php

namespace ticmakers\core\helpers;

use Yii;

/**
 * Clase Helper para ayudar a administrar los textos de la aplicación
 * @package ticmakers
 * @subpackage helpers
 * @category Helpers
 *
 * @author  Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class StringsHelper
{

    //Estados para condiciones
    const YES = 'Y';
    const NO = 'N';
    //Textos estandar
    const TEXT_EMPTY = 'Select ...';
    const TEXT_ALL = 'All';
    //Sexos
    const MALE_GENDER = 'M';
    const FEMALE_GENDER = 'F';


    /**
     * Permite la Generación de un string aleatorio
     *
     * @param integer $length Determines the length of the string to be generated
     * @param boolean $uc Can include letters of the alphabet in the chain generated
     * @param boolean $n It allows the inclusion in the chain generated
     * @param boolean $sc It allows you to include symbols in the string generated
     * @return string Generated string
     */
    public static function randomString(
        $length = 10,
        $uc = true,
        $n = true,
        $sc = false
    ) {
        $source = 'abcdefghijklmnopqrstuvwxyz';
        if ($uc == 1) {
            $source .= 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        }
        if ($n == 1) {
            $source .= '1234567890';
        }
        if ($sc == 1) {
            $source .= '|@#~$%()=^*+[]{}-_';
        }
        if ($length > 0) {
            $rstr = "";
            $source = preg_split('//', $source, -1, PREG_SPLIT_NO_EMPTY);
            for ($i = 1; $i <= $length; $i++) {
                $num = rand(0, count($source) - 1);
                $rstr .= $source[$num];
            }
        }
        return $rstr;
    }

    /**
     * Elimina los accentos de un string
     * @param string $string
     * @param string $forQuery
     * @return string
     */
    public static function removeAccents($string, $forQuery = false)
    {

        $a = [
            'À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë',
            'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø',
            'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å',
            'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò',
            'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā',
            'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č',
            'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę',
            'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ',
            'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ',
            'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ',
            'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ',
            'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ',
            'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ',
            'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů',
            'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż',
            'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ',
            'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ',
            'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ', 'Ά', 'ά', 'Έ', 'έ', 'Ό',
            'ό', 'Ώ', 'ώ', 'Ί', 'ί', 'ϊ', 'ΐ', 'Ύ', 'ύ', 'ϋ', 'ΰ', 'Ή',
            'ή'
        ];
        $b = [
            'A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E',
            'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O',
            'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a',
            'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o',
            'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A',
            'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C',
            'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E',
            'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H',
            'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I',
            'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L',
            'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n',
            'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r',
            'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't',
            'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u',
            'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z',
            'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I',
            'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U',
            'u', 'A', 'a', 'AE', 'ae', 'O', 'o', 'Α', 'α', 'Ε', 'ε', 'Ο',
            'ο', 'Ω', 'ω', 'Ι', 'ι', 'ι', 'ι', 'Υ', 'υ', 'υ', 'υ', 'Η',
            'η'
        ];

        if ($forQuery) {
            $noAccents = str_replace($a, '%', $string);
        } else {
            $noAccents = str_replace($a, $b, $string);
        }


        return $noAccents;
    }

    /**
     * Método para retornar los valores para condición
     * @param string $index Indice para retornar su valor default NULL
     * @return array
     */
    public static function getCondition($index = null)
    {
        $res = [
            static::YES => Yii::t('app', 'Yes'),
            static::NO => Yii::t('app', 'No'),
        ];
        return $index == null ? $res : $res[$index];
    }

    /**
     * Método para retornar el mensaje "Seleccione" para 'empty' de los dropdownlist
     * @return type
     */
    public static function getTextEmpty()
    {
        return Yii::t('app', static::TEXT_EMPTY);
    }

    /**
     * Método para retornar el mensaje "Todos" para 'empty' de los dropdownlist
     * @return type
     */
    public static function getTextAll()
    {
        return Yii::t('app', static::TEXT_ALL);
    }

    /**
     * Método encargado de reemplazar los espacios por guión bajo (' ','_')
     * @param string $texto
     * @param boolean $acentos
     * @return string Texto reemplazado
     */
    public static function replaceSpaces($texto, $acentos = true)
    {
        $noSpaces = str_replace(' ', '_', $texto);

        return $acentos ? $noSpaces : static::removeAccents($noSpaces);
    }

    /**
     * Método encargado de mostrar un número con decimales sin redondear
     * @param integer $number
     * @param integer $decimals
     * @return string
     */
    public static function trucateFloat(
        $number,
        $decimals = 2
    ) {
        $divide = explode('.', $number);

        if (isset($divide[1])) {
            $amount = $divide[0];
            $truncatedDecimal = substr($divide[1], 0, $decimals);

            return floatval("{$amount}.{$truncatedDecimal}");
        }

        return $number;
    }

    /**
     * Eliminamos un directorio recursivamente
     * @param string $carpeta
     */
    public static function deleteFolder($carpeta)
    {
        foreach (glob($carpeta . "/*") as $archivo) {
            if (is_dir($archivo)) {
                //si es un directorio volvemos a llamar recursivamente
                static::deleteFolder($archivo);
            } else {
                //si es un archivo lo eliminamos
                unlink($archivo);
            }
        }

        rmdir($carpeta);
    }

    /**
     * Método encargado de entregar la estructura de filtro para fechas en un \app\db\Query
     *
     * @param string $field
     * @param string $value
     * @return array
     */
    public static function dateFilter($field, $value)
    {
        if (!empty($value)) {
            $firstDate = date('Y-m-d 00:00', strtotime($value));
            $endDate = date('Y-m-d 23:59', strtotime($value));

            $res = ['between', $field, $firstDate, $endDate];
        } else {
            $res = [];
        }
        return $res;
    }
}
