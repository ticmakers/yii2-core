<?php

namespace ticmakers\core\helpers;

use Yii;
use kartik\grid\GridView;
use kartik\dynagrid\DynaGrid;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\Inflector;
use ticmakers\core\helpers\HtmlHelper as HtmlGenerator;

/**
 * Clase para la creación de elementos de la Interfaz del usuario
 * que se utilizan de manera constante
 *
 * @package ticmakers
 * @subpackage helpers
 * @category Helpers
 *
 * @author  Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class UIHelper
{
    /**
     * Retorna un botón en especifico para la búsqueda de datos de los Dynagrids
     * @param string $label Nombre del botón
     * @param string $modalControl Identificador de la modal
     * @param array $options Opciones HTML
     *
     * @return string
     */
    public static function btnSearch(
        $label = null,
        $modalControl = '#search-modal',
        $options = []
    ) {
        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_SEARCH);
        $label = is_null($label) ? Yii::t('app', 'Search') : $label;

        if (!isset($options['data'])) {
            $options['data'] = [
                'toggle' => 'modal',
                'target' => $modalControl
            ];
        }

        if (!isset($options['id'])) {
            $options['id'] = 'search-button';
        }

        $options['class'] = 'btn btn-outline-primary';
        $options['title'] = $label;

        return Yii::$app->html::button($icon . ' ' . $label, $options);
    }

    /**
     * Entrega el Botón para Refrescar el grid
     * @param string $label Nombre del botón
     * @param  string|array $url Url de la accion en Controller
     * @param  array $options Opciones Html
     * @return string   Button
     */
    public static function btnRefresh(
        $label = null,
        $url = ['index'],
        $options = []
    ) {
        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_REPEAT);
        $label = is_null($label) ? Yii::t('app', 'Refresh') : $label;

        $options['id'] = isset($options['id']) ? $options['id'] : 'refresh';
        $options['class'] = 'btn btn-outline-secondary';
        $options['data-pjax'] = true;
        $options['title'] = $label;

        return Yii::$app->html::a($icon . ' ' . $label, $url, $options);
    }

    /**
     * Entrega el Botón para Crear Registro
     * @param string $label Nombre del botón
     * @param  string|array $url Url de la accion en Controller
     * @param  array $options Opciones Html
     * @return string   Button
     */
    public static function btnNew(
        $label = null,
        $url = ['create'],
        $options = []
    ) {
        if (!Yii::$app->userHelper::checkRouteFromUrl($url)) {
            return;
        }

        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_PLUS);
        $label = is_null($label) ? Yii::t('app', 'New') : $label;

        $options['id'] = isset($options['id']) ? $options['id'] : 'create';
        $options['class'] = 'btn btn-primary';
        $options['title'] = $label;
        if (!isset($options['data-pjax'])) {
            $options['data-pjax'] = 0;
        }

        return Yii::$app->html::a($icon . ' ' . $label, $url, $options);
    }

    /**
     * Entrega el Botón para Actualizar Registro
     * @param string $label Nombre del botón
     * @param  string|array $url Url de la accion en Controller
     * @param  array $options Opciones Html
     * @return string   Button
     */
    public static function btnUpdate($label = null, $url, $options = [])
    {
        if (!Yii::$app->userHelper::checkRouteFromUrl($url)) {
            return;
        }

        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_PENCIL);
        $label = is_null($label) ? Yii::t('app', 'Edit') : $label;

        return Yii::$app->html::a($icon . ' ' . $label, $url, [
            'id' => isset($options['id']) ? $options['id'] : 'update',
            'class' => 'btn btn-primary'
        ]);
    }

    /**
     * Entrega el Botón para Eliminar Registro
     * @param string $label Nombre del botón
     * @param  string|array $url Url de la accion en Controller
     * @param  array $options Opciones Html
     * @return string   Button
     */
    public static function btnDelete($label = null, $url, $options = [])
    {
        if (!Yii::$app->userHelper::checkRouteFromUrl($url)) {
            return;
        }

        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_TRASH);
        $label = is_null($label) ? Yii::t('app', 'Delete') : $label;

        return Yii::$app->html::a($icon . ' ' . $label, $url, [
            'id' => isset($options['id']) ? $options['id'] : 'delete',
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t(
                    'app',
                    'Are you sure to delete this record?'
                ),
                'method' => 'post'
            ]
        ]);
    }

    /**
     * Encargado de entregar el botón a usar en las  para agregar
     * @param string $label Nombre del botón
     * @param  array $options Opciones Html
     * @return string
     */
    public static function btnNewLine($label = null, $options = [])
    {
        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_PLUS);
        $label = is_null($label) ? Yii::t('app', 'New') : $label;
        $options['class'] = 'btn-success';
        $options['type'] = 'button';
        $options['data-toggle'] = 'tooltip';
        $options['title'] = $label;

        return Yii::$app->html::button("{$icon} {$label}", $options);
    }

    /**
     * Encargado de entregar el botón a usar en las  para agregar
     * @param string $label Nombre del botón
     * @param  array $options Opciones Html
     * @return string
     */
    public static function btnDeleteLine($label = null, $options = [])
    {
        $icon = Yii::$app->html::iconFontAwesome(Yii::$app->html::ICON_TRASH);
        $label = is_null($label) ? Yii::t('app', 'Delete') : $label;
        $options['class'] = 'btn btn-danger';
        $options['type'] = 'button';
        $options['data-toggle'] = 'tooltip';
        $options['title'] = $label;

        return Yii::$app->html::button("{$icon} {$label}", $options);
    }

    /**
     * Entrega el Botón para Enviar Registro
     * @param  string  $label Label del Botón
     * @param  array  $options Opciones html del Botón
     * @return string   Button
     */
    public static function btnSend($label = null, $options = [])
    {
        $options['id'] = isset($options['id']) ? $options['id'] : 'send';
        $label = is_null($label) ? Yii::t('app', 'Save') : $label;
        $icon = isset($options['icon'])
            ? $options['icon']
            : Yii::$app->html::ICON_OK;
        $label = Yii::$app->html::iconFontAwesome($icon) . ' ' . $label;

        if (isset($options['class'])) {
            $options['class'] .= ' btn btn-primary';
        } else {
            $options['class'] = ' btn btn-primary';
        }

        return Yii::$app->html::submitButton($label, $options);
    }

    /**
     * Entrega el Botón para hacer reset a un Formulario
     * @param  string  $label Label del Botón
     * @param  array  $options Opciones html del Botón
     * @return string   Button
     */
    public static function btnReset($label = null, $options = [])
    {
        $options['id'] = isset($options['id']) ? $options['id'] : 'send';
        $label = is_null($label) ? Yii::t('app', 'Reset') : $label;
        $icon = isset($options['icon'])
            ? $options['icon']
            : Yii::$app->html::ICON_REFRESH;
        $label = Yii::$app->html::iconFontAwesome($icon) . ' ' . $label;

        if (isset($options['class'])) {
            $options['class'] .= ' btn btn-default';
        } else {
            $options['class'] = ' btn btn-default';
        }

        return Yii::$app->html::resetButton($label, $options);
    }

    /**
     * Entrega el Botón para Cancelar Registro
     * @param  string  $label Label del Botón
     * @param  array  $url Url de la accion en Controller
     * @return string   Button
     */
    public static function btnCancel(
        $label = null,
        $url = ['index'],
        $options = []
    ) {
        $label = is_null($label) ? Yii::t('app', 'Cancel') : $label;
        $icon = isset($options['icon'])
            ? $options['icon']
            : Yii::$app->html::ICON_REMOVE;
        $label = Yii::$app->html::iconFontAwesome($icon) . ' ' . $label;
        $btnOptions = array_merge(
            [
                'id' => isset($options['id']) ? $options['id'] : 'cancel',
                'class' => 'btn btn-default',
                'data-confirm' => Yii::t(
                    'app',
                    'Are you sure you want to cancel?'
                )
            ],
            $options
        );
        $btnOptions['class'] .= ' no-validate';
        return Yii::$app->html::a($label, $url, $btnOptions);
    }

    /**
     * Entrega un botón para cerrar una modal con un mensaje de confirmación
     * @return string
     */
    public static function btnCloseModalMessage(
        $label = null,
        $icon = null,
        $options = []
    ) {
        $icon = is_null($icon) ? Yii::$app->html::ICON_OK : $icon;
        $label = is_null($label) ? Yii::t('app', 'To accept') : $label;
        $label = Yii::$app->html::iconFontAwesome($icon) . ' ' . $label;

        if (isset($options['class'])) {
            $options['class'] = 'btn btn-default' . $options['class'];
        } else {
            $options['class'] = 'btn btn-default';
        }
        $options = array_merge(
            [
                'data-message' => Yii::t(
                    'app',
                    'Are you sure you want to cancel?'
                ),
                'onclick' => 'closeModalWithMessage(this)'
            ],
            $options
        );

        return Yii::$app->html::button($label, $options);
    }

    /**
     * Entrega el Botón para Cerrar modal
     * @param string $label Label del Botón
     * @param string $icon Icono del botón
     * @param array $options Opciones HTML
     * @return string   Button
     */
    public static function btnCloseModal(
        $label = null,
        $icon = null,
        $options = []
    ) {
        $icon = is_null($icon) ? Yii::$app->html::ICON_OK : $icon;
        $label = is_null($label) ? Yii::t('app', 'To accept') : $label;
        $label = Yii::$app->html::iconFontAwesome($icon) . ' ' . $label;

        if (isset($options['class'])) {
            $options['class'] = 'btn btn-default ' . $options['class'];
        } else {
            $options['class'] = 'btn btn-default';
        }

        $options['data-dismiss'] = 'modal';

        return Yii::$app->html::button($label, $options);
    }

    /**
     * Entrega la configuración por defecto del kartik\dynagrid\GridView
     *
     * @return array
     */
    public static function getDefaultConfigDynaGrid()
    {
        return [
            'storage' => DynaGrid::TYPE_SESSION,
            'showPersonalize' => true,
            'allowPageSetting' => true,
            'allowThemeSetting' => true,
            'allowFilterSetting' => false,
            'allowSortSetting' => false,
            'toggleButtonGrid' => [
                'label' => Yii::$app->html::iconFontAwesome(
                    Yii::$app->html::ICON_WRENCH
                ),
                'title' => '',
                'data-pjax' => false
            ],
            'deleteConfirmation' => Yii::t(
                'app',
                'Are you sure you want to delete the configuration?'
            ),
            'deleteMessage' => Yii::t('app', 'Remove all custom options ...')
        ];
    }

    /**
     * Entrega la configuración por defecto del kartik\grid\GridView
     *
     * @return array
     */
    public static function getDefaultConfigGridView($titleReport = null)
    {
        $title = static::getTitleHeading($titleReport);
        $titleReport = !empty($titleReport)
            ? $titleReport
            : ucfirst(str_replace(['-', '_'], ' ', Yii::$app->controller->id));
        return [
            'autoXlFormat' => true,
            'responsive' => true,
            'responsiveWrap' => false,
            'resizableColumns' => true,
            'hover' => true,
            'pjax' => true,
            'panelHeadingTemplate' => "<h3>{$title}</h3>",
            'panelTemplate' => '
            <div class="panel panel-bordered">
                {panelHeading}
                {panelBefore}
                {items}
                {panelFooter}
            </div>',
            'export' => [
                'label' => '',
                'fontAwesome' => false,
                'showConfirmAlert' => false,
                'target' => GridView::TARGET_BLANK,
                'options' => ['class' => 'btn btn-default'],
                'header' =>
                '<li role="presentation" class="dropdown-header">' .
                    Yii::t('app', 'Export data') .
                    '</li>',
                'menuOptions' => []
            ],
            'exportConfig' => [
                GridView::EXCEL => [
                    'label' => Yii::t('app', 'Excel'),
                    'alertMsg' => Yii::t(
                        'app',
                        'The Excel file will be generated for download.'
                    ),
                    'mime' => 'application/application/vnd.ms-excel',
                    'config' => [
                        'worksheet' => Yii::t('app', 'Worksheet'),
                        'cssFile' => ''
                    ],
                    'filename' => $titleReport
                ],
                GridView::PDF => [
                    'label' => Yii::t('app', 'PDF'),
                    'alertMsg' => Yii::t(
                        'app',
                        'El archivo de Pdf sera generado para descargar.'
                    ),
                    'config' => [
                        'methods' => [
                            'SetHTMLHeader' =>
                            Yii::$app->name .
                                ' - ' .
                                str_replace('_', ' ', ucwords($titleReport)),
                            'SetFooter' => ['{PAGENO}']
                        ],
                        'options' => [
                            'title' => Yii::t('app', 'Exported data'),
                            'subject' => Yii::t(
                                'app',
                                'PDF file generator by {name}',
                                ['{name}' => Yii::$app->name]
                            ),
                            'keywords' => Yii::t('app', 'grid, export, pdf')
                        ]
                    ],
                    'filename' => $titleReport
                ]
            ],
            'krajeeDialogSettings' => [
                'libName' => 'bootbox'
            ]
        ];
    }

    /**
     * Retorna el posible nombre para el titulo del CRUD
     *
     * @param string $initial Cadena de texto que se pasa como título por defecto
     * @return string
     */
    public static function getTitleHeading($initial = null)
    {
        return !empty($initial)
            ? $initial
            : Yii::t(
                'app',
                ucfirst(str_replace(['-', '_'], ' ', Yii::$app->controller->id))
            );
    }

    /**
     * Entrega la configuración para el widget kartik\widgets\DatePicker
     * @return array
     */
    public static function getDefaultConfigDatePicker()
    {
        return [
            'type' => \kartik\widgets\DatePicker::TYPE_COMPONENT_APPEND,
            'readonly' => true,
            'pluginOptions' => [
                'format' => 'yyyy-mm-dd',
                'autoclose' => true,
                'todayBtn' => 'linked',
                'todayHighlight' => true
            ]
        ];
    }

    /**
     * Entrega la configuración para el widget kartik\widgets\DateTimePicker
     * @return array
     */
    public static function getDefaultConfigDateTimePicker()
    {
        return [
            'type' => \kartik\widgets\DateTimePicker::TYPE_COMPONENT_APPEND,
            'readonly' => true,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd hh:ii:ss'
            ]
        ];
    }

    /**
     * Entrega la configuración para el widget kartik\widgets\Select2
     * @return array
     */
    public static function getDefaultConfigSelect2()
    {
        return [
            'pluginOptions' => [
                'allowClear' => true
            ]
        ];
    }

    /**
     * Entrega la configuración para el widget kartik\widgets\TimePicker
     * @return array
     */
    public static function getDefaultConfigTimePicker()
    {
        return [
            'pluginOptions' => [
                'minuteStep' => 5,
                'showMeridian' => false
            ],
            'options' => [
                'readonly' => true
            ]
        ];
    }

    /**
     * Entrega la configuración para el widget kartik\dialog\Dialog
     * @return array
     */
    public static function getDefaultConfigDialog()
    {
        return [
            'libName' => 'bootbox'
        ];
    }

    /**
     * Renderizado de elemento Dynagrid en las vistas, con columnas y demás
     *
     * @param string $title Título para el elemento
     * @param array $columns Columnas que se mostrarán
     * @param array $config Configuraciones para dynagrid
     * @return string
     */
    public static function dynagridComponent($title = 'any', $config = [])
    {
        return DynaGrid::widget(static::dynagridConfig($title, $config));
    }

    /**
     * Renderizado de elemento Dynagrid en las vistas, con columnas y demás
     *
     * @param string $title Título para el elemento
     * @param array $columns Columnas que se mostrarán
     * @param array $config Configuraciones para dynagrid
     * @return string
     */
    public static function dynagridConfig($title = 'any', $config = [])
    {
        $id = Inflector::slug($title);
        if (isset($config['gridOptions']['filterModel'])) {
            $config['columns'] = ArrayHelper::merge(
                $config['columns'],
                static::getCommonGridColumns($config['gridOptions']['filterModel'], $id)
            );
        }
        $config = ArrayHelper::merge([
            'gridOptions' => [
                'id' => 'grid-' . $id,
                'pjax' => true,
                'toolbar' => [
                    '{toggleData}',
                    [
                        'content' => (Yii::$app->controller->isModal
                            ? Yii::$app->ui::btnNew(
                                '',
                                ['create'],
                                [
                                    'onclick' =>
                                    "openModalGrid(this, '" . $id . "', 'create'); return false;"
                                ]
                            )
                            : Yii::$app->ui::btnNew('')) .
                            ' ' .
                            Yii::$app->ui::btnSearch(
                                '',
                                '#search-modal-' . $id
                            ) .
                            ' ' .
                            Yii::$app->ui::btnRefresh('')
                    ]
                ],
                'panel' => [
                    'type' => GridView::TYPE_DEFAULT
                ],
                'panelHeadingTemplate' =>
                '<h4 class="mb-0">' . (Yii::t('app', $title)) . '</h4>',
                'panelAfterTemplate' =>
                '<div class="row"><div class="col-12">{summary}</div></div>',
                'options' => [],
                'krajeeDialogSettings' => [
                    'libName' => 'bootbox'
                ]
            ],
            'options' => [
                'id' => 'dynagrid-' . $id
            ]
        ], $config);

        return $config;
    }

    /**
     * Devuelve un arreglo con las columnas comúnes que son necesarias en los grid
     *
     * @return array
     */
    public static function getCommonGridColumns($model, $id)
    {
        $userList = Yii::$app->user->identityClass::findAll([
            Yii::$app->user->identityClass::STATUS_COLUMN =>
            Yii::$app->user->identityClass::STATUS_ACTIVE
        ]);
        $result = [
            [
                'attribute' => $model::STATUS_COLUMN,
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => Yii::$app->strings::getCondition(),
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Empty'),
                    'id' => 'grid-' . $model::STATUS_COLUMN
                ],
                'value' => function ($model) {
                    return Yii::$app->strings::getCondition(
                        $model->{$model::STATUS_COLUMN}
                    );
                },
                'visible' => false
            ],
            [
                'attribute' => $model::CREATED_BY_COLUMN,
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(
                    $userList,
                    Yii::$app->user->identityClass::primaryKey(),
                    'username'
                ),
                'value' => function ($model) use ($userList) {
                    foreach ($userList as $user) {
                        if (
                            $model->{$model::CREATED_BY_COLUMN} ==
                            $user->{$user::primaryKey()[0]}
                        ) {
                            return $user->username;
                        }
                    }
                },
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Empty'),
                    'id' => 'grid-' . $model::CREATED_BY_COLUMN
                ],
                'visible' => false
            ],
            [
                'attribute' => $model::CREATED_AT_COLUMN,
                'filterType' => GridView::FILTER_DATETIME,
                'format' => 'datetime',
                'filterInputOptions' => [
                    'id' => 'grid-' . $model::CREATED_AT_COLUMN
                ],
                'visible' => false
            ],
            [
                'attribute' => $model::UPDATED_BY_COLUMN,
                'filterType' => GridView::FILTER_SELECT2,
                'filter' => ArrayHelper::map(
                    $userList,
                    Yii::$app->user->identityClass::primaryKey(),
                    'username'
                ),
                'value' => function ($model) use ($userList) {
                    foreach ($userList as $user) {
                        if (
                            $model->{$model::UPDATED_BY_COLUMN} ==
                            $user->{$user::primaryKey()[0]}
                        ) {
                            return $user->username;
                        }
                    }
                },
                'filterWidgetOptions' => [
                    'pluginOptions' => [
                        'allowClear' => true
                    ]
                ],
                'filterInputOptions' => [
                    'placeholder' => Yii::t('app', 'Empty'),
                    'id' => 'grid-' . $model::UPDATED_BY_COLUMN
                ],
                'visible' => false
            ],
            [
                'attribute' => $model::UPDATED_AT_COLUMN,
                'filterType' => GridView::FILTER_DATETIME,
                'format' => 'datetime',
                'filterInputOptions' => [
                    'id' => 'grid-' . $model::UPDATED_AT_COLUMN
                ],
                'visible' => false
            ]
        ];

        if ($model->includeActionColumns) {

            $result[] = [
                'class' => \ticmakers\core\base\ActionColumn::class,
                'isModal' => Yii::$app->controller->isModal,
                'dynaModalId' => $id
            ];
        }

        return $result;
    }

    /**
     * Renderiza una columna según una configuración pasada en "options" con la siguiente estructura
     * ```
     * [
     *     'name' => 'nombre_columna',
     *     'type' => 'text' // nombre del método que se utilizará para renderizar
     *     'label' => 'Nombre columna',
     *     'items' => [],
     *     'options' => [],
     *     'fieldOptions' => [],
     *     'widget' => [],
     *     'containerOptions' => [] // Opciones para el contenedor esto puede ser la clase de las columnas
     * ]
     * ```
     *
     * @param $form Instancia de formulario
     * @param $model Instancia de modelo
     * @param array $options
     * @return string
     */
    public static function renderField($form, $model, $options = [])
    {
        $field = null;
        if (!is_array($options)) {
            $options = ['name' => $options];
        }
        $options['options'] = ArrayHelper::getValue($options, 'options', []);
        $options['options'] = Yii::$app->arrayHelper::mergeMultiple(
            $options['options'],
            [
                'help' => '',
                'popover' => $model->getHelp($options['name'])
            ]
        );
        $field = $form->field($model, $options['name'], $options['options']);
        if (isset($options['widget']) && !empty($options['widget'])) {
            $class = $options['widget']['class'];
            unset($options['widget']['class']);
            $field = call_user_func_array(
                [$field, 'widget'],
                [$class, $options['widget']]
            );
        } else {
            if (isset($options['type']) && !empty($options['type'])) {

                if (\in_array($options['type'], ['radioList', 'dropDownList'])) {
                    $field = call_user_func_array(
                        [$field, $options['type']],
                        [$options['data'] ?? [], $options['fieldOptions']]
                    );
                } else {
                    $field = call_user_func_array(
                        [$field, $options['type']],
                        [$options['fieldOptions']]
                    );
                }
            }
        }
        if (isset($options['label'])) {
            $field->label($options['label']);
        }
        return $field;
    }

    /**
     * Retorna un botón un icono
     *
     * @param string $label Label del botón
     * @param string $icon Cadena o arreglo con la clase del icono que se quiere utilizar o la configuración para el tag "i"
     * @param array $options Opciones para el botón
     */
    public static function buttonIcon($label = '', $icon = '', $options = [])
    {
        $icon = self::icon($icon);
        $content = $icon . ' ' . $label;
        return Yii::$app->html::tag('button', $content, $options);
    }

    /**
     * Retorna un tag con el icono según los attributos pasados
     *
     * @param array $options Opciones para el elemento o la clase directamente
     */
    public static function icon($options)
    {
        if (!is_array($options)) {
            $options = [
                'class' => $options
            ];
        }
        return Yii::$app->html::tag('i', null, $options);
    }

    /**
     * Undocumented function
     *
     * @param [type] $form
     * @param [type] $model
     * @param [type] $context
     * @param string $viewName
     * @return void
     */
    public static function renderFormColumns($form, $modelsFormColums, $context, $viewName = '_form_tpl', $typeRender = 'C', $isMultiple = false)
    {

        $sufixTypesRender = [
            'C' => '-create',
            'U' => '-update',
            'S' => '-search',
            'I' => '-index',
            'T' => '-search-top',
        ];

        if (!is_array($modelsFormColums)) {
            $modelsFormColums = ['model' => $modelsFormColums];
        }

        $formColumns = [];
        foreach ($modelsFormColums as $keyModel => $model) {
            if (!\is_array($model)) {
                $currentFormColumns = $model->formColumns();
                foreach ($currentFormColumns as $keyColumn => $dataColumn) {
                    $dataColumn['model'] = $keyModel;
                    $formColumns["{$keyModel}.{$keyColumn}"] = $dataColumn;
                }
            }
        }


        $html = Yii::$app
            ->getView()
            ->render(
                $context->loadViewFile($viewName),
                [
                    'formColumns' => $formColumns,
                    'form' => $form,
                    'model' => $modelsFormColums['model']
                ],
                $context
            );

        $prefixIdModels = [];
        $prefixId = null;

        $attributesTemplate = [];
        preg_match_all('/\{\{([a-zA-Z0-9_\.]+):(\d+)\}\}/m', $html, $attributesTemplate);
        // if ($typeRender == 'C') {
        //     dd($html, $attributesTemplate);
        // }

        foreach ($formColumns as $key => $column) {

            $attribute = $column['attribute'];
            $modelKey = $column['model'];
            $model = $modelsFormColums[$column['model']];
            unset($column['model'], $column['attribute']);

            if ($isMultiple && !isset($prefixIdModels[$modelKey])) {
                $prefixIdModels[$modelKey] = "{$model::$counter}";
                $model::$counter++;
            }
            if (isset($prefixIdModels[$modelKey])) {
                $prefixId = $prefixIdModels[$modelKey];
            }


            $options = Yii::$app->arrayHelper::mergeMultiple(
                $column,
                [
                    'name' => ($isMultiple ? "[{$prefixId}]{$attribute}" : $attribute),
                    'options' => [
                        'inputOptions' => [
                            'form' => $form->id,
                            'id' => Yii::$app->html::activeInputId($model, $attribute, ($prefixId ? "{$prefixId}-" : null), $sufixTypesRender[$typeRender])
                        ]
                    ],
                    'fieldOptions' => [
                        'form' => $form->id,
                        'id' => Yii::$app->html::activeInputId($model, $attribute, ($prefixId ? "{$prefixId}-" : null), $sufixTypesRender[$typeRender]),
                    ],
                ]
            );

            if (\in_array($column['type'] ?? '', ['radioList'])) {
                $fieldOptions = $options['fieldOptions'];
                unset($options['fieldOptions'], $fieldOptions['id']);
                $options['fieldOptions'] = [
                    'itemOptions' => $fieldOptions
                ];
            }

            if ($typeRender == 'T') {
                $options['fieldOptions'] = [
                    'class' => 'form-control header-filter'
                ];
                $options['options'] = [
                    'inputOptions' => [
                        'class' => 'form-control header-filter'
                    ]
                ];
            }

            // dd($options);


            // if (!isset($options['fieldOptions']['id'])) {
            //     $options['fieldOptions']['id'] = Yii::$app->html::activeInputId($model, $attribute, ($prefixId ? "{$prefixId}-" : null), $sufixTypesRender[$typeRender]);
            // }


            if (isset($options['widget']) && !isset($options['widget']['options']['id'])) {
                $options['widget']['options']['id'] = Yii::$app->html::activeInputId($model, $attribute, ($prefixId ? "{$prefixId}-" : null), $sufixTypesRender[$typeRender]);
            }
            if (isset($options['widget']) && !isset($options['widget']['options']['form'])) {
                $options['widget']['options']['form'] = $form->id;
            }




            foreach ($attributesTemplate[1] as $keyFieldTemplate => $fieldTemplate) {
                if ($fieldTemplate == "{$modelKey}.{$attribute}" || $fieldTemplate == $attribute) {

                    $tabIndexField = (int) $attributesTemplate[2][$keyFieldTemplate];

                    if (isset($options['widget'])) {
                        if (in_array($options['widget']['class'], ['kartik\select2\Select2', 'kartik\widgets\Select2'])) {
                            $options['widget']['options']['tabindex'] = $tabIndexField;
                        } else {
                            $options['widget']['options']['tabindex'] = $tabIndexField;
                        }
                        unset($options['fieldOptions']['tabindex']);
                        // dd($options);
                    }

                    // if (isset($options['inputOptions'])) {
                    $options['fieldOptions']['tabindex'] = $tabIndexField;
                    $options['options']['inputOptions']['tabindex'] = $tabIndexField;
                    // }
                    // if ($attribute == 'resume_link') {
                    //     dd($options, $html, $attributesTemplate);
                    // }

                    $columnHtml = Yii::$app->ui::renderField(
                        $form,
                        $model,
                        $options
                    );


                    $html = str_replace($attributesTemplate[0][$keyFieldTemplate], $columnHtml, $html);
                    break;
                }
            }
        }


        return $html;
    }

    /**
     * Undocumented function
     *
     * @param [type] $formColumns
     * @param string $type
     * @return void
     */
    public static function buildDefaultRenderTpl($formColumns, $instance, $type = 'C')
    {
        $inSearch = [
            $instance::primaryKey()[0],
            $instance::STATUS_COLUMN,
            $instance::CREATED_BY_COLUMN,
            $instance::CREATED_AT_COLUMN,
            $instance::UPDATED_BY_COLUMN,
            $instance::UPDATED_AT_COLUMN
        ];


        $renderFormColumns = [];
        foreach ($formColumns as $key => $column) {
            if (!isset($column['render'])) {
                $column['render'] = in_array($key, $inSearch) ? ['S'] : ['C', 'U'];
            }
            if (in_array($type, $column['render'])) {
                $renderFormColumns[$key] = $column;
            }
        }

        $countColumns = count($renderFormColumns);
        foreach ($renderFormColumns as $key => $column) {
            $options = Yii::$app->arrayHelper::mergeMultiple(
                $column,
                [
                    'name' => $key
                ]
            );

            $options['containerOptions'] = Yii::$app->arrayHelper::getValue(
                $options,
                'containerOptions',
                ['class' => ($countColumns >= 2 ? 'col-12 col-md-6' : 'col-12')]
            );

            $tabIndex = ($options['fieldOptions']['tabindex'] ?? 1);

            echo Yii::$app->html::tag(
                'div',
                '{{' . $key . ':' . $tabIndex . '}}',
                $options['containerOptions']
            );
        }
    }

    /**
     * Entrega el Botón para Enviar Registro y volver al mismo elemento
     * @param string $label Label del Botón
     * @param array $options Opciones html del Botón
     * @return string Button
     */
    public static function btnSaveBack($label = null, $options = [])
    {
        $options['id'] = isset($options['id']) ? $options['id'] : 'btn-send-back';
        $label = is_null($label) ? Yii::t('app', 'Save and create another') : $label;
        $icon = isset($options['icon']) ? $options['icon'] : HtmlGenerator::ICON_REPEAT;
        $label = HtmlGenerator::iconFontAwesome($icon) . ' ' . $label;
        if (isset($options['class'])) {
            $options['class'] .= ' btn btn-outline-primary';
        } else {
            $options['class'] = ' btn btn-outline-primary';
        }
        $options['onclick'] = 'onclickSend(this, true)';

        return HtmlGenerator::button($label, $options);
    }

    /**
     * Entrega el Botón para Enviar Registro y volver al mismo elemento
     * @param string $label Label del Botón
     * @param array $options Opciones html del Botón
     * @return string Button
     */
    public static function btnSaveAndContinue($label = null, $options = [])
    {
        $options['id'] = isset($options['id']) ? $options['id'] : 'btn-send-continue';
        $label = is_null($label) ? Yii::t('app', 'Save and continue') : $label;
        $icon = isset($options['icon']) ? $options['icon'] : HtmlGenerator::ICON_CHECK_SQUARE;
        $label = HtmlGenerator::iconFontAwesome($icon) . ' ' . $label;
        if (isset($options['class'])) {
            $options['class'] .= '  btn btn-primary';
        } else {
            $options['class'] = ' btn btn-primary';
        }
        $options['onclick'] = 'onclickSend(this, true)';

        return HtmlGenerator::button($label, $options);
    }

    public static function getIconsMenu()
    {
        return [];
    }
}
