<?php

namespace ticmakers\core\i18n;

use yii\i18n\MessageSource;


/**
 *
 *
 * @author Juan Sebastian Muñoz Reyes <juan.munoz@ticmakers.com>
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @author Kevin Daniel Guzman Delgadillo <kevin.guzman@ticmakers.com>
 */

class LabelDbMessageSource extends MessageSource
{

    /**
     * Translates a message to the specified language.
     *
     * Note that unless [[forceTranslation]] is true, if the target language
     * is the same as the [[sourceLanguage|source language]], the message
     * will NOT be translated.
     *
     * If a translation is not found, a [[EVENT_MISSING_TRANSLATION|missingTranslation]] event will be triggered.
     *
     * @param string $category the message category
     * @param string $message the message to be translated
     * @param string $language the target language
     * @return string|bool the translated message or false if translation wasn't found or isn't required
     */
    public function translate($category, $message, $language)
    {
        $text = $message;
        $traslations = json_decode($message, true);
        if (json_last_error() === 0) {
            $text = $traslations['default'];
            if (isset($traslations['language'][$language])) {
                $text = $traslations['language'][$language];
            }
        }
        return $text;
    }
}
