<?php

namespace ticmakers\core\i18n;

use Yii;
use yii\i18n\MissingTranslationEvent;

class TranslationEventHandler
{
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        // if (!YII_ENV_DEV) {
            // $event->translatedMessage = "@MISSING: {$event->category}.{$event->message} FOR LANGUAGE {$event->language} @";
        // } else {
            $event->translatedMessage =  Yii::$app->translate->text($event->message, $event->sender->sourceLanguage, $event->language);
        // }
    }
}
