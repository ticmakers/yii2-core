<?php

namespace ticmakers\core\redis;

use Yii;
use yii\helpers\Inflector;

/**
 * Éste es el modelo base para las consultas y operaciones en memoria.
 * 
 *
 * @package ticmakers\core\redis\Model
 *
 * @property integer $id 
 * @property string $transaction 
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@kulturfit.com>
 * @copyright Copyright (c) 2019 KulturFit S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Model extends \yii\redis\ActiveRecord
{

    const STATUS_ACTIVE = 'Y';
    const STATUS_INACTIVE = 'N';
    const STATUS_COLUMN = 'active';
    const CREATED_BY_COLUMN = 'created_by';
    const CREATED_AT_COLUMN = 'created_at';
    const UPDATED_BY_COLUMN = 'updated_by';
    const UPDATED_AT_COLUMN = 'updated_at';
    const DEFAULT_USER_ID = 1;

    const RENDER_CREATE = 'C';
    const RENDER_UPDATE = 'U';
    const RENDER_SEARCH = 'S';
    const RENDER_GRID = 'G';

    /**
     * Undocumented variable
     *
     * @var boolean
     */
    public $includeActionColumns = true;

    /**
     * @var [type] Undocumented variable
     */
    public static $modelClass;

    /**
     * @var [type] Undocumented variable
     */
    protected $modelInstance;

    /**
     * Undocumented function
     *
     * @return void
     */
    public function init()
    {
        $this->modelInstance = Yii::createObject(static::$modelClass);
        $this->{$this::STATUS_COLUMN} = static::STATUS_ACTIVE;
        parent::init();
    }

    /**
     * @return array the list of attributes for this record
     */
    public function attributes()
    {
        $base = [
            'id',
            'transaction',
        ];

        return Yii::$app->arrayHelper::merge($base, $this->modelInstance->attributes());
    }


    /**
     * Define las reglas, filtros y datos por defecto para las columnas.
     *
     * @return array
     */
    public function rules()
    {
        $newRules = [
            ['transaction', 'safe'],
            ['transaction', 'default', 'value' => Yii::$app->request->get('t', Yii::$app->security->generateRandomString(3))]
        ];

        return Yii::$app->arrayHelper::merge($newRules, $this->modelInstance->rules());
    }

    /**
     * Define los labels para las columnas del modelo.
     *
     * @return array
     */
    public function attributeLabels()
    {
        return $this->modelInstance->attributeLabels();
    }

    /**
     * Retorna un arreglo tipo clave valor según la llave primaria y la primera columna no primaria de la tabla.
     *
     * @param boolean $list Si necesitas un List (por lo general para dropdownlist) o no
     * @param array $attributes Condiciones para la consulta
     * @return array|ActiveRecord[]
     */
    public static function getData($list = true, $attributes = [], $orderBy = [])
    {
        if (!isset($attributes[static::$modelClass::STATUS_COLUMN])) {
            $attributes[static::$modelClass::STATUS_COLUMN] = static::$modelClass::STATUS_ACTIVE;
        }

        if (empty($orderBy)) {
            $orderBy[static::$modelClass::getNameFromRelations()] = SORT_ASC;
        }

        $query = new \ArrayObject(static::find()->where($attributes)->orderBy($orderBy)->asArray()->all());
        if ($list) {
            $query = \yii\helpers\ArrayHelper::map($query,  static::columnInCrud(), static::$modelClass::getNameFromRelations());
        }
        return $query;
    }

    /**
     * Undocumented function
     *
     * @return void 
     */
    public function saveData()
    {
        $allData = static::getData(false, ['transaction' => $this->transaction]);
        $result = true;
        foreach ($allData ?? [] as $data) {
            unset($data['id'], $data['transaction']);
            $model = Yii::createObject(static::$modelClass);
            $model->setAttributes($data);
            $result &= $model->save();
        }
        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getHelp($attribute = null)
    {
        return $this->modelInstance->getHelp($attribute);
    }


    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $newBehaviors = [];
        return Yii::$app->arrayHelper::merge($this->modelInstance->behaviors(), $newBehaviors);
    }

    /**
     * @inheritDoc
     */
    public function formColumns()
    {
        $formColumns = [];
        return Yii::$app->arrayHelper::merge($this->modelInstance->formColumns(), $formColumns);
    }

    /**
     * @inheritDoc
     */
    public function gridColumns()
    {
        $id = Inflector::slug(static::crudTitle());
        $gridColumns = [
            [
                'class' => \ticmakers\core\base\ActionColumn::class,
                'isModal' => true,
                //'controller' => '/members-redis',
                'dynaModalId' => $id
            ]
        ];
        return Yii::$app->arrayHelper::merge($this->modelInstance->gridColumns(), $gridColumns);
    }

    /**
     * @inheritDoc
     */
    public function depDropDependencies()
    {
        $depDropDependencies = [];
        return Yii::$app->arrayHelper::merge($this->modelInstance->depDropDependencies(), $depDropDependencies);
    }

    /**
     * Define la columna que se mostrará en los formularios para identificar el recurso
     * 
     * @return string
     */
    public static function columnInCrud()
    {
        return static::$modelClass::primaryKey()[0];
    }

    /**
     * Define el nombre que se mostrará en el CRUD.
     * 
     * @return string
     */
    public static function crudTitle()
    {
        return Yii::t('app', ucfirst(str_replace('_', ' ', static::$modelClass::tableName())));
    }

    /**
     * Undocumented function
     *
     * @return void
     */
    public static function getNameFromRelations()
    {
        return static::$modelClass::getNameFromRelations();
    }
}
