<?php

namespace ticmakers\core\rest;

use Yii;
use yii\web\Response;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\HttpBasicAuth;

/**
 * ActiveController Implementa las Acciones REST disponibles para los controladores del módulo Api.
 *
 * @package ticmakers
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ActiveController extends \yii\rest\ActiveController
{

    /**
     * Permite definir el modelo para las busquedas
     * @var type
     */
    public $searchModel;

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey;

    /**
     * Permite definir la estructura para procesar los datos con campos adicionales
     * @var array
     */
    public $processStructure = [];

    /**
     * La configuración para crear el serializador para el formato de respuesta.
     * @var array
     */
    public $serializer = [
        'class'              => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $response         = Yii::$app->response;
        $response->format = \yii\web\Response::FORMAT_JSON;

        Yii::$app->user->enableSession = false;

        if ($this->searchModel === null)
        {
            throw new InvalidConfigException('The "searchModel" property must be set.');
        }
    }

    /**
     * Retorna la lista de behaviors que el controlador implementa
     *
     * @return array
     */
    public function behaviors()
    {
        $this->module->cors();

        $behaviors                                                     = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;

        $behaviors['authenticator'] = [
            'class'       => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
            ],
        ];
        return $behaviors;
    }

    /**
     * Retorna la parametrización para las acciones por defecto
     *
     * @return array
     */
    public function actions()
    {
        $actions            = parent::actions();
        $actions['delete']  = [
            'class'       => 'ticmakers\core\rest\actions\DeleteAction',
            'modelClass'  => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];
        $actions['restore'] = [
            'class'       => 'ticmkaers\core\rest\actions\RestoreAction',
            'modelClass'  => $this->modelClass,
            'checkAccess' => [$this, 'checkAccess'],
        ];

        if (!empty($this->primaryKey))
        {
            $actions['sync'] = [
                'class'       => 'ticmakers\core\rest\actions\SyncAction',
                'modelClass'  => $this->modelClass,
                'searchModel' => $this->searchModel,
                'primaryKey'  => $this->primaryKey,
                'checkAccess' => [$this, 'checkAccess'],
            ];

            if (!empty($this->processStructure))
            {
                $actions['sync']['class']            = 'ticmakers\core\rest\actions\SyncActionMultiple';
                $actions['sync']['processStructure'] = $this->processStructure;
            }

            $actions['create-all'] = [
                'class'       => 'ticmakers\core\rest\actions\CreateAllAction',
                'modelClass'  => $this->modelClass,
//                'searchModel' => $this->searchModel,
                'primaryKey'  => $this->primaryKey,
                'checkAccess' => [$this, 'checkAccess'],
            ];
        }

        // customize the data provider preparation with the "prepareDataProvider()" method
        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    /**
     * Retorna el DataProvider para la acction index
     *
     * @return \yii\data\ActiveDataProvider
     */
    public function prepareDataProvider()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $searchModel = Yii::createObject($this->searchModel);

        return $searchModel->search(Yii::$app->request->queryParams);
    }

    /**
     * Declara los verbos HTTP permitidos.
     *
     * @return array
     */
    public function verbs()
    {
        $verbs           = parent::verbs();
        $verbs["index"]  = ['GET'];
        $verbs["update"] = ['PUT', 'PATCH', 'POST'];
        $verbs["delete"] = ['PUT', 'PATCH'];
        return $verbs;
    }

}
