<?php

namespace ticmakers\core\rest;

use yii\web\Response;
use yii\filters\auth\CompositeAuth;
use ticmakers\core\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;

/**
 * Controller Implementa las Acciones REST disponibles para los controladores del módulo Api.
 *
 * @package ticmakers
 * @subpackage rest/controllers
 * @category Controllers
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class Controller extends \yii\rest\Controller
{

    /**
     * Retorna la lista de behaviors que el controlador implementa en el rest api
     *
     * @return array
     */
    public function behaviors()
    {
        $this->module->cors();

        $behaviors                                                     = parent::behaviors();
        $behaviors['contentNegotiator']['formats']['application/json'] = Response::FORMAT_JSON;

        $behaviors['authenticator'] = [
            'class'       => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
            ],
        ];
        return $behaviors;
    }

}
