<?php

namespace ticmakers\core\rest\actions;

use yii\web\ServerErrorHttpException;
use yii\rest\Action;

/**
 * RestoreAction Acción restaurar disponible en el API REST.
 *
 * @package ticmakers
 * @subpackage rest/actions
 * @category Action
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class RestoreAction extends Action
{

    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {

        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $model->{$model::STATUS_COLUMN} = $model::STATUS_ACTIVE;

        if ($model->update() === false) {
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        return $model;
    }
}
