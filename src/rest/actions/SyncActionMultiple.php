<?php

namespace ticmakers\core\rest\actions;

use Yii;

/**
 * Pemite obtener los listados correspondientes para realizar inserción, actualización y elminación de registros para el modelo actual
 *
 * @package ticmakers
 * @subpackage rest/actions
 * @category Actions
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class SyncActionMultiple extends \yii\rest\Action
{

    /**
     * Modelo para las busquedas
     * @var string
     */
    public $searchModel;

    /**
     * Llave primaria del modelo para la sincronización
     * @var string
     */
    public $primaryKey;

    /**
     * Permite definir la estructura para procesar los datos con fields adicionales
     * @var array
     */
    public $processStructure;

    /**
     * Runs the action
     *
     * @return string result content
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        $searchModel             = Yii::createObject($this->searchModel);
        $searchModel->primaryKey = $this->primaryKey;
        $searchModel->perPage    = 0;
        $dataProviderInsert      = $searchModel->search(
            Yii::$app->request->queryParams,
            ''
        );
        $dataProviderInsert->prepare();

        $datosInsertar  = $dataProviderInsert->models;
        $llavesInsertar = array_keys(\yii\helpers\ArrayHelper::map(
            $datosInsertar,
            $this->primaryKey,
            $this->primaryKey
        ));

        $dataProviderDelete = $searchModel->searchDelete(
            Yii::$app->request->queryParams,
            ''
        );
        $dataProviderDelete->prepare();

        $searchModel->notIn = $llavesInsertar;
        $dataProviderUpdate = $searchModel->searchUpdate(
            Yii::$app->request->queryParams,
            ''
        );
        $dataProviderUpdate->prepare();
        $datosActualizar    = [];



        $datosPorProcesar = [];
        foreach ($dataProviderUpdate->models as $model) {
            $datosPorProcesar[] = $model->toArray();
        }

        $this->procesarDatos(
            $datosActualizar,
            $datosPorProcesar,
            $this->processStructure
        );


        $datosEliminar = [];

        foreach ($dataProviderDelete->models as $model) {
            $datosEliminar[] = $model->toArray([$this->primaryKey]);
        }

        return [
            'inserts' => $datosInsertar,
            'updates' => $datosActualizar,
            'deletes' => $datosEliminar
        ];
    }

    private function procesarDatos(&$datosProcesados, $datos, $estructura)
    {
        if (!empty($datos)) {
            foreach ($datos as $datoProcesar) {
                $table         = $estructura['table'];
                $primaryKeyAttribute = $estructura['primaryKeyAttribute'];
                $relations    = $estructura['relations'] ?? [];
                $fields        = $estructura['fields'] ?? [];

                $structureFields = [];
                foreach ($fields as $field) {
                    $structureFields[$field] = $datoProcesar[$field];
                }

                $datosProcesados[$table][] = [
                    'set'   => $structureFields,
                    'where' => [$primaryKeyAttribute => $datoProcesar[$primaryKeyAttribute]]
                ];

                if (!empty($relations)) {
                    foreach ($relations as $realtion) {

                        $this->procesarDatos(
                            $datosProcesados,
                            $datoProcesar[$realtion['keyData']],
                            $realtion
                        );
                    }
                }
            }
        }

        return $datosProcesados;
    }
}
