<?php

namespace ticmakers\core\rest\traits;

use yii\data\ActiveDataProvider;

/**
 * SyncSearch
 *
 * @package ticmakers/rest
 * @subpackage traits
 * @category Traits
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
trait SyncSearch
{

    public $primaryKey;

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchUpdate($params)
    {
        if (isset($params['perPage'])) {
            $this->perPage = $params['perPage'];
        }

        $query = static::find();
        $alias = '';

        if (method_exists($this, 'queryUpdate')) {
            $this->queryUpdate($query);
            if (is_array($query->from)) {
                $alias = array_keys($query->from)[0] . '.';
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'defaultPageSize' => $this->perPage,
            ],
        ]);

        unset($params['notIn']);

        $this->setAttributes($params);

        $this->lastDate        = str_replace('_', ' ', $this->lastDate);
        $query->andFilterWhere(['>', $alias . static::UPDATED_AT_COLUMN, $this->lastDate])
            ->andFilterWhere(['not in', $alias . $this->primaryKey, $this->notIn])
            ->andFilterWhere(['ilike', $alias . static::STATUS_COLUMN, static::STATUS_ACTIVE]);
        $dataProvider->pagination = false;

        return $dataProvider;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function searchDelete($params)
    {
        if (isset($params['notIn'])) {
            $params['notIn'] = \yii\helpers\Json::decode($params['notIn']);
            $this->notIn     = is_array($params['notIn']) ? array_values($params['notIn']) : [];
        }

        $query = static::find();
        $alias = '';

        if (method_exists($this, 'queryDelete')) {
            $this->queryDelete($query);
            if (is_array($query->from)) {
                $alias = array_keys($query->from)[0] . '.';
            }
        }

        $query->select(["{$alias}{$this->primaryKey}"]);

        $dataProvider = new ActiveDataProvider([
            'query'      => $query,
            'pagination' => [
                'defaultPageSize' => $this->perPage,
            ],
        ]);

        $this->setAttributes($params);

        if (empty($this->lastDate)) {
            // uncomment the following line if you do not want to return any records when validation fails
            $query->andWhere('0=1');
        } else {
            $this->lastDate = str_replace('_', ' ', $this->lastDate);
            $query->andFilterWhere(['>', "{$alias}" . static::STATUS_COLUMN, $this->lastDate]);

            $query->andFilterWhere(['in', "{$alias}{$this->primaryKey}", $this->notIn])
                ->andFilterWhere([
                    'ilike', $alias . static::STATUS_COLUMN,
                    static::STATUS_INACTIVE
                ]);

            $dataProvider->pagination = false;
        }

        return $dataProvider;
    }
}
