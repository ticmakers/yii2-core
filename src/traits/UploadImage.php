<?php

namespace ticmakers\core\traits;

use Yii;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\helpers\StringHelper;

/**
 * UploadImage
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@kulturfit.com>
 * @copyright (c) 2018, KulturFit S.A.S.
 * @version 0.0.1
 */
trait UploadImage
{
    private $_directory;
    private $_urlDirectory;
    private $_keyTemporalDirectory;
    private $_urlImage;
    private $_basePath = '@webroot/uploads';
    private $_baseUrl = '@web/uploads/';
    private $_defaultImage = '@web/images/no-image.png';
    private $_suffixOld  = '-old';

    /**
     * 
     * 
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $this->renameDirectory();
        }
    }

    /**
     * 
     * 
     */
    public function getUrlImage()
    {
        return $this->_urlImage;
    }

    /**
     * 
     * 
     */
    public function setUrlImage($urlImage)
    {
        $this->_urlImage = $urlImage;
        return $this;
    }

    /**
     * 
     * 
     */
    public function getDirectory()
    {
        return $this->_directory;
    }

    /**
     * 
     * 
     */
    public function setDirectory($directory)
    {
        $this->_directory = $directory;
        return $this;
    }

    /**
     * 
     * 
     */
    public function getUrlDirectory()
    {
        return $this->_urlDirectory;
    }

    /**
     * 
     * 
     */
    public function setUrlDirectory($urlDirectory)
    {
        $this->_urlDirectory = $urlDirectory;
        return $this;
    }

    /**
     * 
     * 
     */
    public function upload()
    {

        if (
            $this->validate()
            && is_array($this->fileAttributes())
            && count($this->fileAttributes()) > 0
        ) {
            foreach ($this->fileAttributes() as $field => $fileAttribute) {
                $this->verifyDataField($fileAttribute, $value, $modelInstance);
                $this->verifyDirectory($field);
                $this->processFile($field, $value, $modelInstance);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * 
     */
    private function processFile($field, $value, &$modelInstance)
    {
        $oldImages = explode(',', (Yii::$app->request->post($this->formName())[$field . $this->_suffixOld] ?? ''));
        $oldImages = array_map(function ($item) {
            return \str_replace($this->getUrlDirectory(), '', $item);
        }, $oldImages);
        if (
            is_array($modelInstance->{$field})
            && count($modelInstance->{$field}) > 0
        ) {
            foreach ($modelInstance->{$field} as $keyFile => $file) {
                $this->saveFile($file, $field, $modelInstance, true, $keyFile);
            }
            $modelInstance->{$field} = array_merge($modelInstance->{$field}, $oldImages);
        } else if (!empty($modelInstance->{$field})) {
            $this->saveFile($modelInstance->{$field}, $field, $modelInstance);
        } else {
            foreach ($this->fileAttributes() as $attribute => $configAttribute) {
                if ($field == $attribute) {
                    if (isset($configAttribute['multiple']) && $configAttribute['multiple']) {
                        $modelInstance->{$field} = $oldImages;
                    } else {
                        $modelInstance->{$field} = current($oldImages);
                    }
                }
            }
        }
    }

    /**
     * 
     * 
     */
    private function saveFile($file, $attribute, &$modelInstance, $isMultiple = false, $keyFile = 0)
    {
        if (!is_string($file) && !empty($file)) {
            $currentValue = $attribute . '-' . date('Ymd-His') . rand(100, 999) . '.' . $file->extension;
            if ($isMultiple) {
                $newValue = $modelInstance->{$attribute};
                $newValue[$keyFile] = $currentValue;
                $modelInstance->setAttribute($attribute, $newValue);
            } else {
                $modelInstance->setAttribute($attribute, $currentValue);
            }

            $file->saveAs(
                Url::to($this->getDirectory()) . DIRECTORY_SEPARATOR . $currentValue,
                true
            );
        };
    }

    /**
     * 
     * 
     */
    public function getFiles($attribute)
    {
        $files = [];
        if (isset($this->fileAttributes()[$attribute])) {

            $isMultiple = $this->fileAttributes()[$attribute]['multiple'];

            if (is_array($this->{$attribute}) && count($this->{$attribute}) > 0) {
                foreach ($this->{$attribute} as $file) {
                    $files[] =  $file;
                }
            } else if ($isMultiple) {
                $files[] = $this->{$attribute};
            } else {
                $files = $this->{$attribute};
            }
        }

        return $files;
    }

    /**
     * 
     * 
     */
    private function verifyDirectory($fieldName)
    {
        $directoryName = $this->getDirectoryName();

        if ($this->isNewRecord && empty($this->_keyTemporalDirectory)) {
            $this->_keyTemporalDirectory = sha1(time() . rand(1000, 9999));
        } else {
            $this->_keyTemporalDirectory = $this->getPrimaryKey();
        }

        $verifyDirectory = Yii::getAlias("{$this->basePath}/{$directoryName}/{$this->_keyTemporalDirectory}");
        $directory = "{$this->basePath}/{$directoryName}/{$this->_keyTemporalDirectory}";
        $urlUpload = "{$this->baseUrl}{$directoryName}/{$this->_keyTemporalDirectory}/";


        if (!file_exists($verifyDirectory)) {
            mkdir($verifyDirectory, 0775, true);
        }

        $this->setUrlDirectory(Url::to($urlUpload, true));
        $this->setDirectory($directory);
    }

    /**
     * 
     */
    public function getFileName()
    {
        $data = [];
        foreach ($this->fileAttributes() as $field => $fileAttribute) {
            $this->verifyDataField($fileAttribute, $value, $modelInstance);
            $data[$field]['name'] = substr($value, strrpos($value, '/'));
        }
        return $data;
    }


    /**
     * 
     * 
     */
    public function getDefaultImage()
    {
        return Url::to($this->_defaultImage, true);
    }


    /**
     * 
     * 
     */
    public function setDefaultImage($defaultImage)
    {
        $this->_defaultImage = Url::to($defaultImage);
        return self;
    }

    /**
     * 
     * 
     */
    public function afterFind()
    {
        parent::afterFind();

        if (is_array($this->fileAttributes()) && count($this->fileAttributes()) > 0) {

            foreach ($this->fileAttributes() as $field => $fileAttribute) {
                $this->verifyDataField($fileAttribute, $value, $modelInstance);
                $this->verifyDirectory($field);
                if (empty($value) || !file_exists(Yii::getAlias($this->getDirectory() . '/' . trim($value)))) {
                    $modelInstance->{$field} = $this->defaultImage;
                } else {
                    $modelInstance->{$field} = Url::to(
                        $this->getUrlDirectory() . $value,
                        true
                    );
                }
            }
        }
    }

    /**
     * 
     * 
     */
    public function renameDirectory()
    {
        $directoryName = $this->getDirectoryName();
        if ($this->_keyTemporalDirectory != $this->getPrimaryKey()) {
            rename(
                Url::to("{$this->basePath}/{$directoryName}/{$this->_keyTemporalDirectory}"),
                Url::to("{$this->basePath}/{$directoryName}/{$this->getPrimaryKey()}")
            );
            if (PHP_OS === 'Windows') {
                exec("rd /s /q " . Url::to("{$this->basePath}/{$directoryName}/{$this->_keyTemporalDirectory}"));
            } else {
                exec("rm -rf " . Url::to("{$this->basePath}/{$directoryName}/{$this->_keyTemporalDirectory}"));
            }
        }
    }


    /**
     * 
     * 
     */
    protected function getDirectoryName()
    {
        return Inflector::underscore(StringHelper::basename(self::class));
    }

    /**
     * Permite definir los atributos para la carga de archivos 
     *
     * @return array
     */
    public function fileAttributes()
    {
        return [
            // self::PROFILE_PHOTO => $this->member->profile_photo,
            // self::PROFILE_PHOTO_2 => $this->member->profile_photo,
        ];
    }

    /**
     * Get the value of _baseUrl
     */
    public function getBaseUrl()
    {
        return $this->_baseUrl;
    }

    /**
     * Set the value of _baseUrl
     *
     * @return  self
     */
    public function setBaseUrl($baseUrl)
    {
        $this->_baseUrl = $baseUrl;

        return $this;
    }

    /**
     * Get the value of _basePath
     */
    public function getBasePath()
    {
        return $this->_basePath;
    }

    /**
     * Set the value of _basePath
     *
     * @return  self
     */
    public function setBasePath($basePath)
    {
        $this->_basePath = $basePath;

        return $this;
    }

    /**
     * 
     */
    public function verifyDataField($data, &$value, &$modelInstance)
    {
        if (is_array($data)) {
            $value = $data['value'];
            $modelInstance = $data['model'];
        } else {
            $value = $data;
            $modelInstance = $this;
        }
    }
}
