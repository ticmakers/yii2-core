<?php

/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace ticmakers\core\validators;

use ticmakers\core\helpers\ConstantsHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\Html;
use yii\validators\CompareValidator as YiiCompareValidator;
use yii\validators\ValidationAsset;

/**
 * Validador para formularios de creación/actualización
 * 
 *
 * @package ticmakers\core\validators\CompareValidator
 *
 * @author Daniel Julian Sanchez Alvarez <daniel.sanchez@ticmakers.com>
 * @copyright Copyright (c) 2019 TIC Makers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class CompareValidator extends YiiCompareValidator
{

    private $_viewClientOptions;

    /**
     * {@inheritdoc}
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $this->_viewClientOptions = $view;
        return parent::clientValidateAttribute($model, $attribute, $view);
    }

    /**
     * {@inheritdoc}
     */
    public function getClientOptions($model, $attribute)
    {
        $options = parent::getClientOptions($model, $attribute);

        if ($this->_viewClientOptions->context->action instanceof \ticmakers\core\actions\SaveAction) {
            $compareAttribute = $this->compareAttribute === null ? $attribute . '_repeat' : $this->compareAttribute;
            $typeRender = $this->_viewClientOptions->context->action->isNewRecord ? ConstantsHelper::RENDER_CREATE : ConstantsHelper::RENDER_UPDATE;
            $sufixTypesRender = [
                'C' => '-create',
                'U' => '-update'
            ];
            $options['compareAttribute'] = Yii::$app->html::activeInputId($model, $compareAttribute, null, $sufixTypesRender[$typeRender]);
        }
        return $options;
    }
}
