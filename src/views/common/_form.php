<?php

use ticmakers\core\widgets\{Tabs, ActiveForm};
use yii\helpers\Inflector;
use ticmakers\core\helpers\ConstantsHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */
/* @var $form yii\widgets\ActiveForm */

if (!isset($modelsFormColums)) {
    $modelsFormColums = ['model' => $model];
}

$model = $modelsFormColums['model'];

$id = Inflector::slug($model::crudTitle());

$action = $this->context->action;


$redisHtml = [];

if ($action->isMultiModel) {
    foreach ($action->modelClass as $keyModel => $config) {

        if ($config['isMultiple'] && ($config['isRedis'] || $config['renderGrid']) && !$action->formInTabs) {
            unset($modelsFormColums[$keyModel]);
            $searchModel = $config['searchModel'];
            $dataProvider = $config['dataProvider'];
            $redisConfig = $config['redisConfig'];

            $redisHtml[$keyModel] = $this->render($this->context->loadViewFile('index'), [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'baseRoute' => $redisConfig['baseRoute'],
                'isRedisTab' => true,
                'isRedis' => $config['isRedis'],
                'isSortableGrid' => $config['isSortableGrid'] ?? false,
                'titleInGrid' => $config['titleInGrid'] ?? true,
                'showOnEmpty' => $config['showOnEmpty'] ?? true,
                'emptyText' => $config['emptyText'] ?? false,
            ], $redisConfig['controller']);
        }
    }
}

?>



<?php $form = ActiveForm::begin([
    'id' => 'create-update-form-' . $id,
    'options' => [
        'enctype' => 'multipart/form-data',
        'data-tabs' => $action->formInTabs ? '1' : '0',
        'data-tab-id' => "#{$id}-tabs"
    ]
]); ?>


<?php

if (!$action->formInTabs) {
    $formColumnsHtml = Yii::$app->ui->renderFormColumns($form, $modelsFormColums, $this->context, '_form_tpl', $model->isNewRecord ? ConstantsHelper::RENDER_CREATE : ConstantsHelper::RENDER_UPDATE);
    foreach ($redisHtml as $keyModel => $html) :
        $formColumnsHtml = str_replace('{{-grid-' . $keyModel . '-grid-}}', $html, $formColumnsHtml);
    endforeach;
    echo $formColumnsHtml;
}
?>

<?php ActiveForm::end() ?>

<?php if ($action->formInTabs) {

    if ($action->tabsConfig && !empty($action->tabsConfig)) {

        $itemsTab = [];
        foreach ($action->tabsConfig as $keyTab => $configTab) {

            if (isset($configTab['modelsRender'])) {
                $modelsFormColumnsTab = [];
                foreach ($configTab['modelsRender'] ?? [] as $keyModelTab) {
                    $modelsFormColumnsTab[$keyModelTab] = $modelsFormColums[$keyModelTab];
                }
            } else {
                $modelsFormColumnsTab = $modelsFormColums;
            }

            $paramsRenderTab = [
                'viewNameTpl' => $configTab['viewNameTpl'] ?? '_form_tpl',
                'hasPrevious' => $configTab['hasPrevious'] ?? false,
                'hasNext' => $configTab['hasNext'] ?? false,
                'form' => $form,
                'modelsFormColums' => $modelsFormColumnsTab,
                'showOnEmpty' => $configTab['showOnEmpty'] ?? true,
                'emptyText' => $configTab['emptyText'] ?? false,
                'modelKey' => $configTab['modelKey'] ?? false,
                'isSortableGrid' => $configTab['isSortableGrid'] ?? false,
                'titleInGrid' => $configTab['titleInGrid'] ?? true,
            ];

            if (isset($configTab['saveAndContinue']) && $configTab['saveAndContinue'] && $action->isNewRecord) {
                $paramsRenderTab['hasNext'] = false;
                $paramsRenderTab['saveAndContinue'] = true;
            }

            $itemsTab[$keyTab] = [
                'label'   => Yii::t('app', $configTab['label'] ?? 'Information'),
                'content' => $this->render($this->context->loadViewFile($configTab['viewName'] ?? '_tab'), $paramsRenderTab, Yii::$app->controller),
            ];

            if (isset($configTab['disabledOnCreate']) && $configTab['disabledOnCreate'] && $action->isNewRecord) {

                $itemsTab[$keyTab]['linkOptions']  = ['class' => 'disabled'];
            }
        }
        /*
        [
            [
                'label'   => Yii::t('app', 'Information'),
                'content' => $this->render($this->context->loadViewFile('_tab'), [
                    'viewNameTpl' => '_information_tpl',
                    'hasPrevious' => false,
                    'hasNext' => !$modelsFormColums['model']->isNewRecord,
                    'form' => $form,
                    'modelsFormColums' => [
                        'model' => $modelsFormColums['model'],
                        'content' => $modelsFormColums['content'],
                    ]
                ], Yii::$app->controller),
                'active'  => true,
            ],
            [
                'label'   => Yii::t('app', 'Members'),
                'content' => $this->render($this->context->loadViewFile('_tab'), [
                    'hasPrevious' => true,
                    'hasNext' => false,
                    'form' => $form,
                    'modelKey' => 'members',
                    'modelsFormColums' => $modelsFormColums,
                    // 'showOnEmpty' => true,
                    // 'emptyText' => Yii::t('app', 'People have not yet been associated with the entity. When you add a member to the entity, you can view your information here')
                ]),
            ],
        ]
    */
    } else {

        $itemsTab = [
            'model' => [
                'label'   => Yii::t('app', 'Information'),
                'content' => $this->render($this->context->loadViewFile('_tab'), [
                    // 'viewNameTpl' => '_information_tpl',
                    'hasPrevious' => false,
                    'hasNext' => !$modelsFormColums['model']->isNewRecord || $action->isRedis,
                    'form' => $form,
                    'modelsFormColums' => $modelsFormColums,
                ], Yii::$app->controller),
                'active'  => true,
            ]
        ];



        if ($action->isMultiModel) {
            $renderTabs = [];
            foreach ($action->modelClass as $keyModel => $config) {
                if ($config['isMultiple'] && ($config['isRedis'] || $config['renderGrid'])) {
                    $renderTabs[] = $keyModel;
                }
            }

            foreach ($renderTabs as $keyTab => $keyModel) {

                $hasNext = isset($renderTabs[$keyTab + 1]);
                $searchModel = $action->modelClass[$keyModel]['searchModel'];
                $itemsTab[$keyModel] = [
                    'label'   => Yii::t('app', $searchModel::crudTitle() ?? $keyModel),
                    'content' => $this->render($this->context->loadViewFile('_tab'), [
                        'hasPrevious' => true,
                        'hasNext' => $hasNext,
                        'form' => $form,
                        'modelKey' => $keyModel,
                        'isSortableGrid' => $action->modelClass[$keyModel]['isSortableGrid'] ?? false,
                        'titleInGrid' => $action->modelClass[$keyModel]['titleInGrid'] ?? true,
                        // 'showOnEmpty' => true,
                        // 'emptyText' => Yii::t('app', 'People have not yet been associated with the entity. When you add a member to the entity, you can view your information here')
                    ]),
                ];
            }
        }
    }

    $tabsHtml = Tabs::widget(['id' => "{$id}-tabs", 'items' => array_values($itemsTab)]);

    echo $tabsHtml;
} ?>

<?php if (!($action->isModal ?? false) && !$action->formInTabs) : ?>
    <div class="row">
        <div class="col-12 col-md-12 text-right">
            <?= Yii::$app->ui::btnCancel(); ?>
            <?= Yii::$app->ui::btnSend(null, ['form' => $form->id]); ?>
        </div>
    </div>
<?php endif; ?>

<?php $form->registerClientScript() ?>