<div class="row">
    <?= Yii::$app->ui::buildDefaultRenderTpl($formColumns, $model, $model->isNewRecord ? 'C' : 'U');    ?>
</div>
<?php
$action = $this->context->action;

if ($action->isMultiModel && !$action->formInTabs) :
    foreach ($action->modelClass as $keyModel => $config) :
        if ($config['isRedis'] || $config['renderGrid']) :
            ?>
            <div class="<?= $keyModel ?>-redis-grid row">
                <div class="col-12">
                    <hr class="mt-30">
                    <h5 class="card-title"><?= $config['searchModel']::crudTitle() ?? '' ?></h5>
                    {{-grid-<?= $keyModel ?>-grid-}}

                </div>
            </div>

<?php
        endif;
    endforeach;

endif;

?>