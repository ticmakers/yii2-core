<?php

use ticmakers\core\widgets\ActiveForm;
use ticmakers\core\helpers\ConstantsHelper as C;

/* @var $this yii\web\View */
/* @var $model app\models\search\Categories */
/* @var $form sticmakers\core\widgets\ActiveForm */
?>

<div class="<?= $id ?>-search">
    <?php $form = ActiveForm::begin([
        // 'action' => ['index'],
        'method' => 'get',
        'id' => 'search-form-' . $id,
        'options' => [
            'class' => 'search-redis',
            'data-gridview' => $id
        ]
    ]);
    echo Yii::$app->ui
        ->renderFormColumns($form, $model, $this->context, '_search_tpl', C::RENDER_SEARCH);
    ActiveForm::end();
    ?>
</div>