<?php

use ticmakers\core\widgets\ActiveForm;
use ticmakers\core\helpers\ConstantsHelper as C;
use yii\bootstrap4\Html;
use yii\widgets\Pjax;

use kartik\select2\Select2Asset;

Select2Asset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\search\Categories */
/* @var $form ticmakers\core\widgets\ActiveForm */
?>

<div class="<?= $id ?>-search-top">
    <?php
    $form = ActiveForm::begin([
        // 'action' => Yii::$app->request->absoluteUrl,
        'method' => 'get',
        'id' => 'search-top-form-' . $id,
        'options' => [
            'class' => 'search-redis',
            'data-gridview' => $id
        ]
    ]);

    ?>

    <div class="d-flex justify-content-between align-items-center">
        <?= Yii::$app->ui
            ->renderFormColumns($form, $model, $this->context, '_search_top_tpl', C::RENDER_SEARCH_TOP) ?>
    </div>



    <?php
    ActiveForm::end();
    ?>
</div>