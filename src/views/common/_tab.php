<?php

use ticmakers\core\helpers\ConstantsHelper;

$viewNameTpl = $viewNameTpl ?? '_form_tpl';
$showOnEmpty = $showOnEmpty ?? true;
$emptyText = $emptyText ?? false;
$isSortableGrid = $isSortableGrid ?? false;
$titleInGrid = $titleInGrid ?? true;
$saveAndContinue = $saveAndContinue ?? false;

$hasNext = $hasNext ?? false;
$hasPrevious = $hasPrevious ?? false;

if (is_array($this->context->action->modelClass) && isset($modelKey) && $modelKey) {

    $searchModel = $this->context->action->modelClass[$modelKey]['searchModel'];
    $dataProvider = $this->context->action->modelClass[$modelKey]['dataProvider'];
    $redisConfig = $this->context->action->modelClass[$modelKey]['redisConfig'];


    echo $this->render($this->context->loadViewFile('index'), [
        'searchModel' => $searchModel,
        'dataProvider' => $dataProvider,
        'baseRoute' => $redisConfig['baseRoute'],
        'isRedisTab' => true,
        'isSortableGrid' => $isSortableGrid,
        'titleInGrid' => $titleInGrid,
        'showOnEmpty' => $showOnEmpty,
        'emptyText' => $emptyText,
    ], $redisConfig['controller']);
} else {
    echo Yii::$app->ui->renderFormColumns($form, $modelsFormColums, $this->context,  $viewNameTpl, $this->context->action->isNewRecord ? ConstantsHelper::RENDER_CREATE : ConstantsHelper::RENDER_UPDATE);
}
?>


<?php if (!($this->context->action->isModal ?? false)) : ?>

    <div class="form-group float-right mt-20">
        <?= Yii::$app->ui::btnCancel(); ?>

        <?php if ($hasPrevious) {
                echo Yii::$app->html::button(
                    '<i class="fa fa-arrow-left"></i> ' . Yii::t('app', 'Previous'),
                    ['class' => 'btn btn-outline-primary', 'onclick' => "directionTab(null, 'L')"]
                );
            } ?>
        <?php if ($hasNext) {
                echo Yii::$app->html::button(
                    '<i class="fa fa-arrow-right"></i> ' . Yii::t('app', 'Next'),
                    ['class' => 'btn btn-outline-primary', 'onclick' => "directionTab()"]
                );
            } ?>

        <?php if ($saveAndContinue &&  $this->context->action->isNewRecord) : ?>
            <?= Yii::$app->ui::btnSaveAndContinue(null, ['form' => $form->id]); ?>
        <?php else : ?>
            <?= Yii::$app->ui::btnSend(null, ['form' => $form->id]); ?>
        <?php endif; ?>
    </div>
<?php endif; ?>


<?php
$script = <<<JS
    function directionTab(tabId, direction){
        if(!tabId){ tabId = '.nav-tabs' }
        let item = $(tabId).find('a.active').parent()
        item = (direction && direction == 'L') ? item.prev() : item.next()
        item.find('a').click();
    }

JS;
$this->registerJs($script, yii\web\View::POS_BEGIN);
?>