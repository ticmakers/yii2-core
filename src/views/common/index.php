<?php

use kartik\dynagrid\DynaGrid;
use yii\bootstrap4\Modal;
use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\Categories */
/* @var $dataProvider yii\data\ActiveDataProvider */

$isSortableGrid = $isSortableGrid ?? false;
$isRedisTab = $isRedisTab ?? false;
$isRedis = $isRedis ?? false;
$baseRoute = $baseRoute ?? '';
$titleInGrid = $titleInGrid ?? true;
$displayPanel = $displayPanel ?? true;
$moduleBase = (Yii::$app->controller->module->id === Yii::$app->id) ? '' : ("/" . Yii::$app->controller->module->id);

$filterUrl = [
    $moduleBase . "/" . Yii::$app->controller->id . "/" . Yii::$app->controller->action->id
];
foreach (Yii::$app->request->getQueryParams() ?? [] as $key => $value) {
    $filterUrl[$key] = $value;
}


$id = Inflector::slug($searchModel::crudTitle());
$controllerBase = $isRedisTab ? $this->context : Yii::$app->controller;
$title = Yii::t('app', $searchModel::crudTitle());

if (!$isRedisTab) {
    $this->title = $title;
    $this->params['breadcrumbs'][] = $this->title;
}

$searchTop = $this->render($this->context->loadViewFile('_search_top'), [
    'model' => $searchModel,
    'id' => $id,
    'controller' => $baseRoute
], $controllerBase);

if ($controllerBase->isModal) {

    $baseRouteCreate = [$baseRoute . 'create'];

    if ($isRedis) {
        $baseRouteCreate['t'] = Yii::$app->request->get('t');
    } else {
        foreach ($searchModel->attributes as $attriute => $value) {
            if (!empty($value)) {
                $baseRouteCreate[$attriute] = $value;
            }
        }
    }

    $btnNew = Yii::$app->ui::btnNew(
        ucfirst((Yii::t('app', Inflector::singularize($searchModel::crudTitle())))),
        $baseRouteCreate,
        [
            'onclick' =>
            "openModalGrid(this, '" . $id . "', 'create'); return false;"
        ]
    );
} else {
    $btnNew = Yii::$app->ui::btnNew(ucfirst((Yii::t('app', Inflector::singularize($searchModel::crudTitle())))));
}

$btnSearch = Yii::$app->ui::btnSearch(
    Yii::t('app', 'Advanced'),
    '#search-modal-' . $id
);
// $btnRefresh = Yii::$app->ui::btnRefresh();

$toolbarHtml = $this->render($this->context->loadViewFile('_toolbar_tpl'), [
    'model' => $searchModel,
    'id' => $id,
    'controller' => $baseRoute
], $controllerBase);

$replacesToolbar = [
    '{{form-search-top}}' => $searchTop,
    '{{btn-new}}' => $btnNew,
    '{{btn-advanced-search}}' => $btnSearch,
    // '{{btn-refresh}}' => $btnRefresh,
];

$toolbarHtml = str_replace(array_keys($replacesToolbar), array_values($replacesToolbar), $toolbarHtml);

?>

<?php
$gridConfig = Yii::$app->ui::dynagridConfig($searchModel::crudTitle(), [
    'columns' => $searchModel->gridColumns(),
    'enableMultiSort' => !$isRedisTab,
    'gridOptions' => [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'filterUrl' => $filterUrl,
        'filterSelector' => '.header-filter',
        // 'pjaxSettings' => [
        //     'options' => [
        //         'options' => [
        //             'data-aditional-pjax-reload' => '#test'
        //         ]
        //     ]
        // ],
        'pjax' => true,
        'layout' => "{toolbar}\n{items}\n<div class=\"d-flex justify-content-between align-items-center mb-10\">{pager}{summary}</div>",
    ]
]);

if (!$titleInGrid) {
    $gridConfig['gridOptions']['panelHeadingTemplate'] = '';
}

if ($isRedisTab && !$displayPanel) {
    $gridConfig['gridOptions']['panel'] = false;
} else {
    $gridConfig['gridOptions']['layout'] = "{items}\n<div class=\"d-flex justify-content-between align-items-center mb-10\">{pager}{summary}</div>";
    $gridConfig['gridOptions']['panelBeforeTemplate'] = "{toolbar}";
    $gridConfig['gridOptions']['panelAfterTemplate'] = false;

    $gridConfig['gridOptions']['panel']['beforeOptions']['class'] = $titleInGrid ? 'mt-20' : '';
}

$gridConfig['gridOptions']['toolbar'] = [
    [
        'content' => $toolbarHtml,
        'options' => ['class' => 'row']
    ],
];

if (isset($showOnEmpty)) {
    $gridConfig['gridOptions']['showOnEmpty'] = $showOnEmpty;
}
if (isset($emptyText) && $emptyText) {
    $gridConfig['gridOptions']['emptyText'] = Yii::t('app', $emptyText);
}


?>


<?php if ($isSortableGrid) :

    $gridConfig['sortUrl'] = \yii\helpers\Url::to(['sortItem']);
    $widgetDataHtml = \ticmakers\sortablegridview\SortableGridView::widget($gridConfig);

else :

    $widgetDataHtml = DynaGrid::widget($gridConfig);

endif ?>


<?php



$indexHtml = $this->render($this->context->loadViewFile('index_tpl'), [
    'model' => $searchModel,
    'id' => $id,
    'controller' => $baseRoute,
    'dataProvider' => $dataProvider
], $controllerBase);

$replacesIndex = [
    '{{form-search-top}}' => $searchTop,
    '{{btn-new}}' => $btnNew,
    '{{btn-advanced-search}}' => $btnSearch,
    '{{widget-data}}' => $widgetDataHtml,
    '{{toolbar}}' => $toolbarHtml,
];

$indexHtml = str_replace(array_keys($replacesIndex), array_values($replacesIndex), $indexHtml);

unset($searchTop, $btnNew, $btnSearch, $widgetDataHtml, $toolbarHtml);

?>

<?= $indexHtml ?>

<?php Modal::begin([
    'id' => 'search-modal-' . $id,
    'title' => Yii::t('app', 'Advanced search'),
    'size' => Modal::SIZE_LARGE,
    'footer' =>
    Yii::$app->ui::btnCloseModal(Yii::t('app', 'Cancel'), Yii::$app->html::ICON_REMOVE) .
        Yii::$app->ui::btnSend(Yii::t('app', 'Search'), [
            'form' => 'search-form-' . $id
        ])
]);
?>


<?php //Pjax::begin(['id' => 'test']); 
?>

<?= $this->render($this->context->loadViewFile('_search'), [
    'model' => $searchModel,
    'id' => $id
], $controllerBase) ?>

<?php //Pjax::end(); 
?>

<?php Modal::end(); ?>
<?= Modal::widget([
    'id' => $id . '-modal',
    'title' => '',
    'size' => Modal::SIZE_EXTRA_LARGE,
    'footer' =>
    Yii::$app->ui::btnCloseModalMessage(Yii::t('app', 'Cancel'), Yii::$app->html::ICON_REMOVE) . Yii::$app->ui::btnSaveBack(null, ['form' => 'create-update-form-' . $id, 'class' => 'btn btn-info']) . Yii::$app->ui::btnSend(Yii::t('app', 'Save'),  ['form' => 'create-update-form-' . $id]),
    'closeButton' => ['class' => 'close confirm-close'],
    'options'       => [
        'style' => 'display: none;',
        'tabindex' => false,
    ],
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => false]
]); ?>