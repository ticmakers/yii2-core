<?php

use yii\helpers\Inflector;

/* @var $this yii\web\View */
/* @var $model app\models\Categories */

if (!isset($modelsFormColums)) {
    $modelsFormColums = ['model' => $model];
}

$model = $modelsFormColums['model'];

$this->title = Yii::t('app', 'Update {title}: {name}', [
    'title' => strtolower(Yii::t('app', Inflector::singularize($model::crudTitle()))),
    'name' => $model->{$model::getNameFromRelations()},
]);
$this->params['breadcrumbs'][] = ['label' => $model::crudTitle(), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
$id = Inflector::slug($model::crudTitle());

$params = [
    'model' => $model,
    'id' => $id
];

if (isset($modelsFormColums)) {
    $params['modelsFormColums'] = $modelsFormColums;
}

if (isset($paramsName)) {
    foreach ($paramsName as $paramName) {
        if (!isset($params[$paramName])) {
            $params[$paramName] = ${$paramName} ?? null;
        }
    }
}

$formHtml = Yii::$app
    ->getView()
    ->render(
        $this->context->loadViewFile('_form'),
        $params,
        $this->context
    );
$templateHtml = Yii::$app
    ->getView()
    ->render(
        $this->context->loadViewFile('update_tpl'),
        $params,
        $this->context
    );

$replacesHtml = [
    '{{form}}' => $formHtml,
    '{{title}}' => $this->title,
];

$templateHtml = str_replace(array_keys($replacesHtml), array_values($replacesHtml), $templateHtml);

echo $templateHtml;
