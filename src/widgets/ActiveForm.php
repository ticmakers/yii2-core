<?php

namespace ticmakers\core\widgets;

use Yii;
use yii\bootstrap4\ActiveForm as BaseActiveForm;

/**
 * ActiveFom es un widget que construye un formulario HTML interactivo con uno o varios modelos.
 *
 * @package ticmakers
 * @subpackage widgets
 * @category Widgets
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @author Kevin Daniel Guzmán Delgadillo <kevindanielguzmen98@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class ActiveForm extends BaseActiveForm
{

    public function beforeRun()
    {
        if (empty($this->action)) {
            $this->action = [
                Yii::$app->controller->id . "/" . Yii::$app->controller->action->id
            ];
            foreach (Yii::$app->request->getQueryParams() ?? [] as $key => $value) {
                $this->action[$key] = $value;
            }
        }
        return parent::beforeRun();
    }
    /**
     * @var string Using different style themes:
     *
     * + default: Default layout with labels at the top of the fields.
     * + horizontal: Horizontal layout set the labels left to the fields.
     */
    public $layout = 'default';

    /**
     * @var string The error Summary alert class
     */
    public $errorSummaryCssClass = 'error-summary alert alert-danger';

    /**
     * @inheritdoc
     */
    public $errorCssClass = 'is-invalid';

    /**
     * @var string Validation type
     */
    public $validationStateOn = BaseActiveForm::VALIDATION_STATE_ON_INPUT;

    /**
     * @inheritdoc
     */
    public $successCssClass = 'is-valid';

    /**
     * @inheritdoc
     */
    public $fieldClass = \ticmakers\core\widgets\ActiveField::class;

    /**
     * Runs the widget.
     * This registers the necessary JavaScript code and renders the form open and close tags.
     * @throws InvalidCallException if `beginField()` and `endField()` calls are not matching.
     */
    public function run()
    {
        if (!empty($this->_fields)) {
            throw new InvalidCallException('Each beginField() should have a matching endField() call.');
        }

        $content = ob_get_clean();
        $html = Yii::$app->html::beginForm($this->action, $this->method, $this->options);
        $html .= $content;

        if ((!isset($this->options['data-tabs']) || $this->options['data-tabs'] == '0') && $this->enableClientScript) {
            $this->registerClientScript();
        }

        $html .= Yii::$app->html::endForm();
        return $html;
    }
}
