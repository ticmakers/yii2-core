<?php

namespace ticmakers\core\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Menu displays a multi-level menu using nested HTML lists.
 *
 * The main property of Menu is [[items]], which specifies the possible items in the menu.
 * A menu item can contain sub-items which specify the sub-menu under that menu item.
 *
 * Menu checks the current route and request parameters to toggle certain menu items
 * with active state.
 *
 * Note that Menu only renders the HTML tags about the menu. It does do any styling.
 * You are responsible to provide CSS styles to make it look like a real menu.
 *
 * The following example shows how to use Menu:
 *
 * ```php
 * echo Menu::widget([
 *     'items' => [
 *         // Important: you need to specify url as 'controller/action',
 *         // not just as 'controller' even if default action is used.
 *         ['label' => 'Home', 'url' => ['site/index']],
 *         // 'Products' menu item will be selected as long as the route is 'product/index'
 *         ['label' => 'Products', 'url' => ['product/index'], 'items' => [
 *             ['label' => 'New Arrivals', 'url' => ['product/index', 'tag' => 'new']],
 *             ['label' => 'Most Popular', 'url' => ['product/index', 'tag' => 'popular']],
 *         ]],
 *         ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
 *     ],
 * ]);
 * ```
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Menu extends \yii\widgets\Menu
{
    /**
     * @var bool whether the navbar content should be included in an inner div container.
     */
    public $renderInnerContainer = true;

    /**
     * Renders the menu.
     */
    public function run()
    {
        if ($this->route === null && Yii::$app->controller !== null) {
            $this->route = Yii::$app->controller->getRoute();
        }
        if ($this->params === null) {
            $this->params = Yii::$app->request->getQueryParams();
        }
        $items = $this->normalizeItems($this->items, $hasActiveChild);
        if (!empty($items)) {
            $options = $this->options;
            $tag = ArrayHelper::remove($options, 'tag', 'ul');

            if ($this->renderInnerContainer) {
                echo Yii::$app->html::tag('div', Yii::$app->html::tag($tag, $this->renderItems($items), $options), ['class' => ' col-3']);
            } else {
                echo Yii::$app->html::tag($tag, $this->renderItems($items), $options);
            }
        }
    }
    /**
     * Creates a widget instance and runs it.
     * The widget rendering result is returned by this method.
     * @param array $config name-value pairs that will be used to initialize the object properties
     * @return string the rendering result of the widget.
     * @throws \Exception
     */
    public static function widget($config = [])
    {

        if (!isset($config['options']['class'])) {
            $config['options']['class'] = 'nav nav-pills flex-column border border-primary rounded p-1';
        }

        if (!isset($config['itemOptions']['class'])) {
            $config['itemOptions']['class'] = 'nav-item';
        }

        if (!isset($config['encodeLabels'])) {
            $config['encodeLabels'] = false;
        }
        if (!isset($config['activeCssClass'])) {
            $config['activeCssClass'] = 'active';
        }
        if (!isset($config['linkTemplate'])) {
            $config['linkTemplate'] = '<a data-method="post" class="nav-link {active}" href="{url}" {data-toggle} {data-target-submenu}><em class="fa {icon}"></em><span> {label} {arrow} </span></a>';
        }
        if (!isset($config['submenuTemplate'])) {
            $config['submenuTemplate'] = '<a class="nav-link collapsed" {data-toggle} {data-target-submenu} aria-expanded="false">
    <em class="fa {icon}"></em><span> {label} {arrow} </span>
     </a>
     <div id="{id-submenu}" class="collapse "><ul class="nav flex-column ml-3">{items}</ul></div>';
        }
        if (!isset($config['activateParents'])) {
            $config['activateParents'] = true;
        }

        return parent::widget($config);
    }

    public $principalLinkItem;

    /**
     * Recursively renders the menu items (without the container tag).
     * @param array $items the menu items to be rendered recursively
     * @return string the rendering result
     */
    protected function renderItems($items, $sub = false)
    {
        $n = count($items);
        $lines = [];
        foreach ($items as $i => $item) {

            $options = array_merge(
                $this->itemOptions,
                ArrayHelper::getValue($item, 'options', [])
            );

            if (!empty($item['items'])) {
                $item['label'] = $item['label'];
            }

            if (!isset($item['url'])) {
                $options['class'] = 'site-menu-category open';
            }

            $tag = ArrayHelper::remove($options, 'tag', 'li');
            $class = [];

            if ($item['active']) {
                $class[] = 'open';
                $class[] = $this->activeCssClass;
            }
            if ($i === 0 && $this->firstItemCssClass !== null) {
                $class[] = $this->firstItemCssClass;
            }
            if ($i === $n - 1 && $this->lastItemCssClass !== null) {
                $class[] = $this->lastItemCssClass;
            }
            if (!empty($class)) {
                if (empty($options['class'])) {
                    $options['class'] = implode(' ', $class);
                } else {
                    $options['class'] .= ' ' . implode(' ', $class);
                }
            }

            if (!empty($item['items'])) {
                $idSubMenu = Yii::$app->security->generateRandomString();
                $menu = $this->renderItem($item, $idSubMenu, $sub);
            } else {
                $menu = $this->renderItem($item, '', $sub);
            }

            if (!empty($item['items'])) {

                $submenuTemplate = ArrayHelper::getValue(
                    $item,
                    'submenuTemplate',
                    $this->submenuTemplate
                );
                $menu .= strtr(
                    $submenuTemplate,
                    [
                        '{id-submenu}' => $idSubMenu,
                        '{items}' => $this->renderItems($item['items'], true),
                        '{data-target-submenu}' => "href=\"#{$idSubMenu}\"",
                        '{data-toggle}' => !empty($idSubMenu) ? 'data-toggle="collapse"' : '',
                        '{url}' => Yii::$app->html::encode(Url::to($item['url'])),
                        '{label}' => $item['label'],
                        '{active}' => $class,
                        '{icon}' => isset($item['icon']) ? $item['icon'] : (isset($item['items']) ? '' : ''),
                        '{arrow}' => isset($item['items']) ? '<em class="fa fa-caret-down "></em>' : '',
                        '{show}' => $item['active'] ? 'show' : ''
                    ]
                );
            }
            $lines[] = Yii::$app->html::tag($tag, $menu, $options);
        }

        return implode("\n", $lines);
    }

    /**
     * Renders the content of a menu item.
     * Note that the container and the sub-menus are not rendered here.
     * @param array $item the menu item to be rendered. Please refer to [[items]] to see what data might be in the item.
     * @return string the rendering result
     */
    protected function renderItem($item, $idSubMenu = '', $sub = false)
    {

        if (isset($item['url'])) {
            if ($item['url'] == 'javascript:;' || $item['url'] == '') {
                $template = ArrayHelper::getValue(
                    $item,
                    'template',
                    $this->principalLinkItem
                );
            } else {
                $template = ArrayHelper::getValue(
                    $item,
                    'template',
                    $this->linkTemplate
                );
            }

            if ($sub) {
                $template = '<a class="nav-link {active}" href="{url}" {data-toggle} {data-target-submenu}><i class="material-icons">{icon}</i><span class="sidebar-mini"> {prefix} </span>
                        <span class="sidebar-normal"> {label} </span> </a>';
            }

            $class = 'collapsed';
            if ($item['active']) {
                $class = $this->activeCssClass;
            }

            return strtr(
                $template,
                [
                    '{data-target-submenu}' => "href=\"#{$idSubMenu}\"",
                    '{data-toggle}' => !empty($idSubMenu) ? 'data-toggle="collapse"' : '',
                    '{url}' => Yii::$app->html::encode(Url::to($item['url'])),
                    '{label}' => $item['label'],
                    '{prefix}' => $item['prefix'] ?? '',
                    '{active}' => $class,
                    '{icon}' => isset($item['icon']) ? $item['icon'] : (isset($item['items']) ? '' : ''),
                    '{arrow}' => isset($item['items']) ? '<em class="fa fa-caret-down "></em>' : '',
                ]
            );
        } else {
            $template = ArrayHelper::getValue(
                $item,
                'template',
                $this->labelTemplate
            );

            return strtr(
                $template,
                [
                    '{label}' => $item['label'],
                ]
            );
        }
    }
}
