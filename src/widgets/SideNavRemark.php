<?php

namespace ticmakers\core\widgets;

use Yii;
use ticmakers\core\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\base\InvalidConfigException;

/**
 * Este widget extiende yii\bootstrap4\Nav para que se adapte a la plantilla
 *
 * En el parametro "icon" usar iconos de FontAwesome
 * Se puede usar el parametro "activeWhitController" para que este activo con
 * cualquier acción que pertenezca al controlador de la ruta del item (por defecto es "true")
 * @package ticmakers
 * @subpackage widgets
 * @category Widgets
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class SideNavRemark extends \yii\bootstrap4\Nav
{

    /**
     * Inicializa el widget
     */
    public function init()
    {
        parent::init();

        Yii::$app->html::removeCssClass($this->options, 'nav');
        Yii::$app->html::addCssClass($this->options, 'site-menu');
        $this->options['data-plugin'] = 'menu';
    }

    /**
     * Renders widget items.
     */
    public function renderItems()
    {
        $items = parent::renderItems();

        return '
        <div class="site-menubar site-menubar-light">
            <div class="site-menubar-body">
                <div>
                    <div>' . $items . '</div>
                </div>
            </div>
        </div>';
    }

    /**
     * Renders un item del widget
     * @param string|array $item el item que va a render.
     * @param boolean $sub Indica si es un subItem o no
     * @return string el resultado del render.
     * @throws InvalidConfigException
     */
    public function renderItem($item, $sub = false)
    {
        if (is_string($item))
        {
            return Yii::$app->html::tag(
                            'li', $item, ['class' => 'site-menu-category']
            );
        }
        if (!isset($item['label']))
        {
            throw new InvalidConfigException("La opción 'label' es obligatoria.");
        }
        $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
        $icon        = ArrayHelper::getValue($item, 'icon');
        $label       = $encodeLabel ? Yii::$app->html::encode($item['label']) : $item['label'];
        $label       = Yii::$app->html::tag('i', '',
                                 ['class' => "site-menu-icon fa-{$icon}"]) . Yii::$app->html::tag('span',
                                                                                       $label,
                                                                                       [
                    'class' => $sub ? 'site-menu-category' : 'site-menu-title']);
        $options     = ArrayHelper::getValue($item, 'options', []);
        $items       = ArrayHelper::getValue($item, 'items');
        $url         = ArrayHelper::getValue($item, 'url', 'javascript:;');
        $linkOptions = ArrayHelper::getValue($item, 'linkOptions', []);
        $visible     = ArrayHelper::getValue($item, 'visible', true);

        if (!$visible)
        {
            return '';
        }

        if (isset($item['url']))
        {
            Yii::$app->html::addCssClass($options, 'site-menu-item');
        }
        else
        {
            Yii::$app->html::addCssClass($options, 'site-menu-category open');
        }

        if (isset($item['active']))
        {
            $active = ArrayHelper::remove($item, 'active', false);
        }
        else
        {
            $active = $this->isItemActive($item);
        }

        if ($items !== null)
        {
            Yii::$app->html::addCssClass($options, ['widget' => 'has-sub']);
            $label .= Yii::$app->html::tag('span', '', ['class' => 'site-menu-arrow']);

            if (is_array($items))
            {
                if ($this->activateItems)
                {
                    $items = $this->isChildActive($items, $active);
                }

                if ($this->hasChildActive($items))
                {
                    Yii::$app->html::addCssClass($options, 'open');
                    Yii::$app->html::addCssClass($options, 'active');
                }

                $items = $this->renderTreeView($items, $item);
            }
        }

        if ($this->activateItems && $active)
        {
            Yii::$app->html::addCssClass($options, 'open');
            Yii::$app->html::addCssClass($options, 'active');
            Yii::$app->html::addCssClass($linkOptions, 'active');
        }

        return Yii::$app->html::tag(
                        'li', Yii::$app->html::a($label, $url, $linkOptions) . $items,
                                      $options
        );
    }

    /**
     * Sobreescribe método para activarlo tambien cuando
     * este dentro de cualquier accion del controlador
     * @param array $item
     */
    protected function isItemActive($item)
    {
        $activeWhitController = ArrayHelper::getValue(
                        $item, 'activeWhitController', true
        );
        $active               = parent::isItemActive($item);

        if ($activeWhitController || $active)
        {
            $route = explode('/', $item['url'][0]);
            if (isset($route[1]))
            {
                $active = $active || $route[1] === Yii::$app->controller->id;
            }
        }

        return $active;
    }

    /**
     * Indica si algun item hijo esta activo
     * @param array $items Items Hijos
     * @return boolean
     */
    protected function hasChildActive($items)
    {
        foreach ($items as $item)
        {
            if ($this->isItemActive($item))
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Renderiza los items para sub-menus
     * @param array $items items dados
     * @param array $parentItem item Padre
     * @return string Resultado Html
     */
    protected function renderTreeView($items, $parentItem)
    {
        Yii::$app->html::addCssClass($parentItem['options'], 'site-menu-sub');
        $content = '';

        foreach ($items as $item)
        {
            $content .= $this->renderItem($item, true);
        }

        return Yii::$app->html::tag('ul', $content, $parentItem['options']);
    }

}
