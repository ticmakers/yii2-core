<?php

namespace ticmakers\core\widgets;

use ticmakers\core\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * Este widget extiende yii\bootstrap4\Tabs para que se adapte a la plantilla
 *
 * @package ticmakers
 * @subpackage widgets
 * @category Widgets
 *
 * @author Juan David Rodriguez Ramirez <jdrodrigez429@gmail.com>
 * @copyright Copyright (c) 2018 TicMakers S.A.S.
 * @version 0.0.1
 * @since 1.0.0
 */
class TabsVertical extends Tabs
{

    public function init()
    {
        parent::init();

        $this->navType .= 'flex-column';
        Yii::$app->html::addCssClass($this->tabContentOptions, 'col-9');
        Yii::$app->html::removeCssClass($this->tabContentOptions, 'pt-20');
    }

    /**
     * Renderiza los items del tab especificado en [[items]].
     *
     * @return string Html con el restultado.
     * @throws InvalidConfigException.
     */
    protected function renderItems()
    {
        $headers = [];
        $panes = [];

        if (!$this->hasActiveTab()) {
            $this->activateFirstVisibleTab();
        }

        foreach ($this->items as $n => $item) {
            if (!ArrayHelper::remove($item, 'visible', true)) {
                continue;
            }
            if (!array_key_exists('label', $item)) {
                throw new InvalidConfigException("The 'label' option is required.");
            }
            $encodeLabel = isset($item['encode']) ? $item['encode'] : $this->encodeLabels;
            $label = $encodeLabel ? Yii::$app->html::encode($item['label']) : $item['label'];
            $headerOptions = array_merge(
                $this->headerOptions,
                ArrayHelper::getValue(
                    $item,
                    'headerOptions',
                    []
                )
            );
            $linkOptions = array_merge(
                $this->linkOptions,
                ArrayHelper::getValue(
                    $item,
                    'linkOptions',
                    []
                )
            );

            // Ajustes para el template------------------------
            Yii::$app->html::addCssClass($headerOptions, 'nav-item');
            $headerOptions['role'] = 'presentation';
            Yii::$app->html::addCssClass($linkOptions, 'nav-link');
            //-------------------------------------------

            if (isset($item['items'])) {
                $label .= ' <b class="caret"></b>';
                Yii::$app->html::addCssClass($headerOptions, ['widget' => 'dropdown']);

                if ($this->renderDropdown($n, $item['items'], $panes)) {
                    Yii::$app->html::addCssClass($headerOptions, 'active');
                }

                Yii::$app->html::addCssClass($linkOptions, ['widget' => 'dropdown-toggle']);
                if (!isset($linkOptions['data-toggle'])) {
                    $linkOptions['data-toggle'] = 'dropdown';
                }
                /** @var Widget $dropdownClass */
                $dropdownClass = $this->dropdownClass;
                $header = Yii::$app->html::a($label, "#", $linkOptions) . "\n"
                    . $dropdownClass::widget([
                    'items' => $item['items'],
                    'clientOptions' => false,
                    'view' => $this->getView()
                ]);
            } else {
                $options = array_merge(
                    $this->itemOptions,
                    ArrayHelper::getValue(
                        $item,
                        'options',
                        []
                    )
                );
                $options['id'] = ArrayHelper::getValue(
                    $options,
                    'id',
                    $this->options['id'] . '-tab' . $n
                );
                $options['role'] = 'tabpanel'; //------Ajustes para el template---------
                $linkOptions['aria-controls'] = $options['id']; //------Ajustes para el template---------
                $linkOptions['role'] = 'tab'; //------Ajustes para el template---------

                Yii::$app->html::addCssClass($options, ['widget' => 'tab-pane']);
                if (ArrayHelper::remove($item, 'active')) {
                    Yii::$app->html::addCssClass($options, 'active');
                    //Yii::$app->html::addCssClass($headerOptions, 'active');
                    Yii::$app->html::addCssClass($linkOptions, 'active'); //------Ajustes para el template---------
                }

                if (isset($item['url'])) {
                    $header = Yii::$app->html::a($label, $item['url'], $linkOptions);
                } else {
                    if (!isset($linkOptions['data-toggle'])) {
                        $linkOptions['data-toggle'] = 'tab';
                    }
                    $header = Yii::$app->html::a($label, '#' . $options['id'], $linkOptions);
                }

                if ($this->renderTabContent) {
                    $tag = ArrayHelper::remove($options, 'tag', 'div');
                    $panes[] = Yii::$app->html::tag(
                        $tag,
                        isset($item['content']) ? $item['content'] : '',
                        $options
                    );
                }
            }

            $headers[] = Yii::$app->html::tag('li', $header, $headerOptions);
        }

        $items = Yii::$app->html::tag('div', Yii::$app->html::tag('ul', implode("\n", $headers), $this->options), ['class' => 'col-3']) . $this->renderPanes($panes);

        return Yii::$app->html::tag(
            'div',
            $items,
            ['class' => 'row', 'data-plugin' => 'tabs']
        );
    }

}
