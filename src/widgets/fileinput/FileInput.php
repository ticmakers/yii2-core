<?php

namespace ticmakers\core\widgets\fileinput;

/**
 * Widget para inputs tipo file.
 *
 * @package ticmakers\core
 * @subpackage widgets\FileInput
 * @category Category
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr320@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */

use Yii;
use yii\base\Widget;
use yii\base\InvalidConfigException;


class FileInput extends Widget
{
    public $model;
    public $attribute;
    public $formName;
    public $dataPreview;
    public $multiple;
    public $_suffixFileOld = '-old';
    public $allowedFileExtensions = [];
    public $maxFilesCount;
    public $titlesInput = [];
    public $options = [];

    /**
     * Evento para inicializar el widget
     */
    public function init()
    {
        parent::init();
        $this->allowedFileExtensions = array_unique($this->allowedFileExtensions);
        if ($this->multiple == null) {
            $this->multiple = false;
        }
        $this->attribute = str_replace('[]', '', $this->attribute);
        if ($this->maxFilesCount == null || $this->maxFilesCount == 0) {
            $this->maxFilesCount = 1;
        }
        if (is_string($this->dataPreview)) {
            $this->dataPreview = [$this->dataPreview];
        }
    }

    /**
     * Evento de ejecución del widget
     * 
     * @return string
     */
    public function run()
    {
        FileInputAsset::register($this->view);
        return $this->render('main');
    }
}
