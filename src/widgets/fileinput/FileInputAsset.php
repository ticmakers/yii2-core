<?php

namespace ticmakers\core\widgets\fileinput;

use yii\web\AssetBundle;

class FileInputAsset extends AssetBundle
{
    public $sourcePath = '@ticmakers/core/widgets/fileinput/assets';
    public $css = [
        'css/magnific-popup.css',
        'css/fileinput.css'
    ];
    public $js = [
        'js/magnific-popup.js',
        'js/fileinput.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
        'ticmakers\core\web\BaseAsset',
    ];
}
