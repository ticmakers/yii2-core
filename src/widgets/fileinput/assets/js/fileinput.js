function FileInputTest(id, pluginOptions, alertMessages) {
    $(document).ready(function () {
        $(`#${id} .file-drop-zone-t`).magnificPopup({
            delegate: 'a.zoom-preview',
            type: 'image',
            gallery: {
                enabled: true
            },
        });

        $(`#${id} #file-input-${id}`).change(function () {
            readURL(this);
        });

        $(`#${id} .hide-preview`).click(function ($event) {
            preDeleteItem($(this));
            event.stopImmediatePropagation();
            return false;
        });

        $(`#${id} .clear-preview`).click(function () {
            clearPreview();
        });

        preloadImages();
    });

    function preloadImages() {
        let dataPreview = pluginOptions.dataPreview;

        if (dataPreview && dataPreview.length) {
            for (let imageData of dataPreview) {
                addItemPreview(imageData.url, imageData.name);
            }
        }
    }

    function preDeleteItem(element) {
        let $this = element;
        let url = $this.data('url');
        let name = $this.data('caption');
        deleteItem(url, name);
    }

    function clearPreview() {
        let input = getAttributeInput();
        let fileListUpdated = new FileList([]);
        let nameInputHidden = pluginOptions.formName + '[' + pluginOptions.attribute + '-old]';
        let nameInputHiddenJson = pluginOptions.formName + '[' + pluginOptions.attribute + '-old-data]';
        let hiddenInput = $('input[name="' + nameInputHidden + '"]')
        let hiddenInputJson = $('input[name="' + nameInputHiddenJson + '"]')
        input.get(0).files = fileListUpdated;
        hiddenInput.val(null);
        hiddenInputJson.val('[]');
        $(`#${id} .file-drop-zone-t`).empty();
    }

    function readURL(input) {
        let msg = '';
        if (validateFilesCount()) {
            //let oldPreviewFiles = $(`#${id} .data-prev`).val();
            console.log(`#${id} .data-prev-json`);
            let oldPreviewJson = JSON.parse($(`#${id} .data-prev-json`).val());
            /* let files = oldPreviewFiles.split(','); */
            if (validateFileExtensions()) {
                if (input.files && input.files[0]) {
                    $(`#${id} .file-drop-zone-t`).empty();
                    for (let $file of input.files) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            addItemPreview(e.target.result, $file.name);
                        }
                        reader.readAsDataURL($file);
                    }
                    if (oldPreviewJson.length) {
                        for (let filePreview of oldPreviewJson) {

                            addItemPreview(filePreview.url, filePreview.name);
                        }
                    }
                }
                $(`#${id} #file-error-content`).addClass('d-none');
            } else {
                msg = alertMessages.invalidExtensions;
                showAlertMessage(msg);
                /* clearPreview(); */
            }
        } else {
            msg = alertMessages.invalidCountFiles;
            showAlertMessage(msg);
            /* clearPreview(); */
        }
    }

    function validateFilesCount() {
        let status = true;
        let files = getAttributeInput().get(0).files;
        let oldPreviewImages = $(`#${id} .data-previous`).find('input[type="hidden"]').val() || '';
        let images = oldPreviewImages.split(',');

        if (pluginOptions.maxFilesCount == 1) {
            $(`#${id} .file-drop-zone-t`).empty()
            $(`#${id} .data-previous`).find('input[type="hidden"]').val('')
            images = []
        }

        let currentCountFiles = files ? files.length : 0;
        let previousCountfiles = images[0] == "" ? 0 : images.length;
        if ((currentCountFiles + previousCountfiles) > pluginOptions.maxFilesCount) {
            status = false;
        }
        return status;

    }

    function validateFileExtensions(fileInput) {
        let response = true;
        let extensionsAllowed = pluginOptions.allowedFileExtensions;
        let files = getAttributeInput().get(0).files;
        if (files && files.length) {

            for (let file of files) {
                let fileExtension = file.name.split('.').pop();
                if ($.inArray(fileExtension, extensionsAllowed) == -1) {
                    response = false;
                }
            }
        }
        return response;
    }


    function showAlertMessage(msg) {
        let textMessageAlert = $(`#${id} #file-error-message`);
        textMessageAlert.text(msg);
        textMessageAlert.parent().show();
        textMessageAlert.parent().removeClass('d-none');
    }

    /* function clearFilesLoaded()
    {
        let input = getAttributeInput();
        let fileListUpdated = new FileList([]);
        input.get(0).files = fileListUpdated;
    } */

    function previewFile(url, fileName) {
        if (pluginOptions.maxFilesCount == 1) {
            $(`#${id} .file-drop-zone-t`).append(
                '<a href="' + url + '" download class="">' +
                '<span class="fa fa-file-alt mr-2"></span>' +
                '<span>' + fileName + '</span>' +
                '</a>'
            );
        } else {
            $(`#${id} .file-drop-zone-t`).append(
                '<a href="' + url + '" download class="preview-files">' +
                '<div class="m-1 rounded file-name-preview col-12"><span class="fa fa-file-alt mr-2"></span>' + fileName +
                '<button type="button" class="close hide-preview" data-url ="' + url + '" data-caption="' + fileName + '" >' +
                '<span aria-hidden="true" >&times;</span>' +
                '</button>' +
                '</div></a>'
            );
        }
    }

    function previewImage(url, fileName) {
        if (pluginOptions.maxFilesCount == 1) {
            $(`#${id} .file-drop-zone-t`).append(
                '<a href="' + url + '" class="zoom-preview col-5 p-0 m-2">' +
                '<img class="m-1 preview-image rounded w-100" src="' + url + '">' +
                '</img></a>'
            );
        } else {
            $(`#${id} .file-drop-zone-t`).append(
                '<a href="' + url + '" class="zoom-preview col-5 p-0 m-2">' +
                '<div class="m-1 preview-image rounded flex-grow-1 col-12" style="background-image: url(\'' + url + '\')">' +
                '<button type="button" class="close hide-preview" data-url ="' + url + '" data-caption="' + fileName + '" >' +
                '<span aria-hidden="true" >&times;</span>' +
                '</button>' +
                '</div></a>'
            );
        }
    }

    function addItemPreview(url, fileName) {

        let extensionFiles = ['jpg', 'png', 'jpge'];

        let currentFileExt = fileName.substr(fileName.lastIndexOf(".") + 1);
        extensionFiles.includes(currentFileExt) ? previewImage(url, fileName) : previewFile(url, fileName);

        $(`#${id} .hide-preview`).click(function () {
            preDeleteItem($(this));
            event.stopImmediatePropagation();
            return false;
        })
    }


    function getAttributeInput() {
        let multipleOptionName = pluginOptions.multiple ? '[]' : '';
        let nameInput = pluginOptions.formName + '[' + pluginOptions.attribute + ']' + multipleOptionName;
        console.log(nameInput);
        let input = $(`#${id} input[name="${nameInput}"]`)
        return input;
    }


    function deleteItem(url, name) {
        let input = getAttributeInput();
        let files = input.get(0).files;
        let nameInputHidden = pluginOptions.formName + '[' + pluginOptions.attribute + '-old]';
        let nameInputHiddenJson = pluginOptions.formName + '[' + pluginOptions.attribute + '-old-data]';
        let hiddenInput = $(`#${id} input[name="${nameInputHidden}"]`)
        let hiddenInputJson = $(`#${id} input[name="${nameInputHiddenJson}"]`)
        let oldFiles = hiddenInput.val().split(',');
        let oldFilesJson = JSON.parse(hiddenInputJson.val())
        let element = $(`#${id} .hide-preview[data-url="${url}"]`);
        let elementLoad = $(`#${id} .hide-preview[data-caption="${name}"]`);
        oldFiles = $.map(oldFiles, function (n, i) {
            if (n != url) {
                return n;
            }
        });
        oldFilesJson = $.map(oldFilesJson, function (n, i) {
            if (n.url != url) {
                return n;
            }
        });

        if (name && name != '') {
            let fileIndex = elementLoad.data('caption');
            let newFileList = Array.from(files);
            if (newFileList.length) {
                newFileList = $.map(newFileList, function (n, i) {
                    if (n.name != fileIndex) {
                        return n;
                    }
                });
                let fileListUpdated = new FileList(newFileList);
                input.get(0).files = fileListUpdated;
            }
        }

        element.parent().parent().hide();
        hiddenInput.val(oldFiles.join(","));
        hiddenInputJson.val(JSON.stringify(oldFilesJson));
    }





    /**
     *
     *
     */

    class FileList {
        constructor(...items) {
            // flatten rest parameter
            items = [].concat(...items);
            // check if every element of array is an instance of `File`
            if (items.length && !items.every(file => file instanceof File)) {
                throw new TypeError("expected argument to FileList is File or array of File objects");
            }
            // use `ClipboardEvent.clipboardData` for Firefox, which returns `null` at Chromium
            // we just need the `DataTransfer` instance referenced by `.clipboardData`
            const dt = new ClipboardEvent("").clipboardData || new DataTransfer();
            // add `File` objects to `DataTransfer` `.items`
            for (let file of items) {
                dt.items.add(file)
            }
            return dt.files;
        }
    }

}