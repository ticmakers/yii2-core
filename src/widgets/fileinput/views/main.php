<?php

use yii\helpers\Json;
use yii\web\View;

$widget = $this->context;
$config = Json::encode($widget);
$inputName = $widget->formName . '[' . $widget->attribute . ']';

?>

<div class="form-group" id="<?= $this->context->getId() ?>">
    <div class="file-input <?= $widget->multiple ? '' : 'only' ?>">
        <div class="file-container rounded p-3">
            <button type="button" class="close" style="margin: -10px -10px 0 0;">
                <span aria-hidden="true" class="clear-preview">&times;</span>
            </button>
            <div class="file-drop-zone-t flex-column rounded row m-0 mb-2">
                <!-- hidden input old values -->
            </div>
            <div class="alert alert-danger alert-dismissible fade show d-none" id="file-error-content" role="alert">
                <p id="file-error-message"></p>
                <button type="button" class="close" onclick="$(this).parent().hide()">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="data-previous">
                <?php
                if ($widget->dataPreview && is_array($widget->dataPreview) && count($widget->dataPreview) > 0) {

                    $finalDataPreview = [];
                    
                    foreach ($widget->dataPreview as $key => $value) {
                        /* if(is_array($value)){ */
                            $finalDataPreview[] = $value['url'];
                        /* }else{
                            $finalDataPreview[] = $value;
                        } */
                    }
                    $dataPreview = implode(',', $finalDataPreview ?? []);
                    $dataPreviewJson = Json::encode($widget->dataPreview);
                } else {
                    $dataPreview = '';
                    $dataPreviewJson = '[]';
                }

                ?>

                <?= Yii::$app->html::hiddenInput($widget->formName . '[' . $widget->attribute . $widget->_suffixFileOld . ']', $dataPreview, ['class' => 'data-prev']) ?>
                <?= Yii::$app->html::hiddenInput($widget->formName . '[' . $widget->attribute . $widget->_suffixFileOld . '-data]', $dataPreviewJson, ['class' => 'data-prev-json']) ?>
            </div>
            <div class="custom-file" id="customFile" lang="es">
                <!-- File input -->
                <?= Yii::$app->html::fileInput(
                    $widget->multiple ? $inputName . '[]' : $inputName,
                    $value = '',
                    $options = Yii::$app->arrayHelper::merge($widget->options, [
                        'class' => 'custom-file-input',
                        'id' => 'file-input-' . $this->context->getId(),
                        'multiple' => $widget->multiple
                    ])
                ) ?>
                <label class="custom-file-label" for="file-input-<?= $this->context->getId() ?>"></label>
            </div>
        </div>
    </div>
</div>
<?php

$id = $this->context->getId();

$js = <<<JS
var pluginOptions$id = {$config}
var alertMessages$id = {
    'invalidExtensions': 'Archivo no válido, extensiones permitidas: [ ' + pluginOptions$id.allowedFileExtensions.toString() + ' ]',
    'invalidCountFiles': 'Límite de archivos excedido, máximo ' + pluginOptions$id.maxFilesCount + ' archivos.'
}
var test$id = FileInputTest('$id', pluginOptions$id, alertMessages$id)
JS;
$this->registerJs($js);
