<?php

namespace ticmakers\core\widgets\tagsinput;

use yii\base\Widget;

/**
 * Widget para inputs tags
 *
 * @package ticmakers\core
 * @subpackage widgets\tagsinput
 * @category Widgets
 *
 * @author Juan Sebastian Muñoz Reyes <sebastianmr320@gmail.com>
 * @version 0.0.1
 * @since 1.0.0
 */

/**
 * @example 
 * Ejemplo de implementación de widget dentro de ActiveForm y 
 * parametros necesarios para su funcionamiento
 * ``` 
 * <?= $form->field($model, 'attribute', ['help' => '', 'popover' => $model->getHelp('attribute')])->widget(TagsInput::class, [
 *		'attribute' => 'attribute',
 *		'formName' => $model->formName(),
 *		'items' => $model->attribute,
 *       'options' => [ 'tabindex' => 1 ]
 *	]);
 * ?>
 * ```
 */
class TagsInput extends Widget
{

    /**
     * Nombre del atributo con el que quiere generar el input
     */
    public $attribute;

    /**
     * Numbre del modelo con el que quiere vincular el attributo
     * y asignar el name al campo
     */
    public $formName;

    /**
     * Valor del atributo que se visualizara en el listado tags
     * formato: 'value1, value2, value3'
     */
    public $items;

    /**
     * Opciones html que se quieren asignar al input
     */
    public $options = [];

    /**
     * Modelo utilizado para obtener el attributo
     */
    public $model;


    /**
     * Evento para inicializar el widget
     */
    public function init()
    {
        parent::init();
    }

    /**
     * Evento de ejecución del widget
     * 
     * @return string
     */
    public function run()
    {
        TagsInputAsset::register($this->view);
        return $this->render('main');
    }
}
