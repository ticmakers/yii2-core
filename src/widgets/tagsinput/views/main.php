<?php
use ticmakers\core\helpers\ArrayHelper;

$widget = $this->context;
$inputName = $widget->formName . '[' . $widget->attribute . ']';

/**
 * @todo Organizar carga de assets y 
 * configuracion pasada al filieinput.js
 */

// $this->registerCss($this->render('../tagsinput.css'));
// $this->registerJs($this->render('../tagsinput.js'));
?>

<div class="form-group">
    <div class="tags-input-container">
        <div class="w-100" lang="es">
            <!-- Tags input -->
            <?= Yii::$app->html::textInput(
                $inputName,
                $value = $widget->items, 
                $options = ArrayHelper::merge([
                    'class' => 'custom-file-input', 
                    'id' => $widget->getId(). '-tags-input-'.$widget->attribute,
                    'data-role' =>  'tagsinput' 
                ],
                $widget->options))
            ?>    
           
        </div>
    </div>
</div>